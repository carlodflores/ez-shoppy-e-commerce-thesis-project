<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
	parent::__construct();
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {
		if($this->session->userdata('admin') !== null) {
			redirect(base_url() . 'ez/dashboard/');
		}

		$this->form_validation->set_rules('txt_email', '', 'required');
		$this->form_validation->set_rules('txt_password', '', 'required|callback_check_password');

		if ($this->form_validation->run()) {
			redirect(base_url() . 'ez/dashboard/');
		}

		$data['title'] = "EZ Dashboard Login";
		$this->load->view('login_view', $data);
	}

	public function check_password($password) {
		$email = $this->input->post('txt_email');

		if(count($this->Account_Model->check_adm_login($email, md5($password))) >= 1) {
			$this->session->set_userdata('admin', $this->Account_Model->check_adm_login($email, md5($password))->account_id);
			return true;
		} else {
			return false;
		}
	}

	public function logout() {
		$this->session->unset_userdata('admin');
		redirect(base_url() . 'ez/');
	}

}
