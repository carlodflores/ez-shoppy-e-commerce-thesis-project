<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content field">
        <div class="row">
            <div class="seven centered columns text-center field">
                <h3><i class="fa fa-user-plus"> </i> Sign Up</h3>
                <form action="<?=base_url()?>customer/signup/" method="post">
                    <?php if (validation_errors() != ""): ?>
                        <div class="row">
                            <div class="danger alert" style="padding: 10px;"><center><?=validation_errors('<span style="display: block;">','</span>')?></center></div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">First Name <span class="text-indicator">*</span></span>
                            <input type="text" class="input" name="txt_fname" placeholder="First Name" value="<?=set_value('txt_fname')?>"><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">Last Name <span class="text-indicator">*</span></span>
                            <input type="text" class="input" name="txt_lname" placeholder="Last Name" value="<?=set_value('txt_lname')?>"><br/>
                        </div>
                    </div>
                    <span class="text-label input-label">Email <span class="text-indicator">*</span></span>
                    <input type="text" class="input" name="txt_email" placeholder="E-mail" value="<?=set_value('txt_email')?>"><br/>
                    <span class="text-label input-label">Password <span class="text-indicator">*</span></span>
                    <input type="password" class="input" name="txt_password" placeholder="Password"><br/>
                    <span class="text-label input-label">Confirm Password <span class="text-indicator">*</span></span>
                    <input type="password" class="input" name="txt_re_password" placeholder="Confirm Password"><br/><br/>
                    <input type="hidden" name="txt_url" value="" />
                    <input type="submit"  class="medium primary btn" name="login" value="Sign Up" />
                </form>
            </div>
        </div>
	</div>
<?php
    $this->load->view('main/footer_view');
?>
