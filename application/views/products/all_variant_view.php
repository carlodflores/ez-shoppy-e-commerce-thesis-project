<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Manage Variant</h3><a href="<?=base_url(). 'ez/product/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">
                    <div class="centered push_one twelve columns">
                        <center><h2>Manage Product Variant</h2>
                        <p>
                            Click the variant image to change the display image.

                        </p>
                        <br/>
                    <h3><?=$this->Product_Model->get_product_by_id($pid)->product_title?></h3></center>
                        <Br/>
                        <table class="paginate">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th></th>
                                    <?php foreach($this->Product_Model->get_meta_by_id($pid) as $row): ?>
                                        <th><center><span class="product-title"><?=$row->product_meta_keyword?></span></center></th>
                                    <?php endforeach; ?>
                                    <th><center><span class="product-title">Price</span></center></th>
                                    <th><center><span class="product-title">SKU</span></center></th>
                                    <th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach($variants as $row): ?>
                            <tr class="stocks-row">
                                <td>
                                    <div class="upload-holder">
                                        <input type='file' data-value="<?=$row->product_variant_id?>" name="file_prod_images" id="inputFile" multiple/>
                                        <?php if($row->product_variant_img != ""): ?>
                                            <?php if(strpos($row->product_variant_img, ',')): ?>
                                                <?php $img=explode(',',$row->product_variant_img); ?>
                                                <img src="<?=base_url() . 'img/products/' . $img[0]?>" alt="" width="50"/>
                                            <?php else: ?>
                                                <img src="<?=base_url() . 'img/products/' .$row->product_variant_img?>" alt="" width="50"/>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                    </div>
                                </td>
                                    <td>
                                        <span class="product-title"><?=$row->product_variant_name?></span><Br/>
                                        <span class="txt-label"></span>
                                    </td>

                                <td>
                                    <span class="product-title"><?=$row->product_variant_price?></span><Br/>
                                    <span class="txt-label">Price</span>
                                </td>
                                <td>
                                    <span class="product-title"><?=$row->product_variant_sku?></span><Br/>
                                    <span class="txt-label">Sku</span>
                                </td>

                                <td>
                                    <a href="<?=base_url(). 'ez/product/manage/edit/' . $row->product_variant_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Edit</span></a>
                                    <a href="<?=base_url(). 'ez/product/manage/embed/' . $row->product_variant_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Generate Embed Code</span></a>
                                </td>

                            </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('input[name="file_prod_images"]').on('change', function() {
            var form_data = new FormData();
            for(var i = 0; i < $(this).prop('files').length; i++) {
                form_data.append('file[]', $(this).prop('files')[i]);
            }

            form_data.append('pid', $(this).attr('data-value'));

            if($(this).prop('files').length >= 1) {
                $.ajax({
                    xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                            if (percentComplete === 1) {
                                $('.progress').addClass('hide');
                            }
                        }
                    }, false);
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                        }
                    }, false);
                    return xhr;
                    },
                    url: '<?=base_url()?>ez/product/image/uploadvariant/', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        if(response == "OK") {
                            location.reload();
                        } else {
                            alert("Something went wrong.");
                        }
                    }
                });
            }
        });
    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
