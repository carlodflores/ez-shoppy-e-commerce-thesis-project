<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		if(count($this->Discount_Model->get_all_discounts()) <= 0) {
			$data['page_module'] = "<i class='icon-tag'> </i>All Discounts";
			$this->load->view('alerts/alert_discount_zero_view', $data);
		} else {
			$data['discount'] = $this->Discount_Model->get_all_discounts();
			$this->load->view('discount/all_discount_view', $data);
		}
    }

	public function create() {

		$this->form_validation->set_rules('discount_name', ' Name', 'required|xss_clean');
		$this->form_validation->set_rules('discount_code', ' Code', 'required|xss_clean|is_unique[cs_discounts.discount_code]');
		$this->form_validation->set_rules('discount_type_value', ' Type', 'required|xss_clean');
		$this->form_validation->set_rules('discount_type', '', 'required|xss_clean');
		$this->form_validation->set_rules('discount_limit_value', ' Limit', 'xss_clean');
		$this->form_validation->set_rules('discount_limit', '', 'xss_clean');
		$this->form_validation->set_rules('discount_start_date', ' Start Date', 'required|xss_clean');
		$this->form_validation->set_rules('discount_start_time', ' Start time', 'required|xss_clean');
		$this->form_validation->set_rules('discount_end_date', ' End Date', 'required|xss_clean');
		$this->form_validation->set_rules('discount_end_time', ' End Time', 'required|xss_clean');

		if($this->form_validation->run()) {
			$limit = 0;

			if($this->input->post('discount_limit') == 2) {
				$limit = 99999999;
			} else {
				$limit = $this->input->post('discount_limit_value');
			}

			$discount_data = array(
				"discount_name" => $this->input->post('discount_name'),
				"discount_code" => $this->input->post('discount_code'),
				"discount_used" => 0,
				"discount_value" => $this->input->post('discount_type_value'),
				"discount_type" => $this->input->post('discount_type'),
				"discount_limit" => $limit,
				"date_created" => $this->input->post('discount_start_date') . ' ' . $this->input->post('discount_start_time'),
				"date_expires" => $this->input->post('discount_end_date') . ' ' . $this->input->post('discount_end_time'),
				"discount_status" => 1,
			);

			$this->Discount_Model->insert_discount($discount_data);


			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => $this->Option_Model->get_option_by_value('emailer')->option_value, // change it to yours
				'smtp_pass' => $this->Option_Model->get_option_by_value('emailer_password')->option_value, // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			$customer_id = $this->Order_Model->get_order_by_id($order_id)->customer_id;

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$email_data = array(
				"date" => $time,
				"Message" => "Be quick! We have a " . $this->input->post('discount_type_value')  . " " . ($this->input->post('discount_type') == 1 ? 'Off' : "%") . " Discount <br/><br/> Promo Starts On : " . $this->input->post('discount_start_date') . ' ' . $this->input->post('discount_start_time') . "<Br/>Promo Ends On : " . $this->input->post('discount_end_date') . ' ' . $this->input->post('discount_end_time')
			);



			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('no-reply@hairgeek.ph', 'HairGeek'); // change it to yours
			$this->email->to($this->Customer_Model->get_newsletter()->emails); // change it to yours
			$this->email->subject('HairGeek - We have a ' . $this->input->post('discount_type_value')  . " " . ($this->input->post('discount_type') == 1 ? 'Off' : "%") . ' Discount');
			$this->email->message($this->load->view('email/message_email', $email_data, TRUE));
			$this->email->send();



			redirect(base_url() . 'ez/discount/?success=' . md5("success"));
		}

		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$this->load->view('discount/add_discount_view', $data);
	}

	public function end($code) {
		$data = array(
			"discount_status" => '0'
		);

		$this->Discount_Model->update_discount_by_code($code, $data);

		redirect(base_url() . 'ez/discount/');
	}

}

?>
