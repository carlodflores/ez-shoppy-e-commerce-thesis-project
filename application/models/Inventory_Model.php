<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

	    function insert_item_sku($data) {
	        $this->db->insert('cs_inventory', $data);
	        return $this->db->insert_id();
	    }

	    function get_inventory_details($product_id) {
	        $this->db->where('product_id', $product_id);
	        return $this->db->get('cs_inventory')->row();
	    }

		function get_sum_stocks($pid) {
			$this->db->select('SUM(inventory_stocks) as total_stocks');
			$this->db->from('cs_inventory');
			$this->db->where('product_id', $pid);
			$this->db->group_by('product_id');
			$query = $this->db->get();
			return $query->row()->total_stocks;
		}

		function get_inventory_details_sku($sku) {
	        $this->db->where('inventory_sku', $sku);
	        return $this->db->get('cs_inventory')->row();
	    }

		function update_stocks_by_sku($data, $sku, $pid) {
			$this->db->where('inventory_sku', $sku);
			$this->db->where('product_id', $pid);
			$this->db->update('cs_inventory', $data);
			return $this->db->affected_rows();
		}

	// Transfer

		function insert_transfer($data) {
			$this->db->insert('cs_inventory_transfer', $data);
	        return $this->db->insert_id();
		}
}
?>
