<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Inquiry Details</h3><a href="<?=base_url(). 'ez/inquiry/'?>"><span class="btn-more" style="color:#fff;">Back</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <form action="<?=base_url()?>ez/inquiry/info/<?=$ini->inquiry_id?>/" method="post">
                        <div class="centered ten columns">
                            <h4><?=$ini->inquiry_fullname?></h4>
                            <h6><?=$ini->inquiry_email?></h6>
                            <h6><?=$ini->date_created?></h6>

                            <br/>

                            <p><?=$ini->inquiry_message?></p>
                            <Br/>
                            <?php if (count($this->Customer_Model->get_inquiry_reply($ini->inquiry_id)) >= 1): ?>
                                <h4>Your Replies</h4>
                                <?php foreach ($this->Customer_Model->get_inquiry_reply($ini->inquiry_id) as $key => $value): ?>
                                    <p>
                                        <?=$value->inquiry_reply_message?>
                                        <?=$value->date_created?>
                                    </p>
                                    <hr style="height: 1px; background: rgba(0, 0, 0, .2);">
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <h4>Reply</h4><Br/>
                            <textarea name="txt_message" class="input textarea" rows="3" placeholder="Reply Message"></textarea>
                            <Br/>
                            <div class="row">
                                <div class="centered four columns">
                                    <center>
                                        <center><input class="medium primary btn" type="submit" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;" value="Send"></center>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/><br/>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
<script src="<?=base_url()?>js/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init(
        {
            selector:'textarea',
            plugins: [
       "advlist autolink lists link image charmap print preview anchor",
       "searchreplace visualblocks code fullscreen",
       "insertdatetime media table contextmenu paste imagetools"
   ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            menubar: false,
            max_height: 300
        }
    );
</script>
</body>
</html>
