$(function() {

    $('input[type="submit"]').click(function() {
        $(this).val('Please Wait.').attr('disabled', 'disabled');
        $(this).parents('form').submit()
    });

    $('.toggle-nav').click(function() {
        toggleNav();
    });

    $("#main-navigation li").click(function() {
        $('nav').animate({'marginLeft': '-=200px'}, "fast");
    });

    $("#site-wrapper").click(function() {
        $('#site-wrapper').removeClass('show-nav');
        $('#site-menu').removeClass('show');
        $('#menu-expander').removeClass('usog');
        $('.c-hamburger').removeClass('is-active');
    });

    $('table.paginate').each(function() {
        var currentPage = 0;
        var numPerPage = ($(this).attr('max'))? $(this).attr('max'): 10;
        var $table = $(this);
        $table.bind('repaginate', function() {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<div class="pager"></div>');
        if(numPages > 1) {
            for (var page = 0; page < numPages; page++) {
                $('<span class="page-number info badge"></span>').text(page + 1).bind('click', {
                    newPage: page
                }, function(event) {
                    currentPage = event.data['newPage'];
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
                }).appendTo($pager).addClass('clickable');
            }
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });

    $('form').on('keyup keypress', function(e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });

});

function toggleNav() {
    if ($('#site-wrapper').hasClass('show-nav')) {
        $('#site-wrapper').removeClass('show-nav');
        $('#site-menu').removeClass('show');
        $('#menu-expander').removeClass('usog');
    } else {
        $('#site-wrapper').addClass('show-nav');
        $('#site-menu').addClass('show');
        $('#menu-expander').addClass('usog');
    }
}


(function() {

"use strict";

var toggles = document.querySelectorAll(".c-hamburger");

var toggle = toggles[0];
toggleHandler(toggle);

function toggleHandler(toggle) {
toggle.addEventListener( "click", function(e) {
e.preventDefault();
(this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
});
}

})();

$(function() {

    $('ul.main-navigation li').click(function() {
        $(this).children('ul').slideToggle("fast");
        $(this).children('span').toggleText('+', '-');
    });
});

jQuery.fn.extend({
    toggleText: function(stateOne, stateTwo) {
        return this.each(function() {
            stateTwo = stateTwo || '';
            $(this).text() !== stateTwo  && stateOne
            ? $(this).text(stateTwo)
            : $(this).text(stateOne);
        });
    }
});
