<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-flight"> </i>Transfer</h3><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid field">
                <div class="row">
                    <div class="centered push_one eight columns">
                        <center><h2>Manage Incoming Inventory</h2>
                        <p>
                            Receive new products and track incoming inventory with transfers.

                        </p></center>
                        <Br/>
                        <form method="post" class="append">
                            <span class="txt-label">Product Name</span><br/>
                            <input type="text" class="input" name="search_temp" placehoder="Search Product" list="search-datalist">
                            <datalist id="search-datalist">
                                <?php foreach($products as $row): ?>
                                    <option data-value="<?=$row->product_id?>" value="<?=ucwords(strtolower($row->product_title))?>" />
                                <?php endforeach; ?>
                            </datalist>
                            <div class="medium primary btn"><i class="icon-search" style="color: #fff;"></i></div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="centered push_one twelve columns">
                        <table class="paginate">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th></th>
                                    <th><center><span class="product-title">Name</span></center></th>
                                    <th><center><span class="product-title">Inventory</span></center></th>
                                    <th><center><span class="product-title">Category</span></center></th>
                                    <th><center><span class="product-title">Brand</span></center></th>
                                    <th><center><span class="product-title">SKU</span></center></th>
                                    <th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach($products as $row): ?>
                            <?php
                                $img = $row->product_img;
                                if(strpos(',', $row->product_img)) {
                                    $img = explode(',',$row->product_img);
                                    $img = $img[0];
                                }
                             ?>
                            <tr class="stocks-row" data-tag="<?=$row->product_title?>">
                                <?php if(strpos($row->product_img, ',')): ?>
                                    <td><span class="product-title"><img src="<?=base_url() . 'img/products/' . $img[0]?>" alt="" width="50"/></span></td>
                                <?php else: ?>
                                    <td><span class="product-title"><img src="<?=base_url() . 'img/products/' . $img?>" alt="" width="50"/></span></td>
                                <?php endif; ?>
                                <td>
                                    <span class="product-title"><?=$row->product_title?></span><Br/>
                                    <span class="txt-label"></span>
                                </td>
                                <td>
                                    <span class="product-title"><?=$this->Inventory_Model->get_sum_stocks($row->product_id)?>/<?=count($this->Product_Model->get_product_variant($row->product_id))?></span><Br/>
                                    <span class="txt-label">Stocks/Variants</span>
                                </td>
                                <td>
                                    <span class="product-title"><?=$row->product_type_name?></span><Br/>
                                    <span class="txt-label">Category</span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$row->product_brand_name?></span><Br/>
                                    <span class="txt-label">Brand</span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$row->product_sku_id?></span><Br/>
                                    <span class="txt-label">SKU</span>
                                </td>

                                <td>
                                    <a href="<?=base_url(). 'ez/product/transfer/edit/' . $row->product_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Manage Transfer</span></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>

    $(function() {



        $('input[name="search_temp"]').keyup('change', function() {
            var data = $('#search-datalist'),
                sel = $(this).val();

            $( ".stocks-row[data-tag]" ).each(function( index ) {
                if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                    $(this).hide("fast");
                } else {
                    $(this).show("fast");
                }
            });

        });

    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
