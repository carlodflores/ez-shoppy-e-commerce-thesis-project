<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i> Embed Variant</h3><a href="<?=base_url(). 'ez/product/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">

                </div>
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <center><h2>Embed Product</h2>
                        <p>This feature allow you to include your product to another existing website.</p></center>
                        <Br/>
                    </div>

                    <div class="centered push_one twelve columns field">
                        <p>
                            Copy the following code and paste to it another website.<br/><br/>
                        </p>

                        <p>
                            Preview :
                        </p>
                        <div id="ezshop-container-<?=$pvid?>"></div>
                        <br/><br/><br/>
                        <p>
                            HTML :<br/>
                            Place it where you want the content to display.
                        </p>

                        <textarea style="font-size: 14px; line-height: inherit;" rows="4" class="input textarea"><div id="ezshop-container-<?=$pvid?>"></div></textarea>
                        <Br/><Br/>
                        <p>
                            The Javascript:<br/>
                            Place it in the head tag
                        </p>
                        <textarea style="font-size: 14px; line-height: inherit;" rows="10" class="input textarea">
                            <script type="text/javascript">$(function(){function r(r,e){e=JSON.stringify(e).replace(/"/g,"").replace(/,/g,";"),$("<style>").prop("type","text/css").html(r+e).appendTo("head")}var e=$("#ezshop-container-<?=$pvid?>"),t="auto",d=200,o="#1495ef"
e.css({height:t+"px",width:d+"px",backgroundColor:o,font:"700 14px Calibri",color:"#fff",padding:"0px 0px 10px 0px","border-radius":"5px"}),r(".product-img-holder",{width:"100%"}),r("button",{border:"none",background:"#1d3e55",padding:"10px",color:"#fff","border-radius":"3px","font-weight":"800",cursor:"pointer"}),r(".the-real-holder",{"max-height":"200px","overflow-y":"hidden"}),$.post("<?=base_url()?>ez/api/pos/get/<?=$pvid?>/",{},function(r){var r=$.parseJSON(r)
e.append("<div class='the-real-holder'><img class='product-img-holder' src='<?=base_url()?>img/products/"+r.product_variant_img+"'/></div>"),e.append("<div class='product-details'><center>"+r.product_title+": "+r.product_variant_name+"<br/><BR/> P"+r.product_variant_price+" Only</center></div>"),e.append("<br/><center><a href='<?=base_url()?>cart/add/<?=$pvid?>/?redirect_to=<?=base_url()?>cart/embed/'><button>BUY NOW!</button></a></center>")})})
</script>
                        </textarea>
                    </div>
                </div>
            </div>

        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>

<script type="text/javascript">$(function(){function r(r,e){e=JSON.stringify(e).replace(/"/g,"").replace(/,/g,";"),$("<style>").prop("type","text/css").html(r+e).appendTo("head")}var e=$("#ezshop-container-<?=$pvid?>"),t="auto",d=200,o="#1495ef"
e.css({height:t+"px",width:d+"px",backgroundColor:o,font:"700 14px Calibri",color:"#fff",padding:"0px 0px 10px 0px","border-radius":"5px"}),r(".product-img-holder",{width:"100%"}),r("button",{border:"none",background:"#1d3e55",padding:"10px",color:"#fff","border-radius":"3px","font-weight":"800",cursor:"pointer"}),r(".the-real-holder",{"max-height":"200px","overflow-y":"hidden"}),$.post("<?=base_url()?>ez/api/pos/get/<?=$pvid?>/",{},function(r){var r=$.parseJSON(r)
e.append("<div class='the-real-holder'><img class='product-img-holder' src='<?=base_url()?>img/products/"+r.product_variant_img+"'/></div>"),e.append("<div class='product-details'><center>"+r.product_title+": "+r.product_variant_name+"<br/><BR/> P"+r.product_variant_price+" Only</center></div>"),e.append("<br/><center><a href='<?=base_url()?>cart/add/<?=$pvid?>/?redirect_to=<?=base_url()?>cart/embed/'><button>BUY NOW!</button></a></center>")})})
</script>

<script>
$(function() {

    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
