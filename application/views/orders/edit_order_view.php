<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }

    a {
        color: #7997c1;
    }

    a:hover {
        color: #7999e2;
    }

    .ship-fields input {
        margin-top: 10px;
    }

    .show-holder {
        background: #fff; border-radius: 3px; padding: 10px; width: 200px; position: absolute; display: none; z-index: 999999;
        border: 2px solid rgba(121, 151, 193, 0.6);
    }
</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-download"> </i>Update Order #<?=$order_id?></h3><a href="<?=base_url(). 'ez/order/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid field">
                <div class="row">
                    <div class="push_one fifteen columns">
                        <form action="<?=base_url()?>ez/order/edit/<?=$order_id?>/" method="post">
                        <input type="hidden" name="variants_added" value="" />
                        <div class="row">
                            <div class="nine columns">
                                <h3 class="content-title">Order Details</h3>
                                <div class="row">
                                    <div>
                                        <table id="order-details-holder">
                                            <?php foreach ($this->Order_Model->get_order_content_by_id($order_id) as $key => $row) {
                                                $var_data = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                                                $img = $var_data->product_variant_img;
                                                if(strpos($var_data->product_variant_img, ',')) {
                                                    $img = explode(',', $var_data->product_variant_img);
                                                }
                                            ?>
                                                <tr class="stocks-row loaded-order <?=$row->order_content_id?>_row" data-value="<?=$row->product_variant_id?>">
                                                    <td><img src="<?=base_url() ?>img/products/<?=is_array($img)? $img[0] : $img?>" width="50"></td>
                                                    <td>
                                                        <span class="product_title"><?=$this->Product_Model->get_product_by_id($var_data->product_id)->product_title?></span>
                                                        <Br/><span class="txt-label"><?=str_replace(' ', ' / ', $var_data->product_variant_name)?></span>
                                                    </td>
                                                    <td>
                                                        <a class="show-panel" data-trigger="show-item-disc-<?=$row->product_variant_id?>">
                                                            P <?=$var_data->product_variant_price?>
                                                        </a> x
                                                        <input type="number" readonly="readonly" min="0" class="input narrow qty_input" name="qty_<?=$row->product_variant_id?>" data-name="qty_<?=$row->product_variant_id?>" data-orig="<?=$var_data->product_variant_price?>" data-price="<?=$var_data->product_variant_price?>" placeholder="Quantity" value="<?=$row->order_content_qty?>"/>
                                                    </td>
                                                    <td class="v_price_display">
                                                        P <?=$var_data->product_variant_price?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <br/>
                                    <div class="eight columns alpha">
                                        <input type="text" class="input" name="txt_order_note" value="<?=$order->order_note?>" placeholder="Add a note" readonly="readonly"/>
                                    </div>
                                    <div class="eight columns omega">
                                        <table>
                                            <tr class="stocks-row">
                                                <?php $check = count($this->Order_Model->get_order_discount($order_id, '0', 'all')); ?>
                                                <?php if ($check >= 1): ?>
                                                    <td style="text-align: right;">Discount</td>
                                                    <td>
                                                        <span class="prepend append" style="margin-right: 20px; ">
                                                            <div class="medium <?=($check != 0)? (($this->Order_Model->get_order_discount($order_id, '0', 'all')->order_discount_type == 1)? 'primary' : 'default') : 'primary'?> btn" data-action="1" data-tag="discount_option" style="font-size: 12px;"><a>P</a></div>
                                                            <div class="medium <?=($check != 0)? (($this->Order_Model->get_order_discount($order_id, '0', 'all')->order_discount_type == 1)? 'default' : 'primary') : 'default'?> btn" data-action="2" data-tag="discount_option" style="font-size: 12px;"><a>%</a></div>
                                                        </span>
                                                        <span><input type="number" readonly="readonly" min="0" class="input narrow" name="discount_value" data-action="<?=($check != 0)? $this->Order_Model->get_order_discount($order_id, '0', 'all')->order_discount_type : ''?>" value="<?=($check != 0)? $this->Order_Model->get_order_discount($order_id, '0', 'all')->order_discount_value : ''?>"></span>
                                                        <br/>
                                                        <input type="hidden" name="discount_value_type" value="1">
                                                        <input type="text" style="margin-top: 10px;" class="input wide" name="txt_discount_reason" placeholder="Reason" value="" readonly="readonly">
                                                    </td>
                                                <?php endif; ?>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;">Subtotal</td>
                                                <td class="subtotal-display">P <?=number_format($order->order_subtotal, 2)?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;">Shipping</td>
                                                <td>P <?=$order->order_shipping?></td>
                                            </tr>

                                            <?php if ($order->customer_id != '-1'): ?>
                                                <tr>
                                                    <td style="text-align: right;">Additional Payment</td>
                                                    <td>P <?=$order->order_additional_payment . ' (' . $this->Order_Model->get_all_meta_value($order->order_id, 'payment_method')->value . ')'?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <tr>
                                                <td style="text-align: right; font: 700 20px 'Open Sans', sans-serif;">Total</td>
                                                <td style="font: 700 20px 'Open Sans', sans-serif;" class="total-display">P <?=number_format($order->order_total, 2)?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <br/>

                                <?php if (count($this->Order_Model->check_if_has_proof($order->order_id)) >= 1): ?>
                                    <?php $proof = $this->Order_Model->check_if_has_proof($order->order_id); ?>
                                    <div class="row">
                                        <h3 class="content-title">Proof Details</h3>
                                        <Br/><Br/>
                                        <img src="<?=base_url()?>img/proofs/<?=$proof->order_proof_file?>" alt="" />
                                        <p>
                                            <?=$proof->order_proof_note?>
                                        </p>
                                    </div>
                                <?php endif; ?>

                                <?php if ($order->order_payment_status == 2 && $order->order_status != 1): ?>
                                    <Br/>
                                    <div class="row">

                                        <div class="five columns"><i class="icon-upload" style="float: left; font-size: 24px; padding-top: 5px; color: #7997c1;"> </i><h3 class="content-title" style="float:none;">Fulfill Items</h3></div>
                                        <div class="eleven columns">
                                            <a href="<?=base_url()?>ez/order/fulfillment/<?=$order_id?>"><span class="btn-more" id="btn_mark_fulfill" data-action="fulfill">Fulfill Items</span></a>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if ($order->order_payment_status <= 1): ?>
                                    <Br/>
                                    <Br/>
                                    <div class="row">

                                        <div class="five columns"></div>
                                        <div class="eleven columns">
                                            <i class="icon-credit-card" style="float: left; font-size: 24px; padding-top: 5px; color: #7997c1;"> </i><h3 class="content-title" style="float:none;">Accept Payment</h3>

                                            <a href="<?=base_url()?>ez/order/marker/<?=$order_id?>/paid/">
                                                <span class="btn-more" id="btn_mark_paid" style="float: none; margin-left: 20px;">Mark as Paid</span>
                                            </a>

                                            <a href="<?=base_url()?>ez/order/marker/<?=$order_id?>/reject/">
                                                <span class="btn-more" id="btn_mark_paid" style="float: none; margin-left: 20px;">Reject Proof</span>
                                            </a>

                                        </div>
                                    </div>
                                    <Br/>
                                <?php endif; ?>

                                <input type="hidden" name="txt_order_total" value="" />
                            </div>

                            <?php if ($order->customer_id != '-1'): ?>
                                <div class="seven columns">
                                    <div class="row">
                                        <h3 class="content-title">Customer Details</h3>
                                        <table></table>
                                    </div>
                                    <div class="row">
                                        <input type="text" class="input" name="txt_customer_email" placeholder="Customer Email" list="costumer_list" value="<?=$this->Customer_Model->get_cutomer_detail_by_id($order->customer_id)->customer_email?>" readonly="readonly"/>
                                    </div>

                                    <div class="row">
                                        <BR/>
                                        <span class="txt-label">Customer Profile</span>
                                        <div class="row">
                                            <div class="four columns">
                                                <img src="<?=base_url()?>img/profile-placeholder.jpg" id="customer_img" width="80" style="border-radius: 50%;" alt="" />
                                            </div>
                                            <div class="twelve columns">
                                                <h3 class="content-title" id="customer_name" style="color: #000; float:none; margin-top: 10px; "><?=$this->Customer_Model->get_cutomer_detail_by_id($order->customer_id)->customer_fname . ' ' . $this->Customer_Model->get_cutomer_detail_by_id($order->customer_id)->customer_lname?></h3>
                                                <h3 class="content-title" id="customer_email" style="color: #000; margin-top: -10px; font-size: 12px;"><?=$this->Customer_Model->get_cutomer_detail_by_id($order->customer_id)->customer_email?></h3>
                                            </div>
                                        </div>

                                        <div class="row ship-fields">
                                            <span class="txt-label">Shipping Details</span>
                                            <div class="row">
                                                <div class="eight columns">
                                                    <span class="txt-label" style="font-size: 12px;">Company</span>
                                                    <input type="text" class="input" name="customer_company" placeholder="Company" value="<?=$order->order_company?>" readonly="readonly"/>
                                                </div>
                                                <div class="eight columns">
                                                    <span class="txt-label" style="font-size: 12px;">Phone</span>
                                                    <input type="text" class="input" name="customer_phone" placeholder="Phone" value="<?=$order->order_phone?>" readonly="readonly"/>
                                                </div>
                                            </div>

                                            <span class="txt-label" style="font-size: 12px;">Address</span>
                                            <input type="text" class="input" name="customer_address" placeholder="Address" value="<?=$order->order_address?>" readonly="readonly"/>

                                            <div class="row">
                                                <div class="eight columns">
                                                    <span class="txt-label" style="font-size: 12px;">City</span>
                                                    <input type="text" class="input" name="customer_city" placeholder="City" value="<?=$order->order_city?>" readonly="readonly"/>
                                                </div>
                                                <div class="eight columns">
                                                    <span class="txt-label" style="font-size: 12px;">Zip/Postal Code</span>
                                                    <input type="text" class="input" name="customer_zipcode" placeholder="Postal/Zip Code" value="<?=$order->order_zipcode?>" readonly="readonly"/>
                                                </div>
                                            </div>

                                            <span class="txt-label" style="font-size: 12px;">State</span>
                                            <input type="text" class="input" name="custer_state" placeholder="State" value="<?=$order->order_state?>" readonly="readonly"/>
                                            <Br/><br/>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        </form>

                        <div class="row">
                            <div class="centered eight columns">
                                <h3 class="content-title" style="float: none;">Order History</h3>
                                <?php foreach ($this->Order_Model->get_order_history($order_id) as $key => $value): ?>
                                    <div>
                                        <h3 class="content-title" style="color: #fff; margin-right: 30px; font-size: 14px; padding: 10px; background: #000;"><?=$value->date_created?></h3>
                                        <p style=" padding: 10px; border-bottom: 1px solid rgba(0, 0, 0, 0.2); font: 700 14px 'Open Sans', sans-serif;"><?=$value->order_history_message?></p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <br><br/>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>

    $(function() {

        var shipping = "<?=$order->order_shipping?>";
        var additional = "<?=$order->order_additional_payment?>";


        generate_subtotal();

        $("#btn_mark_pending, #btn_mark_fulfill").click(function(){
            var action = $(this).attr('data-action'),
                id = "<?=$order_id?>";

            $.post("<?=base_url()?>ez/order/marker/" + id +"/" + action + "/", function(data) {
                if(data == "OK!") {
                    location.reload();
                }
            });
        });

        $('input[name="search_temp"]').keyup('change', function() {
            generate_suggestion();
        });

        $(document).on('click', 'a.show-panel', function(){
            var offset = $(this).offset();
    		var posY = offset.top - $(window).scrollTop() + $(this).height() - 20;
    		var posX = offset.left - $(window).scrollLeft();
            var action = $(this).attr('data-trigger');
            $('.item_discount').trigger("change");

            var box = $("div[data-action='"+ action +"']");
            box.css({"top": posY, "left": posX}).toggle({direction: "left"}, "fast");
        });

        <?php if ($order->order_payment_status != 2): ?>
        $(document).on('click', 'div[data-tag="discount_option_item"]', function() {
            var action = $(this).attr('data-action');
            var name = $(this).attr('data-name');
            var id = name.split('-')[1];

            $("div[data-tag='discount_option_item'][data-name='"+ name +"']:eq("+(action-1)+")").removeClass('default').addClass('primary');
            $("div[data-tag='discount_option_item'][data-name='"+ name +"']:not(':eq("+(action-1)+")')").removeClass('primary').addClass('default');
            $("input[name='discount_value_"+id+"']").val("");

            $('input[name="discount_value_'+ id +'"]').attr('data-action', action);
            $('input[name="discount_value_'+ id +'_type"]').val(action);
            switch (action) {
                case '1':
                    $('input[name="discount_value_'+ id +'"]').removeAttr('max');
                    break;

                case '2':
                    $('input[name="discount_value_'+ id +'"]').attr({'min': '0', 'max': '100'});
                    break;
                default:
            }

            generate_subtotal();
        }).trigger('click');
        <?php endif; ?>

        $(document).on('click', '.add-order', function(){
            var count = Math.floor((Math.random() * 1000000) + 1);
            var pvid = $(this).attr('data-value');
            $.post('<?=base_url()?>ez/api/product/get/', {pvid:pvid}, function(data) {
                var data = $.parseJSON(data);
                var img = (data.product_variant_img.indexOf(',') >= 1)? data.product_variant_img.split(',')[0] : data.product_variant_img;

                $('#order-details-holder').append('<tr class="stocks-row loaded-order '+ count +'_row" data-value="'+ data.product_variant_id +'"><td><img src="<?=base_url() ?>img/products/'+ img +'" width="50"></td><td><span class="product_title">'+ data.product_title +'</span><Br/><span class="txt-label">'+ data.product_variant_name.replace(' ', ' / ') +'</span></td><td><a class="show-panel" data-trigger="show-item-disc-'+data.product_variant_id+'">P '+ data.product_variant_price +'</a> x <input type="number" min="0" class="input narrow qty_input" name="qty_'+data.product_variant_id+'" data-name="qty_'+data.product_variant_id+'" data-orig="'+ data.product_variant_price +'" data-price="'+ data.product_variant_price +'" placeholder="Quantity" value="1"></td><td class="v_price_display">P '+ data.product_variant_price +'</td><td style="width: 20px;"><span class="medium default btn variant_remover" data-value="'+ count +'_row"><i class="icon-cancel"></i></span><span></span></td></tr>');
                $('.show-container').append('<div class="show-holder field" data-action="show-item-disc-'+data.product_variant_id+'" class="field"><span class="prepend append" style="margin-right: 20px; "><div class="medium primary btn" data-action="1" data-name="disc-'+data.product_variant_id+'" data-tag="discount_option_item" style="font-size: 12px;"><a>P</a></div><div class="medium default btn" data-action="2" data-name="disc-'+data.product_variant_id+'" data-tag="discount_option_item" style="font-size: 12px;"><a>%</a></div></span><span><input type="number" min="0" class="input normal item_discount" name="discount_value_'+data.product_variant_id+'" data-action="1" value=""></span><br/><input type="hidden" name="discount_value_'+data.product_variant_id+'_type" value="1" /><input type="text" style="margin-top: 10px;" class="input" name="discount_value_'+data.product_variant_id+'_reason" placeholder="Reason" value=""><br/><center><div class="close-item-disc" data-name="close_'+data.product_variant_id+'"><span class="txt-label" style="float: none;">Close</span></div></center></div>');
                $('input[name="search_temp"]').val("");
                generate_suggestion();
                generate_subtotal();
            });
        });

        $(document).on('click', '.close-item-disc', function() {
            var id = $(this).attr('data-name').split("_")[1];
            $("div[data-action='show-item-disc-"+id+"']").hide("fast");
        });

        $(document).on('change', '.qty_input', function() {
            generate_subtotal();
        });

        $(document).on('change', '.item_discount', function() {
            generate_subtotal();
        });

        $('input[name="discount_value"]').on('change', function() {
            generate_subtotal();
        }).trigger('change');



    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
