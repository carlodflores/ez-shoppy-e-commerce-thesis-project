<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	function __construct(){
	parent::__construct();
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
		$this->load->Model('Order_Model');
	}

    public function blogs($id) {
        if($id == "all") {
			$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
			$data['title'] = "EZ Dashboard - " . $store_name;
			$this->load->view('blogs/all_blogs_view', $data);
        } else {

        }
    }

	public function create_blog() {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$this->load->view('blogs/add_blog_view', $data);
	}

    public function pages($id) {
        if($id == "All") {

        } else {

        }
    }

}
