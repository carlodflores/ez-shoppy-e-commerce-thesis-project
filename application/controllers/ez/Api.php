<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {

    }

    public function product($action) {
		switch ($action) {
			case 'search':
				if(isset($_POST['req'])) {
					$q = $this->security->xss_clean($_POST['req']);
					if(count($this->Product_Model->get_product_by_name($q)) >= 1) {
						$arr = array();
						foreach ($this->Product_Model->get_product_by_name($q) as $key => $value) {
							foreach ($this->Product_Model->get_product_variant($value->product_id) as $key => $row) {
								$arr[] = array(
									"product_variant_id" => $row->product_variant_id,
									"product_title" => $value->product_title,
									"product_id" => $value->product_id,
									"product_variant_name" => $row->product_variant_name,
									"product_variant_price" => $row->product_variant_price,
									"product_variant_img" => $row->product_variant_img
								);
							}
						}

						echo json_encode($arr);
					}
				}
				break;

			case 'get':
				if(isset($_POST['pvid'])) {
					$data = $this->security->xss_clean($_POST['pvid']);
					$dataarr = explode('_', $data);
					$pvid = $dataarr[0];
					$pid = $dataarr[1];

					$pdata = $this->Product_Model->get_product_by_id($pid);
					$vdata = $this->Product_Model->get_variant_by_id($pvid);

					$result = array(
						"product_id" => $pdata->product_id,
						"product_title" => $pdata->product_title,
						"product_variant_id" => $vdata->product_variant_id,
						"product_variant_img" => $vdata->product_variant_img,
						"product_variant_name" => $vdata->product_variant_name,
						"product_variant_price" => $vdata->product_variant_price
					);

					echo json_encode($result);
				}
				break;

			default:
				# code...
				break;
		}
    }

	public function pos($action, $value) {

		header('Access-Control-Allow-Origin: *');

			switch ($action) {
				case 'get_all' :
						$var = $this->Product_Model->get_all_variants();

						$arr = array();
						foreach ($var as $key => $value) {

							$ii = $this->Inventory_Model->get_inventory_details_sku($value->product_variant_sku);

							$arr[] = array(
								"product_variant_id" => $value->product_variant_id,
								"product_title" => $this->Product_Model->get_product_by_id($value->product_id)->product_title,
								"product_id" => $value->product_id,
								"product_variant_name" => $value->product_variant_name,
								"product_variant_price" => $value->product_variant_price,
								"product_variant_img" => $value->product_variant_img,
								"product_inventory" => $ii->inventory_stocks
							);
						}

						echo json_encode($arr);
					break;
				case 'search':
					$q = $this->security->xss_clean($value);
					if(count($this->Product_Model->get_product_by_name($q)) >= 1) {
						$arr = array();
						foreach ($this->Product_Model->get_product_by_name($q) as $key => $value) {
							foreach ($this->Product_Model->get_product_variant($value->product_id) as $key => $row) {
									$ii = $this->Inventory_Model->get_inventory_details_sku($row->product_variant_sku);

								$arr[] = array(
									"product_variant_id" => $row->product_variant_id,
									"product_title" => $value->product_title,
									"product_id" => $value->product_id,
									"product_variant_name" => $row->product_variant_name,
									"product_variant_price" => $row->product_variant_price,
									"product_variant_img" => $row->product_variant_img,
									"product_inventory" => $ii->inventory_stocks

								);
							}
						}

						echo json_encode($arr);
					}
					break;

				case 'get':
					$data = $this->security->xss_clean($value);
					$pvid = $data;

					$vdata = $this->Product_Model->get_variant_by_id($pvid);
					$pdata = $this->Product_Model->get_product_by_id($vdata->product_id);


					$result = array(
						"product_id" => $pdata->product_id,
						"product_title" => $pdata->product_title,
						"product_variant_id" => $vdata->product_variant_id,
						"product_variant_img" => $vdata->product_variant_img,
						"product_variant_name" => $vdata->product_variant_name,
						"product_variant_price" => $vdata->product_variant_price
					);

					echo json_encode($result);
					break;

					case 'fulfill':
							$date = "%Y-%m-%d %H:%i:%s";
							$time = mdate($date, time());

							$data = urldecode($value);
							$data = substr($data, 1);
							$data = substr($data, 0, strlen($data)-1);
							$data = explode('],[', $data);

							$total = 0;
							foreach ($data as $key => $value) {
							$val = "";
							if(strpos('[', $value)) {
								$val = substr($value, 0);
							} else {
								$val = str_replace(']', '', $value);
							}

							$da = explode(',', $val);

							if(count($da) == 2) {
								$price = $this->Product_Model->get_variant_by_id($da[0])->product_variant_price;
								$total += intval($price)*intval($da[1]);
							}
							}

							$order_data = array(
								"customer_id" => "-1",
								"order_total" => $total,
								"order_subtotal" => $total,
								"order_note" => "POS SALE",
								"order_company" => "POS SALE",
								"order_phone" => "POS SALE",
								"order_address" => "POS SALE",
								"order_city" => "POS SALE",
								"order_zipcode" => "POS SALE",
								"date_modified" => $time,
								"date_created" => $time,
								"order_payment_status" => '2',
								"order_status" => '1'
							);

							$order_id = $this->Order_Model->insert_order($order_data);
							foreach ($data as $key => $value) {
							$val = "";
							if(strpos('[', $value)) {
								$val = substr($value, 0);
							} else {
								$val = str_replace(']', '', $value);
							}

							$da = explode(',', $val);

								if(count($da) == 2) {
									$order_content_data = array(
										"order_id" => $order_id,
										"product_variant_id" => $da[0],
										"order_content_qty" => $da[1],
										"order_content_discount" => "0"
									);

									$this->Order_Model->insert_order_content($order_content_data);
								}
							}


							foreach ($data as $key => $value) {
								$val = "";
								if(strpos('[', $value)) {
									$val = substr($value, 0);
								} else {
									$val = str_replace(']', '', $value);
								}

								$da = explode(',', $val);

								$v_data = $this->Product_Model->get_variant_by_id($da[0]);
								$inven_data = array(
									"inventory_stocks" => intval($this->Inventory_Model->get_inventory_details_sku($v_data->product_variant_sku)->inventory_stocks)-intval($da[1])
								);

								$this->Inventory_Model->update_stocks_by_sku($inven_data, $v_data->product_variant_sku, $v_data->product_id);
							}

							echo $order_id;

						break;

					case 'login':
						$value = urldecode($value);
						$creds = explode("_", $value);
						if (count($this->Account_Model->check_email($creds[0])) >= 1) {
							if(count($this->Account_Model->check_creds($creds[0], $creds[1])) >= 1) {
								echo "[". json_encode($this->Account_Model->check_creds($creds[0], $creds[1])) ."]";
							} else {
								echo 'null';
							}
						} else {
							echo 'null';
						}
					break;
				default:
					# code...
					break;
			}
    }

	public function customer($action) {
		switch($action) {
			case 'get_info_email':
				if(isset($_POST['uemail'])) {
					$email = $this->security->xss_clean($_POST['uemail']);
					echo json_encode($this->Customer_Model->get_cutomer_detail_by_email($email));
				}
			break;
		}
	}

	public function testing() {
		$this->load->view('email/order_confirmation');
	}

	// Please put a slash at the end of the path.
	public function image($action, $order_id) {
	    switch($action) {
			case 'upload_proof';
				$result = array();
				foreach($_FILES['file']['error'] as $key => $val) {
					if(0 < $val) {
					} else {
						$name =  uniqid() . $_FILES['file']['name'][$key];
						if(move_uploaded_file($_FILES['file']['tmp_name'][$key], 'img/proofs/' . $name)) {
							$result[] = $name;
						}
					}
				}

				$files = implode(',', $result);

				echo $files;

			break;
		}
	}

}

?>
