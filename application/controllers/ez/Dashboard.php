<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Etc/GMT+8');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {
		$data['title'] = "EZ Dashboard";
		$this->load->view('dashboard_view', $data);
    }

	public function inquiry($action, $id="") {
		switch ($action) {
			case 'info':
				$data['title'] = "EZ Dashboard";
				if(count($this->Customer_Model->get_inquiry_details($id)) >= 1) {

					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$this->form_validation->set_rules('txt_message', 'Reply Message','required');
					if($this->form_validation->run()) {
						$config = Array(
							'protocol' => 'smtp',
							'smtp_host' => 'ssl://smtp.googlemail.com',
							'smtp_port' => 465,
							'smtp_user' => $this->Option_Model->get_option_by_value('emailer')->option_value, // change it to yours
							'smtp_pass' => $this->Option_Model->get_option_by_value('emailer_password')->option_value, // change it to yours
							'mailtype' => 'html',
							'charset' => 'iso-8859-1',
							'wordwrap' => TRUE
						);

						$email_data = array(
							"date" => $time,
							"Message" => $this->input->post('txt_message')
						);

						$this->load->library('email', $config);
						$this->email->set_newline("\r\n");
						$this->email->from('no-reply@hairgeek.ph', 'HairGeek'); // change it to yours
						$this->email->to($this->Customer_Model->get_inquiry_details($id)->inquiry_email); // change it to yours
						$this->email->subject('HairGeek - Inquiry Reply');
						$this->email->message($this->load->view('email/message_email', $email_data, TRUE));
						$this->email->send();

						$reply_data = array(
							"inquiry_id" => $id,
							"inquiry_reply_message" => $this->input->post('txt_message'),
							"date_created" => $time
						);

						$this->Customer_Model->insert_inquiry_reply($reply_data);

						redirect(base_url() . 'ez/inquiry/info/' . $id . '/?success');
					}

					$data['ini'] = $this->Customer_Model->get_inquiry_details($id);
					$this->load->view('inquiry_details_view', $data);
				} else {
					$data['page_module'] = "Inquiry Details";
					$this->load->view('alerts/alert_notinquiry_view', $data);
				}
				break;

			default:
				$data['title'] = "EZ Dashboard";
				$this->load->view('all_inquiry_view', $data);
				break;
		}
	}
}

function shorten($n, $precision = 3) {
    if ($n < 1000000) {
        // Anything less than a million
        $n_format = number_format($n);
    } else if ($n < 1000000000) {
        // Anything less than a billion
        $n_format = number_format($n / 1000000, $precision) . 'M';
    } else {
        // At least a billion
        $n_format = number_format($n / 1000000000, $precision) . 'B';
    }

    return $n_format;
}
