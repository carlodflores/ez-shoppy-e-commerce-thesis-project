<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

<style media="screen">
    .pagination li {
        display:inline-block;
        padding:5px;
        background: #000;
        padding: 0px 10px;
        border-radius: 50%;
        margin: 5px;
        color: #fff;
    }

    .pagination li.active {
        background: #f62e9f;
        color: #fff;
    }
</style>
	<div class="sixteen colgrid main-content">
		<div class="row">
			<div class="thirteen columns">
				<div class="banner-slider-holder">
					  <ul class="slide-wrapper">
						  <li data-image="<?=base_url()?>assets/img/banner/sample5.jpg"></li>
						  <li data-image="<?=base_url()?>assets/img/banner/sample2.jpg"></li>
						  <li data-image="<?=base_url()?>assets/img/banner/sample.jpg"></li>
						  <li data-image="<?=base_url()?>assets/img/banner/sample.jpg"></li>
					  </ul>

					  <div class="control">
					  	<div class="left"><i class="fa fa-angle-left"></i></div>
					  	<div class="right"><i class="fa fa-angle-right"></i></div>
					  </div>
				  </div>
			</div>

			<div class="three columns feedback">
				<center>
					<div class="feedback-holder  top-position" data-image="<?=base_url()?>assets/img/banner/sample5.jpg">
						<h4 class="title" style="color: #1a2c46;">Recent FeedBack</h4>
						<?php if ($feedback->feedback_profile != ""): ?>
                            <img src="<?=base_url()?>assets/img/feedback/<?=$feedback->feedback_profile?>" width="80" alt="">
						<?php else: ?>
                            <img src="<?=base_url()?>img/profile-placeholder.jpg" width="80" alt="">
						<?php endif; ?>
						<div class="content">
							<?=$feedback->feedback_fullname?><br/>

							<p style="font-size: 12px;">
                                <?=substr($feedback->feedback_description, 0, 150)?>
							</p>


							<?=substr(str_replace('-', '/', $feedback->date_created), 0, 11)?>
						</div>
					</div>
				</center>
			</div>
		</div>

		<div class="row">
			<div class="sixteen columns">
				<div class="filter-holder">
					<div class="row">
						<ul class="items-control">
                            Products
							<a href="<?=base_url()?>?filter=price&dir=asc"><button><i class="fa fa-filter"> </i> Price High to Low</button></a>
                            <a href="<?=base_url()?>?filter=price&dir=desc"><button><i class="fa fa-filter"> </i> Price Low to High</button></a>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Product Starts -->
		<div id="test-list" class="row">
			<ul class="list product-holder">
                <?php foreach ($products as $key => $value): ?>
                    <?php
                        $product_data = $this->Product_Model->get_product_by_id($value->product_id);

                        $meta = $this->Product_Model->get_meta_by_id($value->product_id);
                        $pvname = $value->product_variant_name;
                        $ini = $this->Inventory_Model->get_inventory_details_sku($value->product_variant_sku);

                        $desc = "";

                        foreach ($meta as $metaKey => $metaRow) {
                            if(is_array($pvname)) {
                                $desc .= $metaRow->product_meta_keyword . ': ' . $pvname[$metaKey] . ' ';
                            } else {
                                $desc =  $metaRow->product_meta_keyword . ': ' . $pvname . ' ';
                            }
                        }

                        $img = $value->product_variant_img;
                        if(strpos($img, ',') >= 1) {
                            $img_ex = explode(',', $img);
                            $img = $img_ex[0];
                        }
                     ?>
                    <li class="product">
    					<div class="content">
    						  <center>
							  <a <?=$ini->inventory_stocks != 0 ? 'href=' . base_url() . 'product/desc/' . $value->product_variant_id . '/' . strtolower(str_replace(' ', '-', $product_data->product_title . ' ' . $value->product_variant_name)) . '/' : ''?>>
    							  <div class="product-img" style="position: relative;">
                                    <?php if ($ini->inventory_stocks <= 0): ?>
                                        <div style="background: red; color: #fff; padding: 15px; border-radius: 50%; font: 700 18px 'Open Sans', sans-serif; position: absolute; width: 80px; height: 80px; top: 50%; left: 50%; margin-top: -40px; margin-left: -40px;">
                                            SOLD OUT
                                        </div>
                                        <img src="<?=base_url()?>img/products/<?=$img?>" alt="">
                                    <?php else: ?>
                                        <img src="<?=base_url()?>img/products/<?=$img?>" alt="">
                                    <?php endif; ?>

    							  </div>
    							  <br/>
    							  <div class="product-details">
    								<span class="product-title"><?=$product_data->product_title?><br/>(<?=$desc?>)</span>
    								<div class="rotate-content">
    									<?php if ($ini->inventory_stocks >= 1): ?>
                                            <ul class="content-rotate">
        										<li>
            											<?php if ($value->product_variant_compare_price == "0.00"): ?>
            											    <span class="product-price">P <?=number_format($value->product_variant_price, 2)?></span>
            											<?php else: ?>
                                                            <span class="product-price sale">P <?=number_format($value->product_variant_price, 2)?></span>
                                                            <span class="product-price old">P <?=number_format($value->product_variant_compare_price, 2)?></span>
            											<?php endif; ?>

        										</li>
        										<li>
            										<a href="<?=base_url()?>cart/add/<?=$value->product_variant_id?>/?redirect_to=<?="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"><button><i class="fa fa-plus"> </i> Add to Cart</button></a>
        										</li>
        									</ul>
                                        <?php else: ?>
                                            <?php if ($value->product_variant_compare_price == "0.00"): ?>
                                                <span class="product-price">P <?=number_format($value->product_variant_price, 2)?></span>
                                            <?php else: ?>
                                                <span class="product-price sale">P <?=number_format($value->product_variant_price, 2)?></span>
                                                <span class="product-price old">P <?=number_format($value->product_variant_compare_price, 2)?></span>
                                            <?php endif; ?>
    									<?php endif; ?>
    								</div>
    							  </div>
                                  </a>
    						  </center>
    					</div>
    				</li>
                <?php endforeach; ?>
			</ul>
            <div class="clearfix"></div>
            <div class="row">
                <Br/><Br/>
                <center>
                    <ul class="pagination"></ul>
                </center>
            </div>
		</div>
		<!-- Product Ends -->

        <div class="row">
            <div class="three columns feedback">

                <ul class="bottom-position">
                    <?php foreach ($feedbacks as $key => $feedback): ?>
                        <li>
                            <center>
            					<div class="feedback-holder" data-image="<?=base_url()?>assets/img/banner/sample5.jpg">
            						<h4 class="title" style="color: #1a2c46;">Recent FeedBack</h4>
            						<?php if ($feedback->feedback_profile != ""): ?>
                                        <img src="<?=base_url()?>assets/img/feedback/<?=$feedback->feedback_profile?>" width="80" alt="">
            						<?php else: ?>
                                        <img src="<?=base_url()?>img/profile-placeholder.jpg" width="80" alt="">
            						<?php endif; ?>
            						<div class="content">
            							<?=$feedback->feedback_fullname?><br/>

            							<p style="font-size: 12px;">
                                            <?=substr($feedback->feedback_description, 0, 150)?>
            							</p>


            							<?=substr(str_replace('-', '/', $feedback->date_created), 0, 11)?>
            						</div>
            					</div>
            				</center>
                        </li>
                    <?php endforeach; ?>
                </ul>
			</div>
        </div>
	</div>

<?php
    $this->load->view('main/footer_view');
?>

<script type="text/javascript">
    var monkeyList = new List('test-list', {
        page: 10,
        plugins: [ ListPagination({}) ]
    });
</script>
