<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	function __construct(){
	parent::__construct();
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
		$this->load->Model('Order_Model');
		date_default_timezone_set('Asia/Manila');
	}

	public function visitors($date_start = "", $date_ends = "") {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		$this->load->view('reports/visitor_view', $data);
	}

	public function sales($divide = "") {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		switch ($divide) {
			case 'week':
				$this->load->view('reports/week_sales_view', $data);
				break;

			case 'month':
				$this->load->view('reports/month_sales_view', $data);
				break;

			case 'year':
				$this->load->view('reports/year_sales_view', $data);
				break;

			default:
				$this->load->view('reports/sales_view', $data);
				break;
		}
	}

	public function stockin($date_start = "", $date_ends = "") {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		$this->load->view('reports/transfer_log_view', $data);
	}

	public function test() {
		echo "testing";
	}

}
