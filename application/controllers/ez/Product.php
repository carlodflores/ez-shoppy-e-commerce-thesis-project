<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		if(count($this->Product_Model->get_all_products()) == 0) {
			$data['page_module'] = "<i class='icon-tag'> </i>All Products";
			$this->load->view('alerts/alert_noproduct_view', $data);
		} else {
			$data['products'] = $this->Product_Model->get_all_products();
			$this->load->view('products/all_product_view', $data);
		}


    }

    public function add() {

		$variant = array();
		$rules = array(
			array(
				'field'   => 'txt_prod_name',
				'label'   => ' Product Name',
				'rules'   => 'required|xss_clean|ucwords'),array(
				'field'   => 'txt_prod_desc',
				'label'   => ' Product Description',
				'rules'   => 'xss_clean'),array(
				'field'   => 'txt_prod_categ',
				'label'   => ' Product Category',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_brand',
				'label'   => ' Product Brand',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_tags',
				'label'   => ' Product Tags',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'file_prod_images_txt',
				'label'   => ' Product Images',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_price',
				'label'   => ' Product Price',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_price_compare',
				'label'   => ' Product Compare Price',
				'rules'   => 'xss_clean'),array(
				'field'   => 'chk_prod_show_online',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'chk_prod_show_local',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'date_prod_publish',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'time_prod_publish',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'txt_prod_sku',
				'label'   => ' Product SKU',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_barcode',
				'label'   => ' Product Barcode',
				'rules'   => 'xss_clean'),array(
				'field'   => 'txt_prod_stock_on_hand',
				'label'   => ' Product Stock',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'chk_prod_sell_option',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'txt_prod_weight',
				'label'   => ' Product Size',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_brand_temp',
				'label'   => ' Product Brand',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_categ_temp',
				'label'   => ' Product Category',
				'rules'   => 'required|xss_clean')
		);

		if($this->input->post('txt_variants') != '') {
			$variants = explode(',', $this->input->post('txt_variants'));
			foreach ($variants as $key => $value) {
				$r = array('field'   => $value, 'label'   => $value, 'rules'   => 'required|xss_clean');
				$rules[] = $r;
				$r = array('field'   => $value . '_tags', 'label'   => $value . '_tags', 'rules'   => 'required|xss_clean');
				$rules[] = $r;
			}
		}
		$this->form_validation->set_rules($rules);

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		if($this->form_validation->run()) {
			$type_id=$brand_id="";
			if($this->Product_Model->check_type_exist($this->input->post('txt_prod_categ')) >= 1
			|| $this->Product_Model->check_type_exist_name($this->input->post('txt_prod_categ')) >= 1) {
				if(count($this->Product_Model->get_type_info_name($this->input->post('txt_prod_categ'))) >= 1) {
					$type_id = $this->Product_Model->get_type_info_name($this->input->post('txt_prod_categ'))->product_type_id;
				} else {
					$type_id = $this->input->post('txt_prod_categ');
				}
			} else {
				$type_data = array(
					'product_type_name' => $this->input->post('txt_prod_categ_temp'),
					'date_created' => $time,
					'date_modified' => $time
				);

				$type_id = $this->Product_Model->insert_type($type_data);

			}

			if($this->Product_Model->check_brand_exist($this->input->post('txt_prod_brand')) >= 1
			|| $this->Product_Model->check_brand_exist_name($this->input->post('txt_prod_brand')) >= 1) {

				if(count($this->Product_Model->get_brand_info_name($this->input->post('txt_prod_brand'))) >= 1) {
					$brand_id = $this->Product_Model->get_brand_info_name($this->input->post('txt_prod_brand'))->product_type_id;
				} else {
					$brand_id = $this->input->post('txt_prod_brand');
				}
			} else {
				$brand_data = array(
					'product_brand_name' => $this->input->post('txt_prod_brand_temp'),
					'date_created' => $time,
					'date_modified' => $time
				);

				$brand_id = $this->Product_Model->insert_brand($brand_data);

			}

			if($this->input->post('txt_prod_tags') != "") {
				$tags = $this->input->post('txt_prod_tags');
				if(strpos($tags, ',')) {
					$prod_tag = explode(',', $tags);
					foreach ($prod_tag as $key => $value) {
						if($this->Product_Model->check_tag_exist($value) <= 0) {
							$tag_data = array(
								'product_tag_name' => $value,
								'date_created' => $time,
								'date_modified' => $time
							);

							$this->Product_Model->insert_tag($tag_data);
						}
					}
				} else {
					$tag_data = array(
						'product_tag_name' => $tags,
						'date_created' => $time,
						'date_modified' => $time
					);

					$this->Product_Model->insert_tag($tag_data);
				}
			}

			$prod_data = array(
				'product_title' => $this->input->post('txt_prod_name'),
				'product_price' =>  $this->input->post('txt_prod_price'),
				'product_compare_price' =>  $this->input->post('txt_prod_price_compare'),
				'product_description' =>  $this->input->post('txt_prod_desc'),
				'product_tags' =>  $this->input->post('txt_prod_tags'),
				'product_img' =>  $this->input->post('file_prod_images_txt'),
				'product_sku_id' =>  $this->input->post('txt_prod_sku'),
				'product_barcode' =>  $this->input->post('txt_prod_barcode'),
				'product_type_id' =>  $type_id,
				'product_brand_id' =>  $brand_id,
				'date_created' => $time,
				'date_modified' => $time
			);

			$product_id = $this->Product_Model->insert_product($prod_data);

			$gen_var = array();
			if($this->input->post('txt_variants') != '') {

				$variants = explode(',', $this->input->post('txt_variants'));
				foreach ($variants as $key => $value) {
					$meta_tags = $this->input->post($value . '_tags');
					$meta_data = array(
						'product_id' =>	$product_id,
						'product_meta_keyword' => $this->input->post($value),
						'product_meta_value' => $meta_tags,
						'product_meta_type' => 0
					);
					$gen_var[] = explode(',', $meta_tags);
					$this->Product_Model->insert_meta($meta_data);
				}
			}

			$result = allPossibleCases($gen_var);

			foreach($result as $key => $value) {

				$var_name = str_replace(' ', '_', $value);
				if($this->input->post($var_name. '_box') !== null) {
					$var_data = array(
						'product_id' =>	$product_id,
						'product_variant_name' => $value,
						'product_variant_price' => $this->input->post($var_name . "_price"),
						'product_variant_sku' => $this->input->post($var_name . "_sku") ,
						'product_variant_barcode' => $this->input->post($var_name . "_barcode"),
						'product_variant_img' => $this->input->post('file_prod_images_txt'),
						'product_variant_weight' =>  $this->input->post('txt_prod_weight')
					);
					$this->Product_Model->insert_variant($var_data);

					$in_data = array(
						'product_id' => $product_id,
						'inventory_sku' => $this->input->post($var_name . "_sku"),
						'inventory_stocks' => $this->input->post('txt_prod_stock_on_hand')
					);

					$this->Inventory_Model->insert_item_sku($in_data);
				}
			}

			redirect(base_url() . 'ez/product/manage/variant/'. $product_id .'/');
		}

		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		$data['types'] = $this->Product_Model->get_all_types();
		$data['brands'] = $this->Product_Model->get_all_brands();
		$this->load->view('products/add_product_view', $data);
    }

	public function edit($product_id) {
		$variant = array();
		$rules = array(
			array(
				'field'   => 'txt_prod_name',
				'label'   => ' Product Name',
				'rules'   => 'required|xss_clean|ucwords'),array(
				'field'   => 'txt_prod_desc',
				'label'   => ' Product Description',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_categ',
				'label'   => ' Product Category',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_brand',
				'label'   => ' Product Brand',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_tags',
				'label'   => ' Product Tags',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'file_prod_images_txt',
				'label'   => ' Product Images',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_price',
				'label'   => ' Product Price',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_price_compare',
				'label'   => ' Product Compare Price',
				'rules'   => 'xss_clean'),array(
				'field'   => 'chk_prod_show_online',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'chk_prod_show_local',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'date_prod_publish',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'time_prod_publish',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'txt_prod_barcode',
				'label'   => ' Product Barcode',
				'rules'   => 'xss_clean'),array(
				'field'   => 'chk_prod_sell_option',
				'label'   => '',
				'rules'   => 'xss_clean'),array(
				'field'   => 'txt_prod_weight',
				'label'   => ' Product Weight',
				'rules'   => 'xss_clean'),array(
				'field'   => 'txt_prod_brand_temp',
				'label'   => ' Product Brand',
				'rules'   => 'required|xss_clean'),array(
				'field'   => 'txt_prod_categ_temp',
				'label'   => ' Product Category',
				'rules'   => 'required|xss_clean')
		);

		$this->form_validation->set_rules($rules);

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		if($this->form_validation->run()) {
			$type_id=$brand_id="";
			if($this->Product_Model->check_type_exist($this->input->post('txt_prod_categ')) >= 1
			|| $this->Product_Model->check_type_exist_name($this->input->post('txt_prod_categ')) >= 1) {
				if(count($this->Product_Model->get_type_info_name($this->input->post('txt_prod_categ'))) >= 1) {
					$type_id = $this->Product_Model->get_type_info_name($this->input->post('txt_prod_categ'))->product_type_id;
				} else {
					$type_id = $this->input->post('txt_prod_categ');
				}
			} else {
				$type_data = array(
					'product_type_name' => $this->input->post('txt_prod_categ_temp'),
					'date_created' => $time,
					'date_modified' => $time
				);

				$type_id = $this->Product_Model->insert_type($type_data);

			}

			if($this->Product_Model->check_brand_exist($this->input->post('txt_prod_brand')) >= 1
			|| $this->Product_Model->check_brand_exist_name($this->input->post('txt_prod_brand')) >= 1) {

				if(count($this->Product_Model->get_brand_info_name($this->input->post('txt_prod_brand'))) >= 1) {
					$brand_id = $this->Product_Model->get_brand_info_name($this->input->post('txt_prod_brand'))->product_type_id;
				} else {
					$brand_id = $this->input->post('txt_prod_brand');
				}
			} else {
				$brand_data = array(
					'product_brand_name' => $this->input->post('txt_prod_brand_temp'),
					'date_created' => $time,
					'date_modified' => $time
				);

				$brand_id = $this->Product_Model->insert_brand($brand_data);

			}

			if($this->input->post('txt_prod_tags') != "") {
				$tags = $this->input->post('txt_prod_tags');
				if(strpos($tags, ',')) {
					$prod_tag = explode(',', $tags);
					foreach ($prod_tag as $key => $value) {
						if($this->Product_Model->check_tag_exist($value) <= 0) {
							$tag_data = array(
								'product_tag_name' => $value,
								'date_created' => $time,
								'date_modified' => $time
							);

							$this->Product_Model->insert_tag($tag_data);
						}
					}
				} else {
					$tag_data = array(
						'product_tag_name' => $tags,
						'date_created' => $time,
						'date_modified' => $time
					);

					$this->Product_Model->insert_tag($tag_data);
				}
			}

			$prod_data = array(
				'product_title' => $this->input->post('txt_prod_name'),
				'product_price' =>  $this->input->post('txt_prod_price'),
				'product_compare_price' =>  $this->input->post('txt_prod_price_compare'),
				'product_description' =>  $this->input->post('txt_prod_desc'),
				'product_tags' =>  $this->input->post('txt_prod_tags'),
				'product_img' =>  $this->input->post('file_prod_images_txt'),
				'product_barcode' =>  $this->input->post('txt_prod_barcode'),
				'product_type_id' =>  $type_id,
				'product_brand_id' =>  $brand_id,
				'date_modified' => $time
			);

			$product_id = $this->Product_Model->update_product($prod_data, $product_id);


			redirect(base_url() . 'ez/product/?success=edit_product');
		}

		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		if(count($this->Product_Model->get_product_by_id($product_id)) == 0) {
			$data['page_module'] = "<i class='icon-pencil'> </i>Edit Product";
			$data['page_back'] = base_url() . 'product/';
			$this->load->view('alerts/alert_notproduct_view', $data);
		} else {
			$data['types'] = $this->Product_Model->get_all_types();
			$data['brands'] = $this->Product_Model->get_all_brands();
			$data['product'] = $this->Product_Model->get_product_by_id($product_id);
			$this->load->view('products/edit_product_view', $data);
		}
	}

	public function manage($action, $product_id) {
		switch($action) {
			case 'variant';
				$data['title'] = "EZ Dashboard";
				if(count($this->Product_Model->get_all_products()) == 0) {
					$data['page_module'] = "<i class='icon-flight'> </i>Transfer";
					$this->load->view('alerts/alert_noproduct_view', $data);
				} else {
					$data['variants'] = $this->Product_Model->get_product_variant($product_id);
					$data['pid'] = $product_id;
					$this->load->view('products/all_variant_view', $data);
				}
			break;

			case 'edit':

				$this->form_validation->set_rules('txt_variant_price', 'Price ' , 'required|xss_clean');
				$this->form_validation->set_rules('txt_variant_capital', 'Capital ' , 'required|xss_clean');
				$this->form_validation->set_rules('txt_variant_weight', 'Weight ' , 'xss_clean');
				$this->form_validation->set_rules('file_prod_images_txt', 'Images' , 'required|xss_clean');

				if($this->form_validation->run()) {
					$var_data = array(
						"product_variant_price" => $this->input->post('txt_variant_price'),
						"product_variant_capital" => $this->input->post('txt_variant_capital'),
						"product_variant_img" => $this->input->post('file_prod_images_txt'),
						"product_variant_weight" => $this->input->post('txt_variant_weight'),
						"product_variant_critical_value" => $this->input->post('txt_critical_value')
					);

					$this->Product_Model->update_variant($var_data, $product_id);

					redirect(base_url() . 'ez/product/manage/variant/'. $this->Product_Model->get_variant_by_id($product_id)->product_id);
				}

				$data['title'] = "EZ Dashboard";
				if(count($this->Product_Model->get_variant_by_id($product_id)) == 0) {
					$data['page_module'] = "<i class='icon-flight'> </i>Transfer";
					$data['page_back'] = base_url() . 'product/';
					$this->load->view('alerts/alert_notproduct_view', $data);
				} else {
					$data['variant'] = $this->Product_Model->get_variant_by_id($product_id);
					$this->load->view('products/edit_variant_view', $data);
				}
			break;

			case 'embed' :
				$data['title'] = "EZ Dashboard";
				$data["pvid"] = $product_id;
				$this->load->view('products/embed_view', $data);
			break;
		}

	}

	public function transfer($action = "", $id="") {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		switch($action) {
			case '':
				if(count($this->Product_Model->get_all_products()) == 0) {
					$data['page_module'] = "<i class='icon-flight'> </i>Transfer";
					$this->load->view('alerts/alert_noproduct_view', $data);
				} else {
					$data['products'] = $this->Product_Model->get_all_products();
					$this->load->view('products/transfer_product_view', $data);
				}
			break;

			case 'edit':
				$data['pid'] = $id;
				$t_rules = array(
					array(
						'field' => 'txt_prod_supplier_temp',
						'label' => 'Supplier',
						'rules' => 'required|xss_clean|ucwords'
					)
				);
				$this->form_validation->set_rules($t_rules);
				if($this->form_validation->run()) {

					$supplier_id = 0;

					if($this->Product_Model->check_supplier_exist_name($this->input->post('txt_prod_supplier_temp')) >= 1) {
						$supplier_id = $this->Product_Model->get_supplier_info_name($this->input->post('txt_prod_supplier_temp'))->product_supplier_id;
					} else {
						$supplier_data = array(
							"product_supplier_id" => $this->input->post('txt_prod_supplier_temp'),
							"date_created" => $time,
							"date_modified" => $time
						);

						$supplier_id = $this->Product_Model->insert_supplier($supplier_data);
					}

					foreach($this->Product_Model->get_product_variant($id) as $row) {
						if($this->input->post($row->product_variant_id . '_name') != "") {
							$stocks_val = $this->input->post($row->product_variant_id . '_name');

							$tran_data = array(
								'inventory_sku' => $row->product_variant_sku,
								'inventory_transfer_stocks' => $stocks_val,
								'supplier_id' => $supplier_id,
								'date_created' => $time,
								'date_modified' => $time,
								'inventory_transfer_status' => 1
 							);

							$this->Inventory_Model->insert_transfer($tran_data);

							$inven_data = array(
								'inventory_stocks' => intval($this->Inventory_Model->get_inventory_details_sku($row->product_variant_sku)->inventory_stocks)+intval($stocks_val)
							);


							$this->Inventory_Model->update_stocks_by_sku($inven_data, $row->product_variant_sku, $id);
						}
					}

					redirect(base_url() . 'ez/product/transfer/?success=transfer');
				}

				$data['suppliers'] = $this->Product_Model->get_all_suppliers();
				$this->load->view('products/edit_transfer_view', $data);
			break;
		}
	}

	public function _inventory() {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;
		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		$data['products'] = $this->Product_Model->get_all_products();
		$data['variants'] = $this->Product_Model->get_all_variants();
		$this->load->view('products/all_inventory_view', $data);
	}

	public function playground() {
		$csv = array_map('str_getcsv', file('C:\Users\Carlo\Desktop\Med.csv'));
		foreach ($csv as $key => $value) {
			echo $value[0] . " (". $value[1] .")<Br/>";
		}
	}


	public function image($action) {
	    switch($action) {
			case 'upload';
				$result = array();
				foreach($_FILES['file']['error'] as $key => $val) {
					if(0 < $val) {
					} else {
						$name =  uniqid() . $_FILES['file']['name'][$key];
						if(move_uploaded_file($_FILES['file']['tmp_name'][$key], 'img/products/' . $name)) {
							$result[] = $name;
						}
					}
				}

				if(count($result) >= 1) {
					echo implode(',', $result);
				}
			break;

			case 'uploadvariant';
				$result = array();
				foreach($_FILES['file']['error'] as $key => $val) {
					if(0 < $val) {
					} else {
						$name =  uniqid() . $_FILES['file']['name'][$key];
						if(move_uploaded_file($_FILES['file']['tmp_name'][$key], 'img/products/' . $name)) {
							$result[] = $name;
							$var_data = array(
								"product_variant_img" => $name
							);
							$pid = $this->security->xss_clean($_POST['pid']);
							$this->Product_Model->update_variant($var_data, $pid);
							echo "OK";
						}
					}
				}
			break;

			case 'update_stock':
					$pid = $this->security->xss_clean($_POST['pid']);
					$qty = $this->security->xss_clean($_POST['qty']);

					$inven_data = array(
						'inventory_stocks' => $qty
					);

					$this->Inventory_Model->update_stocks_by_sku($inven_data, $this->Product_Model->get_variant_by_id($pid)->product_variant_sku, $this->Product_Model->get_variant_by_id($pid)->product_id);
					echo $pid."_".$qty;
			break;

		}
	}

	public function sale($page = "", $pvid="") {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		switch ($page) {
			case 'list':
				$data['products'] = $this->Product_Model->get_all_products();
				$data['variants'] = $this->Product_Model->get_all_variants();
				$this->load->view('products/list_item_sale_view', $data);
				break;

			case 'create':

				$info = $this->Product_Model->get_variant_by_id($pvid);

				$this->form_validation->set_rules('sale_price', ' Price', 'required|xss_clean');
				$this->form_validation->set_rules('sale_start_date', ' Start Date', 'required|xss_clean');
				$this->form_validation->set_rules('sale_start_time', 'Start Time', 'required|xss_clean');
				$this->form_validation->set_rules('sale_end_date', 'End Date', 'required|xss_clean');
				$this->form_validation->set_rules('sale_end_time', 'End Time', 'required|xss_clean');
				$this->form_validation->set_rules('discount_type', ' Type', 'required|xss_clean');

				if($this->form_validation->run()) {
					$sale_data = array(
						"product_variant_id" => $pvid,
						"product_sale_start" => $this->input->post('sale_start_date') . ' ' . $this->input->post('sale_start_time'),
						"product_sale_expires" => $this->input->post('sale_start_date') . ' ' . $this->input->post('sale_start_time'),
						"date_created" => $time
					);

					$this->Product_Model->insert_sale($sale_data);

					$new_price = 0;
					if($this->input->post('discount_type') == 1) {
						$new_price = $info->product_variant_price - $this->input->post('sale_price');
					} else {
						$new_price = $info->product_variant_price - ($info->product_variant_price*($this->input->post('sale_price')/100));
					}

					$variant_data = array(
						"product_variant_compare_price" => $info->product_variant_price,
						"product_variant_price" => $new_price
					);

					$this->Product_Model->update_variant($variant_data, $pvid);

					redirect(base_url() . 'ez/product/sale/?add=success');
				}

				$data['pvid'] = $pvid;
				$this->load->view('products/add_item_sale_view', $data);
				break;

			case 'end':
				$si = $this->Product_Model->get_sale_details($pvid);
				$vi = $this->Product_Model->get_variant_by_id($si->product_variant_id);
				$product_data = array(
					"product_variant_price" => $vi->product_variant_compare_price,
					"product_variant_compare_price" => "0.00"
				);

				$this->Product_Model->update_variant($product_data, $vi->product_variant_id);

				$sale_data = array(
					"product_sale_status" => "0"
				);
				$this->Product_Model->update_sale($sale_data, $si->product_sale_id);

				redirect(base_url() . 'ez/product/sale/');
				break;
			default:
				$data['products'] = $this->Product_Model->get_all_products();
				$this->load->view('products/all_item_sale_view', $data);
				break;
		}
	}

	public function purchase($action = "", $id = "") {

		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		switch ($action) {
			case 'add':

				$this->form_validation->set_rules('variants_added', ' Products', 'required|xss_clean');
				$this->form_validation->set_rules('txt_supplier', ' Supplier', 'required|xss_clean');
				$this->form_validation->set_rules('txt_order_note', ' Order Note', 'xss_clean');

				if($this->form_validation->run()) {
					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());
					$product_supplier_id = "";

					if(count($this->Product_Model->check_supplier_exist_name($this->input->post('txt_supplier'))) >= 1) {
						$product_supplier_id = $this->Product_Model->check_supplier_exist_name($this->input->post('txt_supplier'))->product_supplier_id;
					} else {
						$type_data = array(
							'product_supplier_name' => $this->input->post('txt_supplier'),
							'date_created' => $time,
							'date_modified' => $time
						);

						$product_supplier_id = $this->Product_Model->insert_supplier($type_data);
					}

					$purchase_data = array(
						"product_supplier_id" => $product_supplier_id,
						"purchase_order_note" => $this->input->post('txt_order_note'),
						"date_created" => $time
					);

					$purchase_order_id = $this->Order_Model->insert_purchase($purchase_data);


					$vid = explode(',', $this->input->post('variants_added'));
					$p_var = array();

					foreach ($vid as $key => $id) {
						if($this->input->post('qty_'. $id) == "")
							continue;

						$order_content_data = array(
							"purchase_order_id" => $purchase_order_id,
							"product_variant_id" => $id,
							"purchase_order_content_qty" => $this->input->post('qty_'. $id)
						);

						$this->Order_Model->insert_purchase_order_content($order_content_data);
					}

					redirect(base_url() . 'ez/product/purchase/?sucess='. md5("created"));
				}

				$this->load->view('purchase/add_purchase_order', $data);
				break;

			case 'details':
				$data['id'] = $id;
				$data['info'] = $this->Order_Model->get_purchase_details($id);
				$this->load->view('purchase/info_purchase_order', $data);
				break;

			case 'receive':
				$info = $this->Order_Model->get_purchase_details($id);
				$content = $this->Order_Model->get_purchase_content($id);

				if($info->purchase_order_status != 1) {
					redirect(base_url(). 'ez/product/purchase/?error='.md5("accessthroughurl"));
				}

				foreach ($content as $key => $value) {
					$v_data = $this->Product_Model->get_variant_by_id($value->product_variant_id);
					$inven_data = array(
						"inventory_stocks" => intval($this->Inventory_Model->get_inventory_details_sku($v_data->product_variant_sku)->inventory_stocks)+intval($value->purchase_order_content_qty)
					);

					$this->Inventory_Model->update_stocks_by_sku($inven_data, $v_data->product_variant_sku, $v_data->product_id);
				}

				$purchase_data = array(
					"purchase_order_status" => '0'
				);

				$this->Order_Model->update_purchase_order($id, $purchase_data);

				redirect(base_url(). 'ez/product/purchase/');

				break;

			case 'backorder':
				$info = $this->Order_Model->get_purchase_details($id);
				$content = $this->Order_Model->get_purchase_content($id);
				$purchase_data = array(
					"purchase_order_status" => '2'
				);

				$this->Order_Model->update_purchase_order($id, $purchase_data);

				redirect(base_url(). 'ez/product/purchase/');
				break;

			default:
				$this->load->view('purchase/all_purchase_order', $data);
				break;
		}
	}

}

function allPossibleCases($arr) {
	if(is_array($arr)) {
		if (count($arr) == 1) {
			return $arr[0];
		} else {
			$result = array();
			$allCasesOfRest = allPossibleCases(array_slice($arr, 1));  // recur with the rest of array
			for ($i = 0; $i < count($allCasesOfRest); $i++) {
				for ($j = 0; $j < count($arr[0]); $j++) {
					array_push($result, $arr[0][$j] ." ". $allCasesOfRest[$i]);
				}
			}
			return $result;
		}
	}
}
