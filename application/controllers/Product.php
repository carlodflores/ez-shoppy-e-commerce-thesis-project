<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function desc($pvid = "", $title = "") {
        if(!empty($pvid)) {
            if(count($this->Product_Model->get_variant_by_id($pvid)) >= 1) {
                $var = $this->Product_Model->get_variant_by_id($pvid);
                $product_data = $this->Product_Model->get_product_by_id($var->product_id);

                $slug = strtolower(str_replace(' ','-', $product_data->product_title . ' '. $var->product_variant_name));

                if($title != $slug) {
                    redirect(base_url() . 'product/index/' . $pvid . '/' . $slug . '/');
                } else {

                    $data['v'] = $var;
                    $data['p'] = $product_data;
                    $data['page_title'] = "Hair Geek";
                    $this->load->view('main/product/product_view', $data);
                }

            } else {
                redirect(base_url());
            }
        } else {
            redirect(base_url());
        }
    }

}
