<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

	// Product
		function insert_product($data) {
			$this->db->insert('cs_products', $data);
			return $this->db->insert_id();
		}

		function get_all_products() {
			$this->db->select('*');
			$this->db->from('cs_products as a');
			$this->db->join('cs_product_type as b', 'a.product_type_id = b.product_type_id');
			$this->db->join('cs_product_brand as c', 'a.product_brand_id = c.product_brand_id');
			$result = $this->db->get();
			return $result->result();
		}

		function get_product_by_id($product_id) {
			$this->db->select('*');
			$this->db->from('cs_products as a');
			$this->db->join('cs_product_type as b', 'a.product_type_id = b.product_type_id');
			$this->db->join('cs_product_brand as c', 'a.product_brand_id = c.product_brand_id');
			$this->db->join('cs_inventory as d', 'a.product_id = d.product_id');
			$this->db->where('a.product_id', $product_id);
			$result = $this->db->get();
			return $result->row();
		}

		function get_product_related($category, $id) {
			$this->db->select('*');
			$this->db->from('cs_products as a');
			$this->db->join('cs_product_type as b', 'a.product_type_id = b.product_type_id');
			$this->db->join('cs_product_variants as e', 'a.product_id = e.product_id');
			$this->db->where('b.product_type_id', $category);
			$this->db->where("e.product_variant_id !=", $id);
			$this->db->limit(5);
			$result = $this->db->get();
			return $result->result();
		}

		function get_product_by_name($product_title) {
			$this->db->select('*');
			$this->db->from('cs_products as a');
			$this->db->join('cs_product_type as b', 'a.product_type_id = b.product_type_id');
			$this->db->join('cs_product_brand as c', 'a.product_brand_id = c.product_brand_id');
			$this->db->like('a.product_title', 	$product_title);
			$result = $this->db->get();
			return $result->result();
		}

		function update_product($data, $product_id) {
			$this->db->where('product_id', $product_id);
			$this->db->update('cs_products', $data);
			return $this->db->affected_rows();
		}

		function search_product($keyword) {
			$this->db->select('*');
			$this->db->from('cs_products as a');
			$this->db->join('cs_product_variants as b', 'a.product_id = b.product_id');
			$this->db->join('cs_product_type as c', 'a.product_type_id = c.product_type_id');
			$this->db->join('cs_product_brand as d', 'a.product_brand_id = d.product_brand_id');
			$this->db->like('a.product_title', 	$keyword);
			$this->db->or_like('a.product_tags', $keyword);
			$this->db->or_like('d.product_brand_name', $keyword);
			$this->db->or_like('c.product_type_name', $keyword);
			$result = $this->db->get();
			return $result->result();
		}

	// Variants
		function get_all_variants() {
			$result = $this->db->get('cs_product_variants')->result();
			return $result;
		}

		function get_all_variants_shop($order_by = "", $dir = "") {
			if($order_by != ""){
				switch ($order_by) {
					case 'price':
						$this->db->order_by('product_variant_price', $dir);
						break;

					default:
						# code...
						break;
				}
			}
			$result = $this->db->get('cs_product_variants')->result();
			return $result;
		}

		function insert_variant($data) {
			$this->db->insert('cs_product_variants', $data);
			return $this->db->insert_id();
		}

		function get_product_variant($product_id) {
			$this->db->where('product_id', $product_id);
			$result = $this->db->get('cs_product_variants')->result();
			return $result;
		}

		function get_variant_by_id($variant_id) {
			$this->db->where('product_variant_id', $variant_id	);
			$result = $this->db->get('cs_product_variants')->row();
			return $result;
		}

		function update_variant($data, $variant_id) {
			$this->db->where('product_variant_id', $variant_id);
			$this->db->update('cs_product_variants', $data);
			return $this->db->affected_rows();
		}

		function update_variant_by_sku($data, $sku) {
			$this->db->where('product_variant_sku', $sku);
			$this->db->update('cs_product_variants', $data);
			return $this->db->affected_rows();
		}

	// Meta
		function insert_meta($data) {
			$this->db->insert('cs_product_meta', $data);
			return $this->db->insert_id();
		}

		function get_meta_by_id($product_id) {
			$this->db->where('product_id', $product_id);
			$result = $this->db->get('cs_product_meta')->result();
			return $result;
		}

	// Types
		function get_all_types() {
			return $this->db->get('cs_product_type')->result();
		}

		function check_type_exist($type)	{
			$this->db->where('product_type_id', $type);
			$result = $this->db->get('cs_product_type');
			return count($result->row());
		}

		function check_type_exist_name($type)	{
			$this->db->where('product_type_name', $type);
			$result = $this->db->get('cs_product_type');
			return count($result->row());
		}

		function insert_type($data) {
			$this->db->insert('cs_product_type', $data);
			return $this->db->insert_id();
		}

		function get_type_info_name($type) {
			$this->db->where('product_type_name', $type);
			$result = $this->db->get('cs_product_type');
			return $result->row();
		}

	// Brand
		function get_all_brands() {
			return $this->db->get('cs_product_brand')->result();
		}

		function check_brand_exist_name($brand)	{
			$this->db->where('product_brand_name', $brand);
			$result = $this->db->get('cs_product_brand');
			return count($result->row());
		}

		function check_brand_exist($brand)	{
			$this->db->where('product_brand_id', $brand);
			$result = $this->db->get('cs_product_brand');
			return count($result->row());
		}

		function insert_brand($data) {
			$this->db->insert('cs_product_brand', $data);
			return $this->db->insert_id();
		}

		function get_brand_info_name($brand) {
			$this->db->where('product_brand_name', $brand);
			$result = $this->db->get('cs_product_brand');
			return $result->row();
		}

	// Supplier
		function get_all_suppliers() {
			return $this->db->get('cs_product_supplier')->result();
		}

		function check_supplier_exist_name($brand)	{
			$this->db->where('product_supplier_name', $brand);
			$result = $this->db->get('cs_product_supplier');
			return $result->row();
		}

		function check_supplier_exist($brand)	{
			$this->db->where('product_supplier_id', $brand);
			$result = $this->db->get('cs_product_supplier');
			return $result->row();
		}

		function insert_supplier($data) {
			$this->db->insert('cs_product_supplier', $data);
			return $this->db->insert_id();
		}

		function get_supplier_info_name($brand) {
			$this->db->where('product_supplier_name', $brand);
			$result = $this->db->get('cs_product_supplier');
			return $result->row();
		}


	// Tags

		function get_all_tags() {
			return $this->db->get('cs_product_tag')->result();
		}

		function check_tag_exist($tag) {
			$this->db->where('product_tag_name', $tag);
			$result = $this->db->get('cs_product_tag');
			return count($result->row());
		}

		function insert_tag($data) {
			$this->db->insert('cs_product_tag', $data);
			return $this->db->insert_id();
		}

	// Sale

		function insert_sale($data) {
			$this->db->insert('cs_product_sale', $data);
			return $this->db->insert_id();
		}

		function update_sale($data, $id) {
			$this->db->where('product_sale_id', $id);
			$this->db->update('cs_product_sale', $data);
			return $this->db->affected_rows();
		}

		function get_sale_details($id) {
			$this->db->where('product_sale_id', $id);
			return $this->db->get('cs_product_sale')->row();
		}
}
