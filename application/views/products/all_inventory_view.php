<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Inventory</h3><a href="<?=base_url(). 'ez/product/transfer/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">

                </div>
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <center><h2>Manage Inventory</h2>
                        <p>Add / Set Stocks of your Product Variants</p></center>
                        <Br/>
                        <form method="post" class="append">
                            <span class="txt-label">Product Name</span><br/>
                            <input type="text" class="input" name="search_temp" placehoder="Search Product" list="search-datalist">
                            <datalist id="search-datalist">
                                <?php foreach($products as $row): ?>
                                    <option data-value="<?=$row->product_id?>" value="<?=ucwords(strtolower($row->product_title))?>" />
                                <?php endforeach; ?>
                            </datalist>
                            <div class="medium primary btn"><i class="icon-search" style="color: #fff;"></i></div>
                        </form>
                    </div>

                    <div class="centered push_one twelve columns">
                        <Br/>

                        <table class="paginate" max="10">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th></th>
                                    <th><center><span class="product-title">Product Variant</span></center></th>
                                    <th><center><span class="product-title">SKU</span></center></th>
                                    <th><center><span class="product-title">Quantity</span></center></th>
                                    <th><center><span class="product-title">Update Quantity</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach($variants as $row): ?>
                            <tr class="stocks-row <?=$row->product_variant_id?>_holder"  data-tag="<?=$this->Product_Model->get_product_by_id($row->product_id)->product_title?>">
                                <td>
                                    <div class="upload-holder">
                                        <input type='file' data-value="<?=$row->product_variant_id?>" name="file_prod_images" id="inputFile" multiple/>
                                        <?php if($row->product_variant_img != ""): ?>
                                            <?php if(strpos($row->product_variant_img, ',')): ?>
                                                <?php $img = explode(',',$row->product_variant_img); ?>
                                                <img src="<?=base_url() . 'img/products/' . $img[0]?>" alt="" width="50"/>
                                            <?php else: ?>
                                                <img src="<?=base_url() . 'img/products/' .$row->product_variant_img?>" alt="" width="50"/>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                    </div>
                                </td>

                                <td>
                                    <span class="product-title"><?=$this->Product_Model->get_product_by_id($row->product_id)->product_title?></span><Br/>
                                    <span class="txt-label" style="color: #000;"><?=str_replace(' ', ' / ', $row->product_variant_name)?></span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$row->product_variant_sku?></span><Br/>
                                    <span class="txt-label">Sku</span>
                                </td>

                                <td>
                                    <span class="product-title <?=$row->product_variant_id?>_content" data-value="<?=$this->Inventory_Model->get_inventory_details_sku($row->product_variant_sku)->inventory_stocks?>"><?=$this->Inventory_Model->get_inventory_details_sku($row->product_variant_sku)->inventory_stocks?></span><Br/>
                                    <span class="txt-label">Quantity</span>
                                </td>

                                <td class="field">
                                    <span class="prepend append <?=$row->product_variant_id?>_class" style="margin-right: 10px;">
                                        <div class="medium primary btn" data-tag="inventory_option" data-value="<?=$row->product_variant_id?>_1" style="font-size: 12px;"><a>Add</a></div>
                                        <div class="medium default btn" data-tag="inventory_option" data-value="<?=$row->product_variant_id?>_2" style="font-size: 12px;"><a>Set</a></div>
                                    </span>
                                    <span><input type="number" min="0" class="input narrow value_holder" data-action="1" name="<?=$row->product_variant_id?>_value" value=""></span>
                                    <div class="medium default btn variant_saver" data-value="<?=$row->product_variant_id?>_saver"><i class="icon-floppy"></i></div>
                                </td>

                            </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>

        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>

    <div class="inventory-alert" style="position: fixed; display: none; bottom: 0; width: 100%; padding: 20px; color: #fff; font: 700 18px 'Open Sans', sans-serif; background: rgba(0, 0, 0, 0.5);">
        <center>Inventory has been successfully updated</center>
    </div>
</div>
<script type="text/javascript">
    $(function() {

        $("div[data-tag]").click(function() {
            var data = $(this).attr('data-value').split('_'),
                pid = data[0],
                action = data[1];

                $("."+pid+"_class > div[data-tag]:eq("+(action-1)+")").removeClass('default').addClass('primary');
                $("."+pid+"_class > div[data-tag]:not(':eq("+(action-1)+")')").removeClass('primary').addClass('default');

                generate_new_quantity(pid, action);
        });

        $('.value_holder').on('keyup', function() {
            var pid = $(this).attr('name').split('_')[0],
                action = ($(this).attr('data-action') == "")? 1 : $(this).attr('data-action');

            generate_new_quantity(pid, action);
        });

        $(".variant_saver").click(function() {
            var form_data = new FormData();
            var pid = $(this).attr('data-value').split('_')[0],
                act = ($('input[name="'+ pid +'_value"]').attr('data-action') == "")? 1 : $('input[name="'+ pid +'_value"]').attr('data-action'),
                temp = $("." + pid + "_content").attr('data-value'),
                qty = (act == 1)? (+temp + +$('input[name="'+ pid +'_value"]').val()) : $('input[name="'+ pid +'_value"]').val();

            console.log(qty + " " + act);
            form_data.append('pid', pid);
            form_data.append('qty', qty);

            $.ajax({
                xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        console.log(percentComplete);
                        $('.progress').css({
                            width: percentComplete * 100 + '%'
                        });
                        if (percentComplete === 1) {
                            $('.progress').addClass('hide');
                        }
                    }
                }, false);
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        console.log(percentComplete);
                        $('.progress').css({
                            width: percentComplete * 100 + '%'
                        });
                    }
                }, false);
                return xhr;
                },
                url: '<?=base_url()?>ez/product/image/update_stock/', // point to server-side PHP script
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if(response != "") {
                        var pid = response.split("_")[0],
                            qty = response.split("_")[1];

                         $("." + pid + "_content").text(qty);
                         $('input[name="'+ pid +'_value"]').val("");
                         $("." + pid + "_content").attr('data-value', qty);

                         $(".inventory-alert").slideToggle("fast").delay('1000').slideToggle("fast");
                    }
                }
            });
        });

        function generate_new_quantity(pid, action) {
            $('input[name="'+ pid +'_value"]').attr('data-action', action);
            switch (action) {
                case '1':
                    var temp = $("." + pid + "_content").attr('data-value');
                     $("." + pid + "_content").text(temp + " > " + (+temp + +$('input[name="'+ pid +'_value"]').val()));
                    break;
                case '2':
                    var temp = $("." + pid + "_content").attr('data-value');
                     $("." + pid + "_content").text(temp + " > " + $('input[name="'+ pid +'_value"]').val());
                    break;
                default:

            }
        }

        $('input[name="txt_prod_supplier_temp"]').on('change', function() {
            if($(this).val() != "") {
                var name = $(this).attr('name'),
                    data = $('#' + name + "_list"),
                    sel = $(this).val(),
                    id = data.find('option[value="'+ sel +'"]').attr('data-value');
            } else {
                $('.' + name + "_error").html("This Field is important");
            }
        });

        $('input[name="file_prod_images"]').on('change', function() {
            var form_data = new FormData();
            for(var i = 0; i < $(this).prop('files').length; i++) {
                form_data.append('file[]', $(this).prop('files')[i]);
            }

            form_data.append('pid', $(this).attr('data-value'));

            if($(this).prop('files').length >= 1) {
                $.ajax({
                    xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                            if (percentComplete === 1) {
                                $('.progress').addClass('hide');
                            }
                        }
                    }, false);
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                        }
                    }, false);
                    return xhr;
                    },
                    url: '<?=base_url()?>ez/product/image/uploadvariant/', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        if(response == "OK") {
                            location.reload();
                        } else {
                            alert("Something went wrong.");
                        }
                    }
                });
            }
        });
    });
</script>
<script>
$(function() {



    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
