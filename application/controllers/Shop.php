<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;

	}

	public function index() {
		$data['page_title'] = "Hair Geek";

		if(isset($_GET['filter'])) {
			$order_by = $_GET['filter'];
			$dir =  $_GET['dir'];
		}

		$data['products'] = $this->Product_Model->get_all_variants_shop($order_by, $dir);
		$data['feedbacks'] = $this->Customer_Model->get_limited_feedback(2);
		$data['feedback'] = $this->Customer_Model->get_limited_feedback(1);
        $this->load->view('main/home_view', $data);
    }

	public function search($keyword) {
		$data['page_title'] = "Hair Geek";
		$keyword = urldecode($keyword);
		
		$data['products'] = $this->Product_Model->search_product($keyword);
		$data['keyword'] = $keyword;

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());


		if(count($this->Search_Model->get_detail_by_keyword($keyword)) >= 1) {

			$search = $this->Search_Model->get_detail_by_keyword($keyword);
			$search_data = array(
				"search_statistic_count" => intval($search->search_statistic_count) + 1,
				"date_modified" => $time
			);

			$this->Search_Model->update_statistics($search_data, $keyword);
		} else {

			$search_data = array(
				"search_statistic_keyword" => $keyword,
				"search_statistic_count" => 1,
				"date_created" => $time,
				"date_modified" => $time
			);

			$this->Search_Model->insert_statistics($search_data);
		}

        $this->load->view('main/search_view', $data);
	}

	public function subscribe() {
		if(isset($_POST['email'])
		&& isset($_POST['fullname'])) {

			$e = $_POST['email'];
			$f = $_POST['fullname'];

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$news_data = array(
				"newsletter_email" => $e,
				"newsletter_fullname" => ucwords($f),
				"date_created" => $time
			);

			echo $this->Customer_Model->insert_newsletter($news_data);
		}

		#echo "Yea!";
	}

	public function contact() {
		$data['page_title'] = "Hair Geek";

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());

		$this->form_validation->set_rules('txt_email', ' Email' , 'required|valid_email');
		$this->form_validation->set_rules('txt_fullname', ' Full Name' , 'required');
		$this->form_validation->set_rules('txt_category', ' Category' , 'required');
		$this->form_validation->set_rules('txt_message', 'Message' , 'required');

		$date = "%Y-%m-%d %H:%i:%s";
		$time = mdate($date, time());


		if ($this->form_validation->run() && !isset($_GET['success'])) {
			$inquiries_data = array(
				"inquiry_email" => $this->input->post('txt_email'),
				"inquiry_fullname" => $this->input->post('txt_fullname'),
				"inquiry_category" => $this->input->post('txt_category'),
				"inquiry_message" => $this->input->post('txt_message'),
				"date_created" => $time,
				"inquiry_status" => 1
			);

			$this->Customer_Model->insert_inquiry($inquiries_data);

			redirect(base_url() . 'shop/contact/?success=' . md5('sent'));
		}

		$this->load->view('main/contact', $data);
	}

	public function playground() {
		$this->load->view('play_view');
	}

}
