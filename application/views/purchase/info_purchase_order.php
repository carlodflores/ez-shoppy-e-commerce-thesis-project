<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }

    a {
        color: #7997c1;
    }

    a:hover {
        color: #7999e2;
    }

    .ship-fields input {
        margin-top: 10px;
    }

    .show-holder {
        background: #fff; border-radius: 3px; padding: 10px; width: 200px; position: absolute; display: none; z-index: 999999;
        border: 2px solid rgba(121, 151, 193, 0.6);
    }
</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-download"> </i>Purchase Order #<?=$id?></h3><a href="<?=base_url(). 'ez/product/purchase/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid field">
                <div class="row">
                    <div class="push_one fifteen columns">
                        <input type="hidden" name="variants_added" value="" />
                        <div class="row">
                            <div class="ten columns">
                                <h3 class="content-title">Purchase Order Details</h3>
                                <div class="row">
                                    <div>
                                        <table id="order-details-holder">
                                            <?php $total = 0; ?>
                                            <?php foreach ($this->Order_Model->get_purchase_content($id) as $key => $row) {
                                                $var_data = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                                                $img = $var_data->product_variant_img;
                                                if(strpos($var_data->product_variant_img, ',')) {
                                                    $img = explode(',', $var_data->product_variant_img);
                                                }

                                                $total = $var_data->product_variant_capital*$row->purchase_order_content_qty;
                                            ?>
                                                <tr class="stocks-row loaded-order">
                                                    <td><img src="<?=base_url() ?>img/products/<?=is_array($img)? $img[0] : $img?>" width="50"></td>
                                                    <td>
                                                        <span class="product_title"><?=$this->Product_Model->get_product_by_id($var_data->product_id)->product_title?></span>
                                                        <Br/><span class="txt-label"><?=str_replace(' ', ' / ', $var_data->product_variant_name)?></span>
                                                    </td>
                                                    <td>
                                                        P <?=$var_data->product_variant_capital?> x <?=$row->purchase_order_content_qty?>
                                                    </td>
                                                    <td class="v_price_display">
                                                        P <?=$var_data->product_variant_capital*$row->purchase_order_content_qty?>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size: 18px;">Total : P <?=number_format($total, 2)?></td>
                                            </tr>
                                        </table>
                                        * Price displayed above are the capital per variant.
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="row">
                                        <br/>
                                        <div class="eight columns alpha">
                                            <input type="text" class="input" name="txt_order_note" value="<?=$info->purchase_order_note?>" placeholder="Add a note" readonly="readonly"/>
                                        </div>
                                </div>
                    </div>
                </div>

                <div class="five columns">
                    <h3 class="content-title">Details</h3>
                    <div class="clearfix"></div>
                    <Br/>
                    <p>
                        Supplier : <?=$this->Product_Model->check_supplier_exist($info->product_supplier_id)->product_supplier_name?>
                    </p>
                </div>
            </div>

            <BR/><br/><br/>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
