<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    a {
        color: #7997c1 ;
    }

    a:hover {
        color: #577195;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class='icon-user'> </i><?=$customer->customer_fname . ' ' . $customer->customer_lname?></h3><a href="<?=base_url(). 'ez/customer/'?>"><span class="btn-more" style="color:#fff;">Back</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <div class="push_one eight columns">
                        <div class="row">
                            <div class="push_three five columns">
                                <?php if ($customer->customer_img != ""): ?>
                                    <img src="<?=base_url()?>img/customer/<?=$customer->customer_img?>" id="customer_img" style="width: 80px; border-radius: 50%;" alt="" />
                                <?php else: ?>
                                    <img src="<?=base_url()?>img/profile-placeholder.jpg" id="customer_img" style="width: 80px; border-radius: 50%;" alt="" />
                                <?php endif; ?>

                            </div>

                            <div class="six columns">
                                <h3 class="content-title" id="customer_name" style="color: #000; float:none; margin-top: 10px; "><?=$customer->customer_fname . ' ' . $customer->customer_lname?></h3>
                                <h3 class="content-title" id="customer_email" style="color: #000; margin-top: -10px; font-size: 12px;"><?=$customer->customer_email?></h3>
                            </div>
                            <BR/><BR/><BR/><BR/>
                            <input type="text" class="input"  name="name" value="">
                            <br/><br/>
                        </div>

                        <div class="row" style="margin-top: -20px;">
                            <ul id="customer-view-holder">
                                <li>
                                    <div class="" style="padding: 20px; border-bottom: 2px solid #7997c1;">
                                        <center>
                                            <span class="txt-label" style="font-size: 12px">Recent Order</span><br/>
                                            <span style="font: 700 18px 'Open Sans', sans-serif;">
                                                <?php if ($this->Order_Model->get_customer_last_order($customer->customer_id) !== null): ?>
                                                    <a href="<?=base_url()?>ez/order/edit/<?=$this->Order_Model->get_customer_last_order($customer->customer_id)->order_id?>">#<?=$this->Order_Model->get_customer_last_order($customer->customer_id)->order_id?></a>
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>

                                            </span>
                                        </center>

                                    </div>
                                </li>
                                <li>
                                    <div class="" style="padding: 20px;border-bottom: 2px solid #7997c1;">
                                        <center>
                                            <span class="txt-label" style="font-size: 12px">No. of Orders</span><br/>
                                            <span style="font: 700 18px 'Open Sans', sans-serif;">
                                                <?=count($this->Order_Model->get_all_customer_order($customer->customer_id))?>
                                            </span>
                                        </center>
                                    </div>
                                </li>
                                <li>
                                    <div class="" style="padding: 20px;border-bottom: 2px solid #7997c1;">
                                        <center>
                                            <span class="txt-label" style="font-size: 12px">Lifetime Spent</span><br/>
                                            <span style="font: 700 18px 'Open Sans', sans-serif;">
                                                <?php if ($this->Order_Model->get_total_spent($customer->customer_id) !== null): ?>
                                                    P <?=number_format($this->Order_Model->get_total_spent($customer->customer_id)->total, 2)?>
                                                <?php else: ?>
                                                    P 0.00
                                                <?php endif; ?>
                                            </span>
                                        </center>
                                    </div>
                                </li>
                                <li>
                                    <div class="" style="padding: 20px;border-bottom: 2px solid #7997c1;">
                                        <center>
                                            <span class="txt-label" style="font-size: 12px">Average Orders</span><br/>
                                            <span style="font: 700 18px 'Open Sans', sans-serif;">
                                                <?php if ($this->Order_Model->get_avg_spent($customer->customer_id) !== null): ?>
                                                    P <?=number_format($this->Order_Model->get_avg_spent($customer->customer_id)->total,2)?>
                                                <?php else: ?>
                                                    P 0.00
                                                <?php endif; ?>

                                            </span>
                                        </center>
                                    </div>
                                </li>
                            </ul>
                            <Br/><Br/><Br/><Br/><Br/>
                            <hr style="height: 1px; background: rgba(0, 0, 0, 0.2)">
                        </div>

                        <div class="row">
                            <span><h3 class="content-title">Recent Orders</h3><span class="btn-more">More</span></span>
                            <hr>
                            <?php if ($this->Order_Model->get_all_customer_order($customer->customer_id) !== null): ?>
                                <?php foreach ($this->Order_Model->get_all_customer_order($customer->customer_id) as $key => $value): ?>
                                    <?php if($key >= 5 ) break; ?>
                                    <div class="sixteen columns">
                                        <span style="font: 700 18px 'Open Sans', sans-serif;">
                                            <a href="<?=base_url()?>ez/order/edit/<?=$value->order_id?>/">#<?=$value->order_id?></a>
                                            <span style="float:right; font-size: 12px; padding-top: 10px;"><?=$value->date_created?></span>
                                        </span>
                                        <table>
                                            <?php foreach ($this->Order_Model->get_order_content_by_id($value->order_id) as $key => $row) {
                                                $var_data = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                                                $img = $var_data->product_variant_img;
                                                if(strpos($var_data->product_variant_img, ',')) {
                                                    $img = explode(',', $var_data->product_variant_img);
                                                }
                                            ?>
                                                <tr class="stocks-row <?=$row->order_content_id?>_row" data-value="<?=$row->product_variant_id?>">
                                                    <td><img src="<?=base_url() ?>img/products/<?=is_array($img)? $img[0] : $img?>" width="50"></td>
                                                    <td>
                                                        <span class="product_title"><?=$this->Product_Model->get_product_by_id($var_data->product_id)->product_title?></span>
                                                        <Br/><span class="txt-label"><?=str_replace(' ', ' / ', $var_data->product_variant_name)?></span>
                                                    </td>
                                                    <td>
                                                         <?php
                                                         $oPrice=$nPrice=$var_data->product_variant_price;
                                                          if ($this->Order_Model->get_order_discount($value->order_id, $var_data->product_variant_id, 'item' ) !== null): ?>
                                                             <?php
                                                                $disc = $this->Order_Model->get_order_discount($value->order_id, $var_data->product_variant_id, 'item' );
                                                                switch ($disc->order_discount_type) {
                                                                    case '1':
                                                                        $nPrice -= $disc->order_discount_value;
                                                                        break;

                                                                    default:
                                                                        $nPrice -= $nPrice*($disc->order_discount_value/100);
                                                                        break;
                                                                }
                                                              ?>
                                                             <span class="product_title">P <?=number_format($nPrice, 2)?>  x <?=$row->order_content_qty?></span>
                                                             <Br/><span class="txt-label">P <?=$oPrice?></span>
                                                             <Br/><span class="txt-label"><?=$disc->order_discount_reason?></span>
                                                         <?php else: ?>
                                                            <span class="product_title"> P <?=$var_data->product_variant_price?>  x <?=$row->order_content_qty?></span>
                                                            <span></span>
                                                         <?php endif; ?>
                                                    </td>
                                                    <td class="v_price_display">
                                                        P <?=number_format(($nPrice*$row->order_content_qty), 2)?>
                                                    </td>
                                                </tr>
                                            <?php }
                                                $nTotal = $value->order_total;
                                                $reason = "";
                                                if($this->Order_Model->get_order_discount($value->order_id, '0', 'all' ) !== null) {
                                                    $reason = $this->Order_Model->get_order_discount($value->order_id, '0', 'all' )->order_discount_reason;
                                                    switch ($this->Order_Model->get_order_discount($value->order_id, '0', 'all' )->order_discount_type) {
                                                        case '1':
                                                            $nTotal -= $this->Order_Model->get_order_discount($value->order_id, '0', 'all' )->order_discount_value;
                                                            break;

                                                        case '2':
                                                            $nTotal -= $nTotal*($this->Order_Model->get_order_discount($value->order_id, '0', 'all' )->order_discount_value/100);
                                                            break;
                                                    }
                                                }
                                            ?>

                                            <tr class="stocks-row">
                                                <td></td>
                                                <td></td>
                                                <td>Total</td>
                                                <td>
                                                    <span style="font: 700 18px 'Open Sans', sans-serif;">P <?=number_format($nTotal, 2)?></span>
                                                    <?php if ($reason != ""): ?>
                                                        <Br/><span class="txt-label">P <?=$value->order_total?></span>
                                                    <?php endif; ?>
                                                     <Br/><span class="txt-label"><?=$reason?></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <br/><Br/><br/>
                    </div>

                    <div class="push_one six columns">
                        <br/><Br/><Br/><br/>
                        <h3 class="content-title" style="float: none;">Shipping Details</h3>
                        <form action="<?=base_url()?>ez/customer/profile/<?=$customer->customer_id?>/" method="post">
                            <div class="row">
                                <div class="row">
                                    <div class="twelve columns"><span class="txt-label" style="font-size: 12px;">Company</span>
                                        <input type="text" class="input" name="customer_company" placeholder="Company" value="<?=$customer->customer_company?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="twelve columns"><span class="txt-label" style="font-size: 12px;">Phone</span>
                                        <input type="text" class="input" name="customer_phone" placeholder="Phone" value="<?=$customer->customer_phone?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="twelve columns">
                                        <span class="txt-label" style="font-size: 12px;">Address</span>
                                        <input type="text" class="input" name="customer_address" placeholder="Address" value="<?=$customer->customer_address?>" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="twelve columns"><span class="txt-label" style="font-size: 12px;">City</span>
                                        <input type="text" class="input" name="customer_city" placeholder="City" value="<?=$customer->customer_city?>" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="twelve columns"><span class="txt-label" style="font-size: 12px;">State</span>
                                        <input type="text" class="input" name="custer_state" placeholder="State" value="<?=$customer->customer_state?>" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="twelve columns"><span class="txt-label" style="font-size: 12px;">Zip/Postal Code</span>
                                        <input type="text" class="input" name="customer_zipcode" placeholder="Postal/Zip Code" value="<?=$customer->customer_zipcode?>" />
                                    </div>
                                </div>
                                <Br/><br/>
                                <div class="centered four columns">
                                    <center><input class="medium primary btn" type="submit" name="btn_add_order" value="Update" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
