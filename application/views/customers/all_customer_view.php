<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    a {
        color: #7997c1 ;
    }

    a:hover {
        color: #577195;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class='icon-users'> </i>All Customers</h3><a href="<?=base_url(). 'ez/customer/create/'?>"><span class="btn-more" style="color:#fff;">Add Customer</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <form method="post" class="append">
                            <span class="txt-label">Customer Name</span><br/>
                            <input type="text" class="input" name="search_temp" placeholder="Search Customer" list="search-datalist">
                            <datalist id="search-datalist">
                                <?php foreach($products as $row): ?>
                                    <option data-value="<?=$row->product_id?>" value="<?=ucwords(strtolower($row->product_title))?>" />
                                <?php endforeach; ?>
                            </datalist>
                        </form>
                    </div>
                    <div class="centered push_one twelve columns">
                        <table class="paginate" max="10">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th><center><span class="product-title">Name</span></center></th>
                                    <th><center><span class="product-title">Location</span></center></th>
                                    <th><center><span class="product-title">Orders</span></center></th>
                                    <th><center><span class="product-title">Last Order</span></center></th>
                                    <th><center><span class="product-title">Total Spent</span></center></th>
                                </tr>
                            </thead>

                            <?php foreach($customers as $row): ?>

                                    <tr class="stocks-row"  data-tag="<?=$row->customer_fname. ' ' . $row->customer_lname?>">
                                        <td>
                                            <span class="product-title"><a href="<?=base_url()?>ez/customer/profile/<?=$row->customer_id?>/"><?=$row->customer_fname. ' ' . $row->customer_lname?></a></span><Br/>
                                            <span class="txt-label"></span>
                                        </td>

                                        <td>
                                            <span class="product-title"><?=$row->customer_city?></span><Br/>
                                            <span class="txt-label">City</span>
                                        </td>

                                        <td>
                                            <span class="product-title"><?=(count($this->Order_Model->get_all_customer_order($row->customer_id)) > 0)? count($this->Order_Model->get_all_customer_order($row->customer_id)) : 0?></span><Br/>
                                            <span class="txt-label"># of Orders</span>
                                        </td>

                                        <td>
                                            <span class="product-title">
                                                <?php if ($this->Order_Model->get_customer_last_order($row->customer_id) !== null): ?>
                                                    <a href="<?=base_url()?>ez/order/edit/<?=$this->Order_Model->get_customer_last_order($row->customer_id)->order_id?>">#<?=$this->Order_Model->get_customer_last_order($row->customer_id)->order_id?></a>
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </span><Br/>
                                            <span class="txt-label">Order #</span>
                                        </td>

                                        <td>
                                            <span class="product-title">
                                                <?php if ($this->Order_Model->get_total_spent($row->customer_id) !== null): ?>
                                                    P <?=$this->Order_Model->get_total_spent($row->customer_id)->total?>
                                                <?php else: ?>
                                                    P 0.00
                                                <?php endif; ?>
                                            </span><Br/>
                                            <span class="txt-label">Total Spent</span>
                                        </td>
                                    </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>
$(function() {



    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
