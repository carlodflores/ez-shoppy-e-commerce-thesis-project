<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title><?=$page_title?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<?=base_url()?>css/style.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/style.min.css">
	<link rel="stylesheet" href="<?=base_url()?>css/animate.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.css">
	<script src="<?=base_url()?>assets/js/libs/modernizr-2.6.2.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery-ui-1.9.2.min.js"></script>
	<script src="http://listjs.com/no-cdn/list.js"></script>
	<script src="http://listjs.com/no-cdn/list.pagination.js"></script>

</head>

<body>
	<div class="loading">
		<center>
			<div class="rotator">
				<i class="fa fa-spinner"></i><br/>
				Please wait...
			</div>
		</center>
	</div>

	<div class="modal field" id="search_modal">
		<div class="sixteen colgrid content">
			<div class="row">
				<br/><Br/>
				<center>
					<input type="text" name="txt_search" class="input normal" />
					<button type="button" name="btn_search" style="margin-left: -23px !important;"><i class="fa fa-search"> </i> Search</button>
					<button type="button" name="button" class="switch" gumby-trigger="|#search_modal"><i class="fa fa-close"></i></button>
				</center>
			</div>
		</div>
	</div>
