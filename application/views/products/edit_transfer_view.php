<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Manage Transfer</h3><a href="<?=base_url(). 'ez/product/transfer/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <form class="" action="<?=base_url(). 'ez/product/transfer/edit/' . $pid?>" method="post">
            <div class="sixteen colgrid">
                <div class="row">
                    <div class="centered four columns">
                        <table>
                            <tr class="stocks-row">
                                <td>
                                    <?php if($this->Product_Model->get_product_by_id($pid)->product_img != ""): ?>
                                        <?php
                                            $img = $this->Product_Model->get_product_by_id($pid)->product_img;
                                            if(strpos(',', $this->Product_Model->get_product_by_id($pid)->product_img)) {
                                                $img = explode(',',$this->Product_Model->get_product_by_id($pid)->product_img);
                                                $img = $img[0];
                                            }
                                         ?>
                                        <img src="<?=base_url() . 'img/products/' . $img ?>" alt="" width="50"/>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <span class="product-title"><?=$this->Product_Model->get_product_by_id($pid)->product_title?></span>
                                    <span class="txt-label"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row field">
                    <div class="push_one nine columns">
                        <Br/>

                        <table>
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th></th>
                                    <?php foreach($this->Product_Model->get_meta_by_id($pid) as $row): ?>
                                        <th><center><span class="product-title"><?=$row->product_meta_keyword?></span></center></th>
                                    <?php endforeach; ?>
                                    <th><center><span class="product-title">SKU</span></center></th>
                                    <th><center><span class="product-title">Quantity Ordered</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach($this->Product_Model->get_product_variant($pid) as $row): ?>
                            <tr class="stocks-row <?=$row->product_variant_id?>_holder">
                                <td>
                                    <div class="upload-holder">
                                        <input type='file' data-value="<?=$row->product_variant_id?>" name="file_prod_images" id="inputFile" multiple/>
                                        <?php if($row->product_variant_img != ""): ?>
                                            <?php
                                                $img = $row->product_variant_img;
                                                if(strpos(',', $row->product_variant_img)) {
                                                    $img = explode(',',$row->product_variant_img);
                                                    $img = $img[0];
                                                }
                                             ?>
                                            <img src="<?=base_url() . 'img/products/' . $img?>" alt="" width="50"/>
                                        <?php endif; ?>

                                    </div>
                                </td>
                                <td>
                                    <span class="product-title"><?=$row->product_variant_name?></span><Br/>
                                    <span class="txt-label"></span>
                                </td>
                                <td>
                                    <span class="product-title"><?=$row->product_variant_sku?></span><Br/>
                                    <span class="txt-label">Sku</span>
                                </td>

                                <td class="field" style="width: 200px;">
                                    <span><input type="number" min="0" class="input narrow" name="<?=$row->product_variant_id?>_name" value=""></span>
                                </td>

                            </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>

                    <div class="push_one four columns"><br/><br/>
                        <span class="txt-label">Supplier</span>
                        <input class="input" type="text" name="txt_prod_supplier_temp" placeholder="Supplier" list="txt_prod_supplier_temp_list" value="">
                        <datalist id="txt_prod_supplier_temp_list">
                            <?php foreach($suppliers as $row): ?>
                                <option data-value="<?=$row->product_supplier_id?>" value="<?=$row->product_supplier_name?>" />
                            <?php endforeach; ?>
                        </datalist>
                        <input type="text" name="txtsupplier_brand" value="<?=set_value('txt_prod_brand')?>"/>
                        <br/><br/>
                    </div>
                </div>
            </div>

            <div class="row field">
                <div class="centered four columns">
                    <center><input class="medium primary btn" type="submit" name="btn_prod_add" value="Save Transfer" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                </div>
            </div>
            <br/><Br/>
            </form>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script type="text/javascript">
    $(function() {

        $('input[name="txt_prod_supplier_temp"]').on('change', function() {
            if($(this).val() != "") {
                var name = $(this).attr('name'),
                    data = $('#' + name + "_list"),
                    sel = $(this).val(),
                    id = data.find('option[value="'+ sel +'"]').attr('data-value');
            } else {
                $('.' + name + "_error").html("This Field is important");
            }
        });

        $('input[name="file_prod_images"]').on('change', function() {
            var form_data = new FormData();
            for(var i = 0; i < $(this).prop('files').length; i++) {
                form_data.append('file[]', $(this).prop('files')[i]);
            }

            form_data.append('pid', $(this).attr('data-value'));

            if($(this).prop('files').length >= 1) {
                $.ajax({
                    xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                            if (percentComplete === 1) {
                                $('.progress').addClass('hide');
                            }
                        }
                    }, false);
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                        }
                    }, false);
                    return xhr;
                    },
                    url: '<?=base_url()?>ez/product/image/uploadvariant/', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        if(response == "OK") {
                            location.reload();
                        } else {
                            alert("Something went wrong.");
                        }
                    }
                });
            }
        });

        $(document).on('click', '.variant_remover', function(){
            var cls = $(this).attr('data-value');
            $('.' + cls).hide();
        });
    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
