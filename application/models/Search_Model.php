<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_Model extends CI_Model {

    function __construct(){
	parent::__construct();
	}

    function get_detail_by_keyword($keyword) {
        $this->db->where('search_statistic_keyword', $keyword);
        return $this->db->get('cs_search_statistics')->row();
    }

    function insert_statistics($data) {
        $this->db->insert('cs_search_statistics', $data);
        return $this->db->insert_id();
    }

    function update_statistics($data, $keyword) {
        $this->db->where('search_statistic_keyword', $keyword);
        $this->db->update('cs_search_statistics', $data);
        return $this->db->affected_rows();
    }
}
