<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content field">
        <center>
            <h1>My Address</h1>
            <p>
                This can be use to avoid filling the checkout form.
            </p>
        </center>
        <div class="row">
            <form class="<?=base_url()?>customer/edit/" action="index.html" method="post">
            <div class="ten centered columns">
                <?php if ($alert != ""): ?>
                    <?=$alert?>
                <?php endif; ?>

                <h3>Personal Details</h3>
                <div class="sixteen  colgrid">
                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">First Name </span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_fname" placeholder="First Name" value="<?=$c->customer_fname?>"><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">Last Name</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_lname" placeholder="Last Name" value="<?=$c->customer_lname?>"><br/>
                        </div>
                    </div>

                    <div class="row">
                        <span class="text-label input-label">Phone #</span> <span class="text-indicator">*</span>
                        <input type="text" class="input" name="txt_phone" placeholder="Phone #" value="<?=$c->customer_phone?>"><br/>
                    </div>
                </div>
                <Br/>
                <h4>Address</h4>

                <div class="sixteen colgrid">
                    <span class="text-label input-label">Company</span>
                    <input type="text" class="input" name="txt_company" placeholder="Company" value="<?=$c->customer_phone?>"><br/>

                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">Street Address</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_address" placeholder="Street Address" value="<?=$c->customer_address?>"><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">City</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_city" placeholder="City" value="<?=$c->customer_city?>"><br/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">Province / State (eg. Manila, Davao)<span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_state" placeholder="Province / State" value="<?=$c->customer_state?>"><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">Postal / Zip Code</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_zipcode" placeholder="Postal / Zip Code" value="<?=$c->customer_zipcode?>"><br/>
                        </div>
                    </div>
                </div>

                <span class="text-label input-label">Country</span> <span class="text-indicator">*</span>
                <input type="text" class="input" name="txt_country" placeholder="Country" value="Philippines" readonly="readonly"><br/>
                <br/>
                <center>
                    <div class="address-holder">
                        <button type="submit" style="padding: 10px; font-size: 20px;"><i class="fa fa-save"> </i> Save</button>
                        -or-
                        <a href="<?=base_url()?>customer/"><button type="button" style="padding: 10px; font-size: 20px;"><i class="fa fa-close"> </i> Cancel</button></a>
                    </div>
                </center>
            </div>
            </form>
        </div>
	</div>
<?php
    $this->load->view('main/footer_view');
?>
