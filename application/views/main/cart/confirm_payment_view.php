<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');

    $order = $this->Order_Model->get_order_by_group($group);
?>

    <style media="screen">
        .progress {
            display: block;
            text-align: center;
            width: 0;
            height: 3px;
            background: red;
            transition: width .3s;
        }
        .progress.hide {
            opacity: 0;
            transition: opacity 1.3s;
        }
    </style>

	<div class="sixteen colgrid main-content field">
        <center>
            <h1>Confirm Payment</h1>
            <p>
                We'll base the information to the information you've entered befored placing the order.
            </p>
            <hr style="height: 1px; border: none; background: #000; width: 100px;">
        </center>

        <div class="row">
            <div class="centered nine columns cart-holder">
                <form action="<?=base_url()?>cart/confirm/<?=$order_id?>/<?=$group?>/" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="five columns">
                        <span class="text-label">Order Number</span>
                        <h4>#<?=$order->order_id?></h4>
                    </div>
                    <div class="six columns">
                        <span class="text-label">Date</span>
                        <h4><?=$order->date_created?></h4>
                    </div>
                    <div class="five columns">
                        <span class="text-label">Total</span>
                        <h4>P <?=$order->order_total?></h4>
                    </div>
                </div>

                <div class="row">
                    <div class="five columns">
                        <span class="text-label">Payment Method</span>
                        <h4><?=$this->Order_Model->get_all_meta_value($order->order_id, "payment_method")->value?></h4>
                    </div>
                    <div class="six columns">
                        <span class="text-label">Order Status</span>
                        <?php if($order->order_payment_status == 0): ?>
                            <h4 style="color: red;">Pending</h4>
                        <?php elseif($order->order_payment_status == 1): ?>
                            <h4 style="color: #FFCC00;">Proof Sent</h4>
                        <?php elseif($order->order_payment_status == 3): ?>
                            <h4 style="color: red;">Re-Send Proof</h4>
                        <?php else: ?>
                            <h4 style="color: green;">Paid</h4>
                        <?php endif; ?>
                    </div>
                </div>
                <br/><br/>
                <?php if ($order->order_payment_status != 2): ?>
                    <div class="progress"></div>
                    <div class="row">
                        <?php if (validation_errors() != ""): ?>
                            <div class="row">
                                <div class="danger alert" style="padding: 10px;"><center><?=validation_errors('<span style="display: block;">','</span>')?></center></div>
                            </div>
                        <?php endif; ?>

                        <h3>Attachments (Transaction Slip)</h3><br/>
                        <input type='file' name="file_prod_images" id="inputFile" multiple/>
                        <br/><br/>
                        <h3>Order Note</h3>
                            <span class="text-label input-label">Order Note</span><Br/>
                            <textarea class="input textarea" name="txt_note" placeholder="Place message to Hairgeek Here."></textarea>
                            <input type="text" name="files_txt" value="">
                    </div>
                    <center></br>
                        <button type="submit" name="button" disabled="disabled" style="padding: 10px; font-size: 20px;"><i class="fa fa-check"> </i> Submit</button>
                    </center>
                    </form>
                <?php endif; ?>
            </div>
        </div>

	</div>
    <script type="text/javascript">
        $(function() {
            $('input[name="file_prod_images"]').on('change', function() {
                var form_data = new FormData();
                for(var i = 0; i < $(this).prop('files').length; i++) {
                    form_data.append('file[]', $(this).prop('files')[i]);
                }

                form_data.append('order_note', $('textarea[name="txt_note"]').val());

                if($(this).prop('files').length >= 1) {
                    $.ajax({
                        xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                console.log(percentComplete);
                                $('.progress').css({
                                    width: percentComplete * 100 + '%'
                                });
                                if (percentComplete === 1) {
                                    $('.progress').addClass('hide');
                                }
                            }
                        }, false);
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                console.log(percentComplete);
                                $('.progress').css({
                                    width: percentComplete * 100 + '%'
                                });
                            }
                        }, false);
                        return xhr;
                        },
                        url: '<?=base_url()?>ez/api/image/upload_proof/<?=$order_id?>/', // point to server-side PHP script
                        dataType: 'text',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response){
                            if(response != "") {
                                $("input[name='files_txt']").val(response);
                                $('button[type="submit"]').removeAttr('disabled');
                            }
                        }
                    });
                }
            });
        });
    </script>
<?php
    $this->load->view('main/footer_view');
?>
