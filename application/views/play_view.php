<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="<?=base_url()?>js/jquery.min.js"></script>
</head>
<body>

    <div id="ezshop-container-33"></div>
    <script type="text/javascript">$(function(){function r(r,e){e=JSON.stringify(e).replace(/"/g,"").replace(/,/g,";"),$("<style>").prop("type","text/css").html(r+e).appendTo("head")}var e=$("#ezshop-container-33"),t="auto",o=200,p="#1495ef"
e.css({height:t+"px",width:o+"px",backgroundColor:p,font:"700 14px Calibri",color:"#fff",padding:"0px 0px 10px 0px","border-radius":"5px"}),r(".product-img-holder",{width:"100%"}),r("button",{border:"none",background:"#1d3e55",padding:"10px",color:"#fff","border-radius":"3px","font-weight":"800",cursor:"pointer"}),$.post("http://ez.hairgeek.ph/ez/api/pos/get/33/",{},function(r){var r=$.parseJSON(r)
e.append("<img class='product-img-holder' src='http://ez.hairgeek.ph/img/products/"+r.product_variant_img+"'/>"),e.append("<div class='product-details'><center>"+r.product_title+": "+r.product_variant_name+"<br/><BR/> P"+r.product_variant_price+" Only</center></div>"),e.append("<br/><center><a href='http://ez.hairgeek.ph/cart/add/33/?redirect_to=http://ez.hairgeek.ph/cart/embed/'><button>BUY NOW!</button></a></center>")})})</script>


    <script type="text/javascript">
    $(function() {
        // Container
        var container = $("#ezshop-container-<?=$pvid?>");

        var height = "auto",
            width = 200,
            background = "#1495ef";

        container.css({
            "height": height + "px",
            "width" : width + "px",
            "backgroundColor" : background,
            "font" : "700 14px Calibri",
            "color" : "#fff",
            "padding" : "0px 0px 10px 0px",
            "border-radius" : "5px"
        });

        // Product
        addCssRule(".product-img-holder", {
            "width" : '100%',
        });

        addCssRule("button", {
            "border" : "none",
            "background" : "#1d3e55",
            "padding" : "10px",
            "color" : "#fff",
            "border-radius" : "3px",
            "font-weight" : "800",
            "cursor" : "pointer"
        });

        addCssRule(".the-real-holder", {
            "max-height": "100px",
            "overflow-y": "hidden"
        });
        $.post("<?=base_url()?>ez/api/pos/get/<?=$pvid?>/", {}, function(data) {
            var data = $.parseJSON(data);
            container.append("<div class='the-real-holder'><img class='product-img-holder' src='<?=base_url()?>img/products/"+ data.product_variant_img +"'/></div>");
            container.append("<div class='product-details'><center>"+ data.product_title +": "+ data.product_variant_name +"<br/><BR/> P" + data.product_variant_price +" Only</center></div>");
            container.append("<br/><center><a href='<?=base_url()?>cart/add/<?=$pvid?>/?redirect_to=<?=base_url()?>cart/embed/'><button>BUY NOW!</button></a></center>");
        });

        function addCssRule(rule, css) {
            css = JSON.stringify(css).replace(/"/g, "").replace(/,/g, ";");
            $("<style>").prop("type", "text/css").html(rule + css).appendTo("head");
        }
    });
    </script>
</body>
</html>
