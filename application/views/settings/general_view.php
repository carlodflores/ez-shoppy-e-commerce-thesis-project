<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    a {
        color: #7997c1 ;
    }

    a:hover {
        color: #577195;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class='icon-cog'> </i> General</h3><a href="<?=base_url(). 'ez/customer/'?>"><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <center><h2>General Settings</h2>
                    <p> </p></center>
                    <Br/>
                    <form action="<?=base_url()?>ez/customer/create/" method="post">
                        <div class=" eight centered columns">
                            <h3 class="content-title">Store Details</h3>
                            <hr class="trans-divider">
                            <div class="row">
                                <div class="columns eight">
                                    <span class="txt-label" style="font-size: 12px;">Store Name <span style="color:red;">*</span></span>
                                    <input type="text" class="input" name="store_name" placeholder="First Name" value="" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="columns fifteen">
                                    <span class="txt-label" style="font-size: 12px;">Store Description <span style="color:red;">*</span></span>
                                    
                                </div>
                            </div>
                            <Br/>
                            <h3 class="content-title">Shipping Details</h3>
                            <hr class="trans-divider">
                            <div class="row">
                                <span class="txt-label" style="font-size: 12px;">Company</span>
                                <input type="text" class="input" name="customer_company" placeholder="Company" value="" />
                            </div>

                            <div class="row">
                                <div class="columns eight">
                                    <span class="txt-label" style="font-size: 12px;">Address <span style="color:red;">*</span></span>
                                    <input type="text" class="input" name="customer_address" placeholder="Address" value="" />
                                </div>
                                <div class="columns eight">
                                    <span class="txt-label" style="font-size: 12px;">City <span style="color:red;">*</span></span>
                                        <input type="text" class="input" name="customer_city" placeholder="City" value="" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="columns eight">
                                    <span class="txt-label" style="font-size: 12px;">Province <span style="color:red;">*</span></span>
                                        <input type="text" class="input" name="customer_province" placeholder="Province" value="" />
                                </div>
                                <div class="columns eight">
                                    <span class="txt-label" style="font-size: 12px;">Zip/Postal Code <span style="color:red;">*</span></span>
                                        <input type="text" class="input" name="customer_zipcode" placeholder="Postal/Zip Code" value="" />
                                </div>
                            </div>

                            <div class="row">
                                <Br/><br/>
                                <div class="centered four columns">
                                    <center><input class="medium primary btn" type="submit" name="btn_add_order" value="Create" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                                </div>
                                <Br/><br/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
