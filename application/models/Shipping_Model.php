<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

    function get_shipping_size($local, $size) {
        $this->db->where('shipping_max >=', $size);
        $this->db->where('shipping_min <', $size);
        $this->db->where('shipping_type', $local);
        return $this->db->get('cs_shipping')->row();
    }

}
