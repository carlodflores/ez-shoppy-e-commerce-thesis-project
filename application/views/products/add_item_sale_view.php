<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Put Item On Sale</h3><a href="<?=base_url(). 'ez/product/sale/list/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">
                    <div class="centered push_one eight columns">
                        <center><h2>Put Item On Sale</h2></center>
                    </div><Br/>
                </div>
                <div class="row field">
                    <form action="<?=base_url()?>ez/product/sale/create/<?=$pvid?>/" method="post">
                        <div class="push_four fourteen columns">
                            <div class="row">
                                <div class="five columns">
                                    <span class="txt-label">Item Old Price</span>
                                    <input type="text" class="input" value="<?=$this->Product_Model->get_variant_by_id($pvid)->product_variant_price?>" readonly placeholder="Item New Price" />
                                </div>

                                <div class="five columns">
                                    <span class="txt-label">Price Off</span><br/>
                                    <span class="prepend append " style="margin-right: 10px;">
                                        <div class="medium primary btn" data-tag="discount_type" data-action="1" style="font-size: 12px;"><a>P</a></div>
                                        <div class="medium default btn" data-tag="discount_type" data-action="2" style="font-size: 12px;"><a>%</a></div>
                                    </span>
                                    <span><input type="number" name="sale_price" min="0" class="input narrow value_holder" data-action="1" value=""></span>
                                    <input type="hidden" name="discount_type" value="1">
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="five columns">
                                    <span class="txt-label">Sale Starts</span>
                                    <div class="row">
                                        <div class="eight columns"><input type="date" class="input" name="sale_start_date"/></div>
                                        <div class="eight columns"><input type="time" class="input" name="sale_start_time" /></div>
                                    </div>
                                </div>

                                <div class="five columns">
                                    <span class="txt-label">Sale Ends</span>
                                    <div class="row">
                                        <div class="eight columns"><input type="date" class="input" name="sale_end_date" /></div>
                                        <div class="eight columns"><input type="time" class="input" name="sale_end_time" /></div>
                                    </div>

                                </div>
                            </div>
                            <Br/>
                        </div>

                        <div class="row"><br/><Br/>
                            <div class="centered four columns">
                                <center><input class="medium primary btn" type="submit" name="btn_prod_add" value="Add Discount" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br/><br/>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {

        $("input[name='sale_start_date']").on('change', function() {
            $("input[name='sale_end_date']").attr('min', $(this).val());
        });

        $("div[data-tag='discount_type']").click(function() {
            var action =  $(this).attr('data-action');

            if(action == '2') {
                $('input[name="discount_limit"]').hide("fast");
            } else {
                $('input[name="discount_limit"]').show("fast");
            }

            $("input[name='discount_type']").val(action);

            $("div[data-tag='discount_type']:eq("+(action-1)+")").removeClass('default').addClass('primary');
            $("div[data-tag='discount_type']:not(':eq("+(action-1)+")')").removeClass('primary').addClass('default');

        });


    });

</script>

<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
