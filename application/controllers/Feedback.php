<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {
		$data['page_title'] = "Hair Geek";
        $this->load->view('main/feedback/feedback_view', $data);
    }


}
