<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');

    $order = $this->Order_Model->get_order_by_group($group);
?>

	<div class="sixteen colgrid main-content field">
        <center>
            <h1>Fulfill Payment</h1>
            <hr style="height: 1px; border: none; background: #000; width: 100px;">
        </center>

        <div class="row">

            <?php if (isset($_GET['action']) && $_GET['action'] == md5('consuccess')): ?>
                <div class="row">
                    <div class="success alert" style="padding: 10px;"><center>You've successfuly sent a Proof of Payment. Thank you!</center></div>
                </div>
            <?php endif; ?>

            <div class="centered nine columns">
                <div class="row">
                    <div class="five columns">
                        <span class="text-label">Order Number</span>
                        <h4>#<?=$order->order_id?></h4>
                    </div>
                    <div class="six columns">
                        <span class="text-label">Date</span>
                        <h4><?=$order->date_created?></h4>
                    </div>
                    <div class="five columns">
                        <span class="text-label">Total</span>
                        <h4>P <?=$order->order_total?></h4>
                    </div>
                </div>

                <div class="row">
                    <div class="five columns">
                        <span class="text-label">Payment Method</span>
                        <h4><?=$this->Order_Model->get_all_meta_value($order->order_id, "payment_method")->value?></h4>
                    </div>
                    <div class="six columns">
                        <span class="text-label">Order Status</span>
                        <?php if($order->order_payment_status == 0): ?>
                            <h4 style="color: red;">Pending</h4>
                        <?php elseif($order->order_payment_status == 1): ?>
                            <h4 style="color: #FFCC00;">Proof Sent</h4>
                        <?php elseif($order->order_payment_status == 3): ?>
                            <h4 style="color: red;">Re-Send Proof</h4>
                        <?php else: ?>
                            <h4 style="color: green;">Paid</h4>
                        <?php endif; ?>
                    </div>

                    <?php if ($order->order_payment_status != 2): ?>
                        <div class="five columns confirm-payment">
                            <a href="<?=base_url() . 'cart/confirm/' . $order->order_id . '/' . $group .'/'?>"><button name="button" style="padding: 10px; margin-top:10px;"><i class="fa fa-check"> </i> Confirm Payment</button></a>
                        </div>
                    <?php endif; ?>
                </div>

                <br/>
                <h4>Payment Instruction</h4>
                <div class="row">
                    <p>
                        <?=$this->Order_Model->get_payment_instruction($this->Order_Model->get_all_meta_value($order->order_id, "payment_method")->value)->payment_method_instruction;?>
                    </p>
                    <br/>
                    <p>
                        If you already have the receipt from the payment establishment, confirm your payment by taking a picture of the
                        receipt, then upload upload it, by clicking "Confirm Payment" Button.
                    </p>
                </div>

                <br/>
                <h4>Order Details</h4>
                <div class="cart-holder">
                    <table class="cart-holder">
                        <tr class="field">
                            <td>Item x Quantity</td>
                            <td>Total</td>
                        </tr>
                        <?php
                            $total=$size=$subtotal= 0;

                            $group = $this->session->userdata('group');
                            $content = $this->Order_Model->get_order_content_by_id($order->order_id);
                         ?>
                        <?php foreach ($content as $key => $row): ?>
                            <?php
                                $value = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                                $product_data = $this->Product_Model->get_product_by_id($value->product_id);
                                $subtotal += intval($row->order_content_qty)*intval($value->product_variant_price);

                                $meta = $this->Product_Model->get_meta_by_id($value->product_id);
                                $pvname = (strpos($value->product_variant_name, ' '))? explode(' ', $value->product_variant_name) : $value->product_variant_name;
                                $desc = "";

                                foreach ($meta as $metaKey => $metaRow) {
                                    if(is_array($pvname)) {
                                        $desc .= $metaRow->product_meta_keyword . ': ' . $pvname[$metaKey] . ' ';
                                    } else {
                                        $desc =  $metaRow->product_meta_keyword . ': ' . $pvname . ' ';
                                    }
                                }
                             ?>
                            <tr class="field">
                                <td><?=$product_data->product_title?><br/>(<?=$desc?>)  x <?=$row->order_content_qty?></td>
                                <td>P <?=number_format(intval($value->product_variant_price)*$row->order_content_qty, 2)?></td>
                            </tr>
                        <?php endforeach; ?>

                        <tbody class="load-discount">
                            <?php
                                $discount = 0;
                                if($this->Order_Model->get_order_discount($order->order_id, 0, 'all') !== null) {
                                    $d = $this->Order_Model->get_order_discount($order->order_id, 0, 'all');
                                    if($d->order_discount_type  == '1') {
                                        $discount = $d->order_discount_value;
                                    } else {
                                        $discount = $subtotal*($d->order_discount_value/100);
                                    }

                                    $subtotal = $subtotal-$discount;
                                ?>
                                    <td>Discount -</td>
                                    <td id="subtotal-value">P <?=number_format($discount, 2)?></td>
                                <?php
                                }
                             ?>
                        </tbody>

                        <tr class="field">
                            <td>Subtotal</td>
                            <td id="subtotal-value">P <?=number_format($subtotal, 2)?></td>
                        </tr>

                        <tbody class="load-shipping">
                            <td>Subtotal + </td>
                            <td id="subtotal-value">P <?=number_format($order->order_shipping, 2)?></td>
                        </tbody>

                        <tbody class="load-payment">
                            <td>Payment Fee + </td>
                            <td id="subtotal-value">P <?=number_format($order->order_additional_payment, 2)?></td>
                        </tbody>

                        <?php
                            $total = $subtotal+$order->order_shipping+$order->order_additional_payment;
                         ?>

                        <tfoot>
                            <tr class="field total-holder">
                                <td>Total</td>
                                <td id="total-value">P <?=number_format($total, 2)?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <br/>
                <h4>Customer Details</h4>
                <div class="cart-holder">
                    <?php
                        $c = $this->Customer_Model->get_cutomer_detail_by_id($order->customer_id);
                     ?>
                    <table>
                        <tr>
                            <td>Full Name</td>
                            <td><?=$c->customer_fname . ' ' . $c->customer_lname?></td>
                        </tr>

                        <tr>
                            <td>Email</td>
                            <td><?=$c->customer_email?></td>
                        </tr>

                        <tr>
                            <td>Phone #</td>
                            <td><?=$order->order_phone?></td>
                        </tr>
                    </table>
                </div>
                <br/>
                <h4>Shipping Details</h4>
                <div class="cart-holder">
                    <table>
                        <tr>
                            <td>Company</td>
                            <td><?=$order->order_company?></td>
                        </tr>

                        <tr>
                            <td>Address</td>
                            <td><?=$order->order_address?></td>
                        </tr>

                        <tr>
                            <td>City</td>
                            <td><?=$order->order_city?></td>
                        </tr>

                        <tr>
                            <td>State</td>
                            <td><?=$order->order_state?></td>
                        </tr>

                        <tr>
                            <td>Postal/Zip Code</td>
                            <td><?=$order->order_zipcode?></td>
                        </tr>

                        <tr>
                            <td>Country</td>
                            <td><?=$order->order_country?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
	</div>

<?php
    $this->load->view('main/footer_view');
?>
