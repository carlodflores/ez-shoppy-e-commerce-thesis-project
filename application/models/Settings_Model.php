<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_Model extends CI_Model {

    function __construct(){
	parent::__construct();
	}

    // Shipping
        function get_all_shipping() {
            return $this->db->get('cs_shipping')->result();
        }

        function insert_shipping($data) {
            $this->db->insert('cs_shipping', $data);
            return $this->db->insert_id();
        }

        function get_shipping_details($id) {
            $this->db->where('shipping_id', $id);
            return $this->db->get('cs_shipping')->row();
        }

        function update_shipping($data, $id) {
            $this->db->where('shipping_id', $id);
            $this->db->update('cs_shipping', $data);
            return $this->db->affected_rows();
        }

    // Courier
        function get_all_courier() {
            return $this->db->get('cs_shipping_courier')->result();
        }

        function insert_courier($data) {
            $this->db->insert('cs_shipping_courier', $data);
            return $this->db->insert_id();
        }

        function get_courier_details($id) {
            $this->db->where('shipping_courier_id', $id);
            return $this->db->get('cs_shipping_courier')->row();
        }

        function update_courier($data, $id) {
            $this->db->where('shipping_courier_id', $id);
            $this->db->update('cs_shipping_courier', $data);
            return $this->db->affected_rows();
        }

}
