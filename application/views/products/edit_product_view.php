<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>

<style media="screen">
    .ui-helper-hidden-accessible {
        display: none;
    }

    .img-items {
		border: 1px solid #e7e7e7;
		margin: 2px 2px 15px 2px !important;
        display: inline;
		border-radius: 3px;
		-webkit-box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
		-moz-box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
		box-shadow: 0 4px 4px -2px rgba(0, 0, 0, 0.5);
	}

    .upload-holder {
        width: 100%;
        padding: 20px;
        height: 50px;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }
    .progress {
        display: block;
        text-align: center;
        width: 0;
        height: 3px;
        background: red;
        transition: width .3s;
    }
    .progress.hide {
        opacity: 0;
        transition: opacity 1.3s;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;">Edit Product</h3><a href="<?=base_url(). 'ez/product/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <div class="push_one fifteen columns">
                        <form action="<?=base_url()?>ez/product/edit/<?=$product->product_id?>/" method="post">
                            <div class="row">
                                <div class="ten columns">
                                    <span class="txt-label" style="font-size: 12px;">Product Name</span>
                                    <input class="input" type="text" name="txt_prod_name" placeholder="Product Name" value="<?=$product->product_title?>">
                                    <br/><?=form_error("txt_prod_name","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?><br/>
                                    <span class="txt-label" style="font-size: 12px;">Product Description</span>
                                    <textarea class="input textarea" name="txt_prod_desc" placeholder="Product Description"><?=$product->product_description?></textarea>
                                    <?=form_error("txt_prod_desc","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?>
                                </div>
                                <div class="five columns">
                                    <span class="txt-label" style="font-size: 12px;">Product Category</span>
                                    <input class="input" type="text" name="txt_prod_categ_temp" placeholder="Product Category" list="txt_prod_categ_temp_list" value="<?=$product->product_type_name?>">
                                    <datalist id="txt_prod_categ_temp_list">
                                        <?php foreach($types as $row): ?>
                                            <option data-value="<?=$row->product_type_id?>" value="<?=$row->product_type_name?>" />
                                        <?php endforeach; ?>
                                    </datalist>
                                    <input type="hidden" name="txt_prod_categ" value="<?=$product->product_type_id?>" />
                                    <?=form_error("txt_prod_categ_temp","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?>
                                    <span class="txt-label txt_prod_categ_temp_error" style="color: red; font-size: 12px;"></span>
                                    <br/><br/>

                                    <span class="txt-label" style="font-size: 12px;">Product Brand</span>
                                    <input class="input" type="text" name="txt_prod_brand_temp" placeholder="Product Brand" list="txt_prod_brand_temp_list" value="<?=$product->product_brand_name?>">
                                    <datalist id="txt_prod_brand_temp_list">
                                        <?php foreach($brands as $row): ?>
                                            <option data-value="<?=$row->product_brand_id?>" value="<?=$row->product_brand_name?>" />
                                        <?php endforeach; ?>
                                    </datalist>
                                    <input type="hidden" name="txt_prod_brand" value="<?=$product->product_brand_id?>"/>
                                    <?=form_error("txt_prod_brand_temp","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?>
                                    <span class="txt-label txt_prod_brand_temp_error" style="color: red; font-size: 12px;"></span>
                                    <br/><br/>

                                    <span class="txt-label" style="font-size: 12px;">Product Tags</span>
                                    <input class="input" type="text" name="txt_prod_tags" placeholder="Product Tags" value="<?=$product->product_tags?>">
                                    <span class="txt-label txt_prod_tags_error" style="font-size: 12px;">Tags are seperated with comma</span><Br/>
                                    <?=form_error("txt_prod_tags","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?>
                                    <br/>
                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="progress"></div>
                                <div class="four columns">
                                    <span class="txt-label">Product Images</span>
                                    <div class="upload-holder medium default btn">
                                        <input type='file' name="file_prod_images" id="inputFile" multiple/>
                                        Select Image
                                    </div>

                                    <input class="input" type="hidden" name="file_prod_images_txt" value="<?=$product->product_img?>" />
                                    <?=form_error("file_prod_images_txt","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?>
                                </div>

                                <div class="eleven columns" id="img-loader">
                                    <?php
                                        if($product->product_img != "") {
                                            $imgs = explode(',', $product->product_img);
                                            foreach ($imgs as $key => $value) {
                                                echo "<div class='img-items'><img src='". base_url() . "/img/products/" .$value ."' stlye='max-width: 100%;' /></div>";
                                            }
                                        } else {
                                            echo '<center><img src="'.base_url().'img/fil.jpg" alt="" /></center>';
                                        }
                                    ?>
                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="eight columns prepend fields">
                                    <span class="txt-label">Pricing</span><br/>
                                    <span class="txt-label" style="font-size: 12px;">Product Price</span><BR/>
                                    <span class="adjoined">₱</span><input class="input wide" type="number" name="txt_prod_price" value="<?=$product->product_price?>"  placeholder="Product Price">
                                    <br/><?=form_error("txt_prod_price","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?><br/>
                                </div>
                            </div>

                            <div class="row" style="border-top: 1px solid #7997c1; margin-top:30px;">
                                <br/>
                                <div class="seven columns prepend field">
                                    <span class="txt-label">Shipping</span><br/>
                                    <div class="row">
                                        <span class="txt-label" style="font-size: 12px;">Size in Units</span><BR/>
                                        <span class="adjoined">Size</span><input class="input wide" type="text" name="txt_prod_weight" value="<?=set_value('txt_prod_weight')?>" placeholder="Product Size">
                                        <br/><?=form_error("txt_prod_weight","<span class='txt-label' style='font-size: 12px;color:red;'>","</span>")?><br/>
                                    </div>
                                </div>

                                <div class="eight columns">

                                </div>
                            </div>

                            <div class="row">
                                <div class="centered four columns">
                                    <center><input class="medium primary btn" type="submit" name="btn_prod_add" value="Update Product" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <br/>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<link rel="stylesheet" href="<?=base_url()?>css/tagit.ui-zendesk.css">
<link rel="stylesheet" href="<?=base_url()?>css/jquery.tagit.css">
<script src="<?=base_url()?>js/tinymce.min.js"></script>
<script src="<?=base_url()?>js/tag-it.js"></script>
<script>

    function readURL(input) {
        $("#img-loader").html("");
        if(input.files.length >= 1) {
            for(var i = 0; i < input.files.length; i++) {
                if (input.files && input.files[i]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $("#img-loader").append("<div class='img-items'><img src='"+ e.target.result +"' stlye='max-width: 100%;' /></div>");
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        } else {
            $("#img-loader").append('<center><img src="<?=base_url()?>img/fil.jpg" alt="" /></center>');
        }
    }

    $("#inputFile").change(function() {
        readURL(this);
    });

    $('input[name="txt_prod_tags"]').tagit({
        singleField: true,
        singleFieldNode: $('input[name="txt_prod_tags"]')
    });

    tinymce.init(
        {
            selector:'textarea',
            menubar: false,
            max_height: 300
        }
    );
</script>

<script type="text/javascript">
    <?php
        if(set_value('txt_variants') != "") {
            $variants = explode(',', set_value('txt_variants'));
            foreach ($variants as $key => $value) {
    ?>
            $('input[name="<?=$value?>_tags"]').tagit({
                singleField: true,
                singleFieldNode: $('input[name="variant_1_tags"]')
            });
    <?php
            }
        }
     ?>
</script>

<script type="text/javascript">


    $(function() {

        $('form').on('keyup keypress', function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });

        $('input[name="txt_prod_categ_temp"], input[name="txt_prod_brand_temp"]').on('change', function() {
            if($(this).val() != "") {
                var name = $(this).attr('name'),
                    data = $('#' + name + "_list"),
                    sel = $(this).val(),
                    id = data.find('option[value="'+ sel +'"]').attr('data-value');

                var oName = (name == "txt_prod_categ_temp")? "txt_prod_categ": oName = "txt_prod_brand";

                if(id == null) {
                    $('input[name="'+ oName +'"]').val(sel);
                    $('.' + name + "_error").html(sel + " will automatically be added to the database.");
                } else {
                    $('input[name="'+ oName +'"]').val(id);
                    $('.' + name + "_error").html(" ");
                }
            } else {
                $('.' + name + "_error").html("This Field is important");
            }
        });

        $('input[name="file_prod_images"]').on('change', function() {
            var form_data = new FormData();
            for(var i = 0; i < $(this).prop('files').length; i++) {
                form_data.append('file[]', $(this).prop('files')[i]);
            }

            if($(this).prop('files').length >= 1) {
                $.ajax({
                    xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                            if (percentComplete === 1) {
                                $('.progress').addClass('hide');
                            }
                        }
                    }, false);
                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                            $('.progress').css({
                                width: percentComplete * 100 + '%'
                            });
                        }
                    }, false);
                    return xhr;
                    },
                    url: '<?=base_url()?>ez/product/image/upload/', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(response){
                        if(response != "") {
                            $("input[name='file_prod_images_txt']").val(response);
                        }
                    }
                });
            }
        });

    });
</script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.min.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
