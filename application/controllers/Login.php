<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
	}

	public function index() {

		if ($this->session->userdata('customer') !== null)
			redirect(base_url());


		$this->form_validation->set_rules('txt_email', ' Email', 'required|xss_clean|callback_check_login');
		$this->form_validation->set_rules('txt_password', ' Password', 'required|xss_clean');

		if($this->form_validation->run()) {
			if($this->input->post('txt_url') != "") {
				redirect($this->input->post('txt_url'));
			} else {
				redirect(base_url() . 'customer/');
			}
		}

		$data['page_title'] = "Hair Geek";
		$this->load->view('main/customer/signin_view', $data);
    }

	public function check_login($id) {
		$email = $this->input->post('txt_email');
		$password = $this->input->post('txt_password');

		if($this->Customer_Model->get_cutomer_detail_by_email($email) !== null) {
			$customer_data = $this->Customer_Model->get_cutomer_detail_by_email($email);
			if(md5($password) == $customer_data->customer_password) {
				$user_data = array(
					"customer_id" => $customer_data->customer_id,
					"fullname" => $customer_data->customer_fname
				);

				$this->session->set_userdata('customer', $user_data);
				return true;
			} else {
				$this->form_validation->set_message('check_login', 'Wrong E-mail & Password combination.');
				return false;
			}
		} else {
			$this->form_validation->set_message('check_login', 'E-mail does not exists.');
			return false;
		}
	}

	public function logout() {
		$this->session->unset_userdata('customer');
		redirect(base_url());
	}
}
