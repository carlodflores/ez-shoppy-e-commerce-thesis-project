<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    a {
        color: #7997c1 ;
    }

    a:hover {
        color: #577195;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class='icon-users'> </i>All Inquiries</h3><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <form method="post" class="append">
                            <span class="txt-label">All Inquiries</span><br/>
                            
                        </form>
                    </div>
                    <div class="centered push_one twelve columns">
                        <table class="paginate" max="10">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th><center><span class="product-title">Full Name</span></center></th>
                                    <th><center><span class="product-title">Message</span></center></th>
                                    <th><center><span class="product-title">Category</span></center></th>
                                    <th><center><span class="product-title">Date</span></center></th>
                                </tr>
                            </thead>

                            <?php foreach($this->Customer_Model->get_all_inquiries() as $row): ?>

                                    <tr class="stocks-row" onclick="javascript:window.location='<?=base_url()?>ez/inquiry/info/<?=$row->inquiry_id?>'">
                                        <td>
                                            <span class="product-title"><?=$row->inquiry_fullname?></span><Br/>
                                            <span class="txt-label"></span>
                                        </td>

                                        <td>
                                            <span class="product-title"><?=substr($row->inquiry_message, 0, 45) . '' . ((strlen($row->inquiry_message) > 45 ) ? '...' : '')?></span><Br/>
                                            <span class="txt-label">Message</span>
                                        </td>

                                        <td>
                                            <span class="product-title"><?=$row->inquiry_category?></span><Br/>
                                            <span class="txt-label">Ccategory</span>
                                        </td>

                                        <td>
                                            <span class="product-title"><?=$row->date_created?></span></br>
                                            <span class="txt-label">Order #</span>
                                        </td>
                                    </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>
$(function() {



    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
