<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content field">
        <div class="row">
            <div class="five columns field">
                <div class="banner-slider-holder">
                    <ul class="slide-wrapper" data-rotate="false">
                        <?php if (strpos($v->product_variant_img, ',')): ?>
                            <?php
                                $img = explode(',', $v->product_variant_img);
                            ?>
                            <?php foreach ($img as $key => $value): ?>
                                <li data-image="<?=base_url()?>img/products/<?=rawurlencode($value)?>"></li>
                            <?php endforeach; ?>

                            <div class="control">
                                <div class="left"><i class="fa fa-angle-left"></i></div>
                                <div class="right"><i class="fa fa-angle-right"></i></div>
                            </div>

                        <?php else: ?>
                            <li data-image="<?=base_url()?>img/products/<?=rawurlencode($v->product_variant_img)?>"></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>

            <div class="nine columns field cart-holder">
                <h3><?=$p->product_title ?> (<?=$v->product_variant_name?>)</h3>

                <?php if ($v->product_variant_compare_price == "0.00"): ?>
                    <span class="product-price" style="font-size: 24px; ">P <?=number_format($v->product_variant_price, 2)?></span>
                <?php else: ?>
                    <span class="product-price sale" style="font-size: 24px; color: green;">P <?=number_format($v->product_variant_price, 2)?></span>
                    <span class="product-price old" style="font-size: 24px; text-decoration: line-through; color: red;">P <?=number_format($v->product_variant_compare_price, 2)?></span>
                <?php endif; ?>
                <BR/>
                <span class="text-label input-label" style="color: #000 !important;">SKU :  <?=$v->product_variant_sku?></span><br/>
                <span class="text-label input-label" style="color: #000 !important;">Brand :  <?=$p->product_brand_name?></span><br/>
                <span class="text-label input-label" style="color: #000 !important;">Category :  <?=$p->product_type_name?></span><br/>
                <br/>
                <span class="text-label input-label">Quantity <span class="text-indicator">*</span></span><br/>

                <form class="" action="<?=base_url()?>cart/add/<?=$v->product_variant_id?>/" method="get">
                    <select class="input narrow" name="txt_qty">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                    <button type="submit" style="padding: 5px; font-size: 17px;"> <i class="fa fa-plus"> </i> Add to Cart</button>
                </form>
                <br/>
                <p>
                    <?=$p->product_description?>
                </p>
            </div>
        </div>

        <div class="row">
            <center>
                <h2>Related Products</h2>
                <hr style="height: 1px; border: none; background: #000; width: 100px;">
            </center>
            <ul class="product-holder">
                <?php foreach ($this->Product_Model->get_product_related($p->product_type_id, $v->product_variant_id) as $key => $value): ?>
                    <?php
                        $product_data = $this->Product_Model->get_product_by_id($value->product_id);

                        $meta = $this->Product_Model->get_meta_by_id($value->product_id);
                        $pvname = (strpos($value->product_variant_name, ' '))? explode(' ', $value->product_variant_name) : $value->product_variant_name;
                        $desc = "";

                        foreach ($meta as $metaKey => $metaRow) {
                            if(is_array($pvname)) {
                                $desc .= $metaRow->product_meta_keyword . ': ' . $pvname[$metaKey] . ' ';
                            } else {
                                $desc =  $metaRow->product_meta_keyword . ': ' . $pvname . ' ';
                            }
                        }

                        $img = $value->product_variant_img;
                        if(strpos($img, ',') >= 1) {
                            $img_ex = explode(',', $img);
                            $img = $img_ex[0];
                        }
                     ?>
                    <li class="product">
    					<div class="content">
    						  <center>
                                   <a href="<?=base_url() . 'product/desc/' . $value->product_variant_id . '/' . strtolower(str_replace(' ', '-', $product_data->product_title . ' ' . $value->product_variant_name)) . '/'?>">
    							  <div class="product-img">
    							  	<img src="<?=base_url()?>img/products/<?=$img?>" alt="">
    							  </div>
    							  <br/>
    							  <div class="product-details">
    								<span class="product-title"><?=$product_data->product_title?><br/>(<?=$desc?>)</span>
    								<div class="rotate-content">
    									<ul class="content-rotate">
    										<li>
        											<?php if ($value->product_variant_compare_price == "0.00"): ?>
        											    <span class="product-price">P <?=number_format($value->product_variant_price, 2)?></span>
        											<?php else: ?>
                                                        <span class="product-price sale">P <?=number_format($value->product_variant_price, 2)?></span>
                                                        <span class="product-price old">P <?=number_format($value->product_variant_compare_price, 2)?></span>
        											<?php endif; ?>

    										</li>
    										<li>
        										<a href="<?=base_url()?>cart/add/<?=$value->product_variant_id?>/?redirect_to=<?="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"><button><i class="fa fa-plus"> </i> Add to Cart</button></a>
    										</li>
    									</ul>
    								</div>
    							  </div>
                                  </a>
    						  </center>
    					</div>
    				</li>
                <?php endforeach; ?>
			</ul>
		</div>
        </div>
	</div>
<?php
    $this->load->view('main/footer_view');
?>
