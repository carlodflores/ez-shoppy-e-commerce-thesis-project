<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

    function get_option_by_value($keyword) {
        $this->db->where('option_keyword', $keyword);
        return $this->db->get('cs_options')->row();
    }

}
?>
