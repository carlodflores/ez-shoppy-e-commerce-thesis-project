<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }

    a {
        color: #7997c1;
    }

    a:hover {
        color: #7999e2;
    }

    .ship-fields input {
        margin-top: 10px;
    }

    .show-holder {
        background: #fff; border-radius: 3px; padding: 10px; width: 200px; position: absolute; display: none; z-index: 999999;
        border: 2px solid rgba(121, 151, 193, 0.6);
    }
</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-download"> </i>Update Order #<?=$order_id?></h3><a href="<?=base_url(). 'ez/order/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid field">
                <div class="row">
                    <div class="push_one fifteen columns">
                        <form id="process" action="<?=base_url()?>ez/order/fulfillment/<?=$order_id?>/" method="post">
                        <input type="hidden" class="react" name="variants_added" value="" />
                        <div class="row">
                            <div class="nine columns">
                                <h3 class="content-title">Order Details</h3>
                                <div class="row">
                                    <div>
                                        <table id="order-details-holder">
                                            <?php foreach ($this->Order_Model->get_order_content_by_id($order_id) as $key => $row) {
                                                $var_data = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                                                $img = $var_data->product_variant_img;
                                                if(strpos($var_data->product_variant_img, ',')) {
                                                    $img = explode(',', $var_data->product_variant_img);
                                                }
                                            ?>
                                                <tr class="stocks-row loaded-order <?=$row->order_content_id?>_row" data-value="<?=$row->product_variant_id?>">
                                                    <td><img src="<?=base_url() ?>img/products/<?=is_array($img)? $img[0] : $img?>" width="50"></td>
                                                    <td>
                                                        <span class="product_title"><?=$this->Product_Model->get_product_by_id($var_data->product_id)->product_title?></span>
                                                        <Br/><span class="txt-label"><?=str_replace(' ', ' / ', $var_data->product_variant_name)?></span>
                                                    </td>
                                                    <td>
                                                        <a class="show-panel" data-trigger="show-item-disc-<?=$row->product_variant_id?>">
                                                            P <?=$var_data->product_variant_price?>
                                                        </a> x <?=$row->order_content_qty?>
                                                        <input type="hidden" min="0" class="input narrow qty_input react" name="qty_<?=$row->product_variant_id?>" data-name="qty_<?=$row->product_variant_id?>" data-orig="<?=$var_data->product_variant_price?>" data-price="<?=$var_data->product_variant_price?>" placeholder="Quantity" value="<?=$row->order_content_qty?>">
                                                    </td>
                                                    <td class="v_price_display">
                                                        P <?=$var_data->product_variant_price?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <h3 class="content-title" style="float: none;">Tracking Details (Required)</h3>
                                    <div class="eight columns">
                                        <span class="txt-label" style="font-size: 12px;">Tracking Number</span>
                                        <input type="text" class="input react" name="txt_tracking_number" placeholder="Tracking Number" />
                                    </div>
                                    <div class="seven columns">
                                        <span class="txt-label" style="font-size: 12px;">Courier</span>
                                        <select class="input xxwide" name="txt_tracking_courier">
                                            <?php foreach ($this->Settings_Model->get_all_courier() as $key => $value): ?>
                                                <option value="<?=$value->shipping_courier_name?>"><?=$value->shipping_courier_name?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>
                                <Br/>
                                <p>
                                    This will automatically send an e-mail to the customer.
                                </p>
                                <Br/>
                                <input type="hidden" name="txt_order_total" value="" />
                                <div class="row">
                                    <div class="centered four columns">
                                        <BR/>
                                        <center><input class="medium primary btn react" type="submit" name="btn_add_order" value="Fulfill Order" style="color: #fff; font: 700 16px 'Open Sans', sans-serif;"></center>
                                        <BR/><BR/>
                                    </div>
                                </div>
                            </div>

                            <div class="seven columns">
                                <div class="row">
                                    <h3 class="content-title">Customer Details</h3>
                                    <table></table>
                                </div>
                                <?php $ci = $this->Customer_Model->get_cutomer_detail_by_id($order->customer_id); ?>
                                <div class="row">
                                    <input type="text" class="input" name="txt_customer_email" placeholder="Customer Email" list="costumer_list" value="<?=$ci->customer_email?>"/>
                                </div>

                                <div class="row">
                                    <BR/>
                                    <span class="txt-label">Customer Profile</span>
                                    <div class="row">
                                        <div class="four columns">
                                            <img src="<?=base_url()?>img/profile-placeholder.jpg" id="customer_img" width="80" style="border-radius: 50%;" alt="" />
                                        </div>
                                        <div class="twelve columns">
                                            <h3 class="content-title" id="customer_name" style="color: #000; float:none; margin-top: 10px; "><?=$ci->customer_fname . ' ' .$ci->customer_lname?></h3>
                                            <h3 class="content-title" id="customer_email" style="color: #000; margin-top: -10px; font-size: 12px;"><?=$ci->customer_email?></h3>
                                        </div>
                                    </div>

                                    <div class="row ship-fields">
                                        <span class="txt-label">Shipping Details</span>
                                        <div class="row">
                                            <div class="eight columns">
                                                <span class="txt-label" style="font-size: 12px;">Company</span>
                                                <input type="text" class="input" name="customer_company" placeholder="Company" value="<?=$order->order_company?>"/>
                                            </div>
                                            <div class="eight columns">
                                                <span class="txt-label" style="font-size: 12px;">Phone</span>
                                                <input type="text" class="input" name="customer_phone" placeholder="Phone" value="<?=$order->order_phone?>" />
                                            </div>
                                        </div>
                                        <span class="txt-label" style="font-size: 12px;">Address</span>
                                        <input type="text" class="input" name="customer_address" placeholder="Address" value="<?=$order->order_address?>" />

                                        <div class="row">
                                            <div class="eight columns">
                                                <span class="txt-label" style="font-size: 12px;">City</span>
                                                <input type="text" class="input" name="customer_city" placeholder="City" value="<?=$order->order_city?>" />
                                            </div>
                                            <div class="eight columns">
                                                <span class="txt-label" style="font-size: 12px;">Zip/Postal Code</span>
                                                <input type="text" class="input" name="customer_zipcode" placeholder="Postal/Zip Code" value="<?=$order->order_zipcode?>" />
                                            </div>
                                        </div>
                                        <Br/><br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>

    $(function() {
        generate_subtotal();

        <?php if ($order->order_payment_status != 2): ?>
            $(document).on('click', '.variant_remover', function() {
                $('tr.'+ $(this).attr('data-value')).remove();
                generate_suggestion();
                generate_subtotal();
            });

            $("div[data-tag='discount_option']").click(function() {
                var action = $(this).attr('data-action');
                $("div[data-tag='discount_option']:eq("+(action-1)+")").removeClass('default').addClass('primary');
                $("div[data-tag='discount_option']:not(':eq("+(action-1)+")')").removeClass('primary').addClass('default');

                $('input[name="discount_value"]').attr('data-action', action);
                $('input[name="discount_value_type"]').val(action);
                switch (action) {
                    case '1':
                        $('input[name="discount_value"]').removeAttr('max');
                        break;

                    case '2':
                        $('input[name="discount_value"]').attr({'min': '0', 'max': '100'});
                        break;
                    default:

                }
                generate_subtotal();
            });
        <?php endif; ?>

        <?=($order->order_payment_status==2)? "$('#process input').not('.react').attr('disabled', 'disabled');" : ""?>

        $("#btn_mark_pending, #btn_mark_paid, #btn_mark_fulfill").click(function(){
            var action = $(this).attr('data-action'),
                id = "<?=$order_id?>";

            $.post("<?=base_url()?>ez/order/marker/" + id +"/" + action + "/", function(data) {
                if(data == "OK!") {
                    location.reload();
                }
            });
        });

        $('input[name="search_temp"]').keyup('change', function() {
            generate_suggestion();
        });

        $(document).on('click', 'a.show-panel', function(){
            var offset = $(this).offset();
    		var posY = offset.top - $(window).scrollTop() + $(this).height() - 20;
    		var posX = offset.left - $(window).scrollLeft();
            var action = $(this).attr('data-trigger');
            $('.item_discount').trigger("change");

            var box = $("div[data-action='"+ action +"']");
            box.css({"top": posY, "left": posX}).toggle({direction: "left"}, "fast");
        });

        <?php if ($order->order_payment_status != 2): ?>
        $(document).on('click', 'div[data-tag="discount_option_item"]', function() {
            var action = $(this).attr('data-action');
            var name = $(this).attr('data-name');
            var id = name.split('-')[1];

            $("div[data-tag='discount_option_item'][data-name='"+ name +"']:eq("+(action-1)+")").removeClass('default').addClass('primary');
            $("div[data-tag='discount_option_item'][data-name='"+ name +"']:not(':eq("+(action-1)+")')").removeClass('primary').addClass('default');
            $("input[name='discount_value_"+id+"']").val("");

            $('input[name="discount_value_'+ id +'"]').attr('data-action', action);
            $('input[name="discount_value_'+ id +'_type"]').val(action);
            switch (action) {
                case '1':
                    $('input[name="discount_value_'+ id +'"]').removeAttr('max');
                    break;

                case '2':
                    $('input[name="discount_value_'+ id +'"]').attr({'min': '0', 'max': '100'});
                    break;
                default:
            }

            generate_subtotal();
        }).trigger('click');
        <?php endif; ?>

        $('input[name="txt_customer_email"]').on('change', function() {
            var uemail = $(this).val();
            $.post('<?=base_url()?>api/customer/get_info_email/', {uemail: uemail}, function(data){
                if(data != "null") {
                    var data = $.parseJSON(data);
                    $("#customer_name").html(data.customer_fname + " " + data.customer_lname);
                    $("#customer_email").html(data.customer_email);

                    $('input[name="customer_company"]').val(data.customer_company);
                    $('input[name="customer_phone"]').val(data.customer_phone);
                    $('input[name="customer_address"]').val(data.customer_address);
                    $('input[name="customer_city"]').val(data.customer_city);
                    $('input[name="customer_zipcode"]').val(data.customer_zipcode);

                    if(data.customer_img == "") {
                        $('#customer_img').attr('src', '<?=base_url()?>img/profile-placeholder.jpg');
                    } else {
                        $('#customer_img').attr('src', '<?=base_url()?>img/customer/'+ data.customer_img);
                    }
                } else {
                    $("#customer_name").html("");
                    $("#customer_email").html("");

                    $('input[name="customer_company"]').val("");
                    $('input[name="customer_phone"]').val("");
                    $('input[name="customer_address"]').val("");
                    $('input[name="customer_city"]').val("");
                    $('input[name="customer_zipcode"]').val("");
                    $('#customer_img').attr('src', '<?=base_url()?>img/profile-placeholder.jpg');
                }
            });
        }).trigger('change');

        $(document).on('click', '.add-order', function(){
            var count = Math.floor((Math.random() * 1000000) + 1);
            var pvid = $(this).attr('data-value');
            $.post('<?=base_url()?>api/product/get/', {pvid:pvid}, function(data) {
                var data = $.parseJSON(data);
                var img = (data.product_variant_img.indexOf(',') >= 1)? data.product_variant_img.split(',')[0] : data.product_variant_img;

                $('#order-details-holder').append('<tr class="stocks-row loaded-order '+ count +'_row" data-value="'+ data.product_variant_id +'"><td><img src="<?=base_url() ?>img/products/'+ img +'" width="50"></td><td><span class="product_title">'+ data.product_title +'</span><Br/><span class="txt-label">'+ data.product_variant_name.replace(' ', ' / ') +'</span></td><td><a class="show-panel" data-trigger="show-item-disc-'+data.product_variant_id+'">P '+ data.product_variant_price +'</a> x <input type="number" min="0" class="input narrow qty_input" name="qty_'+data.product_variant_id+'" data-name="qty_'+data.product_variant_id+'" data-orig="'+ data.product_variant_price +'" data-price="'+ data.product_variant_price +'" placeholder="Quantity" value="1"></td><td class="v_price_display">P '+ data.product_variant_price +'</td><td style="width: 20px;"><span class="medium default btn variant_remover" data-value="'+ count +'_row"><i class="icon-cancel"></i></span><span></span></td></tr>')
                $('.show-container').append('<div class="show-holder field" data-action="show-item-disc-'+data.product_variant_id+'" class="field"><span class="prepend append" style="margin-right: 20px; "><div class="medium primary btn" data-action="1" data-name="disc-'+data.product_variant_id+'" data-tag="discount_option_item" style="font-size: 12px;"><a>P</a></div><div class="medium default btn" data-action="2" data-name="disc-'+data.product_variant_id+'" data-tag="discount_option_item" style="font-size: 12px;"><a>%</a></div></span><span><input type="number" min="0" class="input normal item_discount" name="discount_value_'+data.product_variant_id+'" data-action="1" value=""></span><br/><input type="hidden" name="discount_value_'+data.product_variant_id+'_type" value="1" /><input type="text" style="margin-top: 10px;" class="input" name="discount_value_'+data.product_variant_id+'_reason" placeholder="Reason" value=""><br/><center><div class="close-item-disc" data-name="close_'+data.product_variant_id+'"><span class="txt-label" style="float: none;">Close</span></div></center></div>');
                $('input[name="search_temp"]').val("");
                generate_suggestion();
                generate_subtotal();
            });
        });

        $(document).on('click', '.close-item-disc', function() {
            var id = $(this).attr('data-name').split("_")[1];
            $("div[data-action='show-item-disc-"+id+"']").hide("fast");
        });

        $(document).on('change', '.qty_input', function() {
            generate_subtotal();
        });

        $(document).on('change', '.item_discount', function() {
            generate_subtotal();
        });

        $('input[name="discount_value"]').on('change', function() {
            generate_subtotal();
        }).trigger('change');


        function generate_subtotal() {
            var total = 0, subtotal = 0;
            var var_added = [];

            for(var h = 0; h < $('.item_discount').length; h++) {
                var ele = $('.item_discount:eq('+h+')');
                var id = ele.attr('name').split("_")[2];
                var action = ele.attr('data-action');
                var price = $('.qty_input[data-name="qty_'+id+'"]').attr('data-orig');
                var val = ele.val();

                if(val == "" || val == 0) {
                    var price = +($('.qty_input[data-name="qty_'+id+'"]').attr('data-orig'));
                    $('.qty_input[data-name="qty_'+id+'"]').attr('data-price', price.toFixed(2));
                    $('a[data-trigger="show-item-disc-'+id+'"]').html("P " +  price.toFixed(2));
                    continue;
                }


                var p_holder = $('.qty_input[data-name="qty_'+id+'"]');
                switch (action) {
                    case '1':
                        p_holder.attr('data-price', (+price-+val));
                        break;
                    case '2':
                        p_holder.attr('data-price', ( +price-(+price*(val/100))));
                        break;
                }

                var n_price = +(p_holder.attr('data-price'))*1;
                $('a[data-trigger="show-item-disc-'+id+'"]').html("<font style='font-size: 13px; color: #bbbbbb !important;'>P "+ price +"</font style='font-size: 13px;'> / P " +  n_price.toFixed(2));
            }

            for(var h = 0; h < $('input.qty_input').length; h++) {
                var p_perItem = +($('input.qty_input:eq('+ h +')').attr('data-price'))*+($('input.qty_input:eq('+ h +')').val());
                $('.v_price_display:eq('+ h +')').html('P ' + p_perItem.toFixed(2));
                total += p_perItem;
                var_added.push($('input.qty_input:eq('+ h +')').attr('data-name').split('_')[1]);
            }

            $('input[name="variants_added"]').val(var_added.join(','));
            if($('input[name="discount_value"]').val() != "") {
                var action = $('input[name="discount_value"]').attr('data-action');
                var disc = $('input[name="discount_value"]').val();
                switch (action) {
                    case '1':
                        total = +total-+disc;
                        break;

                    case '2':
                        total = +total-(+total*(disc/100));
                        break;
                    default:

                }
            }

            $('input[name="txt_order_total"]').val(total.toFixed(2));
            $('td.subtotal-display').html('P '+ total.toFixed(2));
            $('td.total-display').html('P '+ total.toFixed(2));
        }

        function generate_suggestion() {
            $('#pre-suggestion').html('');
            if($('input[name="search_temp"]').val() != "") {
                var order_list = [];
                var req = $('input[name="search_temp"]').val();
                for(var h = 0; h < $('tr.loaded-order').length; h++) {
                    order_list.push($('tr.loaded-order:eq('+ h +')').attr('data-value'));
                }

                $.post('<?=base_url()?>api/product/search/', {req:req}, function(data) {
                    var data = $.parseJSON(data);
                    $.each(data, function(k, v) {
                        if(jQuery.inArray(data[k].product_variant_id, order_list) < 0) {
                            var img = (data[k].product_variant_img.indexOf(',') >= 1)? data[k].product_variant_img.split(',')[0] : data[k].product_variant_img;
                            $('#pre-suggestion').append('<tr class="stocks-row"><td style="text-align: center;"><img src="<?=base_url()?>img/products/'+ img +'" width="50" /></td><td style="text-align: center;"><span class="product-title">'+ data[k].product_title +'</span><Br/><span class="txt-label" style="color: #000;">'+ data[k].product_variant_name.replace(' ', ' / ') +'</span></td><td style="text-align: center;">'+ data[k].product_variant_price +'</td><td style="text-align: center;"><span class="btn-more add-order" data-value="'+ data[k].product_variant_id +'_'+ data[k].product_id +'" style="margin-right: 5px !important; float: none;">Add</span></td></tr>');
                        }
                    });
                    $("#product_loader").slideDown("fast");
                });
            } else {
                $("#product_loader").slideUp("fast");
            }
        }
    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
