<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_Model extends CI_Model {

	function __construct(){
	parent::__construct();
	}

    function insert_cart($data) {
        $this->db->insert('cs_cart', $data);
        return $this->db->insert_id();
    }

    function get_cart_content($ip, $group) {
        $this->db->where('cart_group', $group);
        $this->db->where('customer_ip', $ip);
		$this->db->where('cart_status', '0');
        return $this->db->get('cs_cart')->result();
    }

    function check_product_exists($ip, $group, $pvid) {
        $this->db->where('cart_group', $group);
        $this->db->where('customer_ip', $ip);
        $this->db->where('product_variant_id', $pvid);
		$this->db->where('cart_status', '0');
        return $this->db->get('cs_cart')->row();
    }

	function update_cart($cart_id, $data) {
        $this->db->where('cart_id', $cart_id);
        $this->db->update('cs_cart', $data);
        return $this->db->affected_rows();
    }

	function update_cart_by_group($group, $data) {
        $this->db->where('cart_group', $group);
        $this->db->update('cs_cart', $data);
        return $this->db->affected_rows();
    }

    function get_cart_by_id($cart_id) {
        $this->db->where('cart_id', $cart_id);
        return $this->db->get('cs_cart')->row();
    }

    function delete_cart($cart_id) {
        $this->db->where('cart_id', $cart_id);
        $this->db->delete('cs_cart');
        return $this->db->affected_rows();
    }

	function get_cart_by_ip($ip) {
        $this->db->where('customer_ip', $ip);
		$this->db->where('cart_status', '0');
        return $this->db->get('cs_cart')->row();
	}
}
