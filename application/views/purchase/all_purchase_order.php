<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn">
                    <h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Purchase Orders</h3>
                    <a href="<?=base_url(). 'ez/product/purchase/add/'?>">
                        <span class="btn-more" style="color:#fff;">Add PO</span>
                    </a>

                    <a href="#" class="switch" gumby-trigger="#supplier_modal">
                        <span class="btn-more" style="color:#fff;">Add Supplier</span>
                    </a>
                    <Br/><Br/>
                </div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">

                </div>
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <center><h2>Purchase Orders</h2>
                        <p>List of all the purchase orders made</p></center>

                    </div>

                    <div class="centered push_one twelve columns">
                        <Br/>

                        <table class="paginate" max="10">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th><center><span class="product-title">Purchase Order Number</span></center></th>
                                    <th><center><span class="product-title">Worth</span></center></th>
                                    <th><center><span class="product-title">Supplier</span></center></th>
                                    <th><center><span class="product-title">Date Created</span></center></th>
                                    <th><center><span class="product-title">Status</span></center></th>
                                    <th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach ($this->Order_Model->get_all_purchase() as $key => $value): ?>
                                <?php
                                    $worth = 0;

                                    foreach ($this->Order_Model->get_purchase_content($value->purchase_order_id) as $key => $row) {
                                        $vi = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                                        $worth += $vi->product_variant_price*$row->purchase_order_content_qty;
                                    }
                                 ?>
                            <tr class="stocks-row">
                                <td>
                                    <span class="product-title"><a href="<?=base_url()?>ez/product/purchase/details/<?=$value->purchase_order_id?>"><?=$value->purchase_order_id?></a></span><Br/>
                                    <span class="txt-label"></span>
                                </td>

                                <td>
                                    <span class="product-title">P <?=number_format($worth, 0)?></span><Br/>
                                    <span class="txt-label">Worth</span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$this->Product_Model->check_supplier_exist($value->product_supplier_id)->product_supplier_name?></span><Br/>
                                    <span class="txt-label">Supplier</span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$value->date_created?></span><Br/>
                                    <span class="txt-label" >Date Created</span>
                                </td>

                                <td>
                                    <span class="product-title">
                                        <?php if ($value->purchase_order_status == 1): ?>
                                            <span class="warning label" style="text-transform: uppercase;">Pending</span>
                                        <?php elseif($value->purchase_order_status == 0): ?>
                                            <span class="success label" style="text-transform: uppercase;">Received</span>
                                        <?php else: ?>
                                            <span class="danger label" style="text-transform: uppercase;">Back Order</span>
                                        <?php endif; ?>
                                    </span><Br/>
                                    <span class="txt-label" ></span>
                                </td>

                                <td>
                                    <?php if ($value->purchase_order_status == 1): ?>
                                        <a href="<?=base_url(). 'ez/product/purchase/receive/' . $value->purchase_order_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Receive</span></a>
                                        <a href="<?=base_url(). 'ez/product/purchase/backorder/' . $value->purchase_order_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Back Order</span></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>

        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>

<div class="modal" id="supplier_modal">
    <div class="content">
        <a class="close switch" gumby-trigger="|#supplier_modal"><i class="icon-cancel" /></i></a>

        <div class="row field">
            <div class="centered six columns">
                <center>
                    <h3>Add Supplier</h3>
                    <span class="txt-label" style="font-size: 12px;">Supplier Name</span>
                    <input type="text" class="input" name="name" placeholder="Supplier Name"><br/>
                    <span class="txt-label" style="font-size: 12px;">Email</span>
                    <input type="text" class="input" name="name" placeholder="Email">
                    <span class="txt-label" style="font-size: 12px;">Contact #</span>
                    <input type="text" class="input" name="name" placeholder="Contact #">
                </center>
            </div>
        </div>
    </div>
</div>



<script>
$(function() {



    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
