<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_Model extends CI_Model {

    function __construct(){
    parent::__construct();
    }

    // POS Login
        function check_email($email) {
            $this->db->where('account_email', $email);
            $this->db->where('account_level', 'chr');
            return $this->db->get('cs_accounts')->row();
        }

        function check_creds($email, $password) {
            $this->db->where('account_email', $email);
            $this->db->where('account_password', $password);
            $this->db->where('account_level', 'chr');
            return $this->db->get('cs_accounts')->row();
        }

    // ADM Login

    function check_adm_login($email, $password) {
        $this->db->where('account_email', $email);
        $this->db->where('account_password', $password);
        $this->db->where('account_level', 'adm');
        return $this->db->get('cs_accounts')->row();
    }

    function get_account_by_id($account_id) {
        $this->db->where('account_id', $account_id);
        return $this->db->get('cs_accounts')->row();
    }

}
