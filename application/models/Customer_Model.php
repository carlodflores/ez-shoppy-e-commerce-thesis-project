<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_Model extends CI_Model {

    function __construct(){
	parent::__construct();
	}

    function get_all_customers() {
        return $this->db->get('cs_customers')->result();
    }

    function gel_all_customers_by_status($status) {
        $this->db->where('customer_status', $status);
        return $this->db->get('cs_customers')->result();
    }

    function get_cutomer_detail_by_email($email) {
        $this->db->where('customer_email', $email);
        return $this->db->get('cs_customers')->row();
    }

    function get_cutomer_detail_by_id($customer_id) {
        $this->db->where('customer_id', $customer_id);
        return $this->db->get('cs_customers')->row();
    }

    function update_customer($customer_id, $data) {
        $this->db->where('customer_id', $customer_id);
        $this->db->update('cs_customers', $data);
        return $this->db->affected_rows();
    }

    function insert_customer($data) {
        $this->db->insert('cs_customers', $data);
        return $this->db->insert_id();
    }

    // Mailing list

        function insert_newsletter($data) {
            $this->db->insert('cs_newsletter', $data);
            return $this->db->insert_id();
        }

        function get_newsletter() {
            $this->db->select('group_concat(newsletter_email) as emails', false);
            $this->db->from('cs_newsletter');
            $this->db->where('newsletter_status', '1');
            $this->db->group_by('newsletter_status');
            return $this->db->get()->row();
        }

    // Feedback

        function get_limited_feedback($limit) {
            $this->db->order_by('date_created', 'desc');
            $this->db->limit($limit);
            if($limit == 1)
                return $this->db->get('cs_feedbacks')->row();
            else
                return $this->db->get('cs_feedbacks')->result();
        }
		
		function get_all_feedback() {
            $this->db->order_by('date_created', 'desc');
			return $this->db->get('cs_feedbacks')->result();
        }

    // Customer

    function insert_inquiry($data) {
        $this->db->insert('cs_inquiries', $data);
        return $this->db->insert_id();
    }

    function get_all_inquiries() {
        $this->db->order_by('date_created', 'desc');
        return $this->db->get('cs_inquiries')->result();
    }

    function get_inquiry_details($id) {
        $this->db->where('inquiry_id', $id);
        return $this->db->get('cs_inquiries')->row();
    }

    function insert_inquiry_reply($data) {
        $this->db->insert('cs_inquiry_replies', $data);
        return $this->db->insert_id();
    }

    function get_inquiry_reply($id) {
        $this->db->where('inquiry_id', $id);
        return $this->db->get('cs_inquiry_replies')->result();
    }

    // visitor
        function insert_visitor($data) {
            $this->db->insert('cs_visitors', $data);
            return $this->db->insert_id();
        }

        function get_visitor_details($ip) {
            $this->db->where('visitor_ip', $ip);
            return $this->db->get('cs_visitors')->row();
        }
}
