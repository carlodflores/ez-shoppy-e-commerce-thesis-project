<?php
    $this->load->view('header_view');
?>

<style media="screen">
    .login-holder {
        background: #fff;
        width: 100%;
        height: 300px;
        border-radius: 10px;
        padding: 10px;
        margin-top: 10%;
    }

    input[type="submit"] {
        width: 100px;
        margin-right: 25px !important;
        margin-top: 10px;
        border: 1px solid #5793e8;
        color: #5793e8;
        text-transform: uppercase;
        font: 700 14px 'Open Sans', sans-serif;
        background: transparent;
        border-radius: 3px;
        text-decoration: none;
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
        float: right;
    }

    .btn:hover {
        color: #5793e8;
        border-color: #5793e8;
    }

</style>

<div class="sixteen colgrid">
    <div class="row">
        <div class="centered four columns login-holder field">
            <center>
                <img src="<?=base_url()?>img/ez-Login.jpg" alt="" />

                <form class="" action="<?=base_url()?>ez/login/" method="post">
                    <span class="txt-label">Email</span><Br/>
                    <input class="input xwide" type="text" name="txt_email"  placeholder="Email"/>
                    <div class="clearfix"></div>
                    <span class="txt-label">Password</span><Br/>
                    <input class="input xwide" type="password" name="txt_password"  placeholder="Password"/>

                    <input type="submit" value="Login" />
                </form>
            </center>
        </div>
    </div>
</div>

<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
