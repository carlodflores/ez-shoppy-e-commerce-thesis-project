<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	function __construct(){
	parent::__construct();
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
		$this->load->Model('Order_Model');
	}

    public function index() {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		if(count($this->Customer_Model->get_all_customers()) <= 0) {
			$data['page_module'] = "<i class='icon-users'> </i>All Customers";
			$this->load->view('alerts/alert_customer_zero_view', $data);
		} else {
			$data['customers'] = $this->Customer_Model->get_all_customers();
			$this->load->view('customers/all_customer_view', $data);
		}

    }

	public function profile($customer_id) {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;
		$data['title'] = "EZ Dashboard - " . $store_name;

		if(count($this->Customer_Model->get_cutomer_detail_by_id($customer_id)) <= 0) {
			$data['page_module'] = "<i class='icon-users'> </i>All Customers";
			$this->load->view('alerts/alert_customer_exists_view', $data);
		} else {

			$this->form_validation->set_rules('customer_company', ' Company', 'required|xss_clean');
			$this->form_validation->set_rules('customer_phone', ' Phone', 'required|xss_clean');
			$this->form_validation->set_rules('customer_address', ' Address', 'required|xss_clean');
			$this->form_validation->set_rules('customer_city', ' City', 'required|xss_clean');
			$this->form_validation->set_rules('customer_zipcode', ' Zipcode', 'required|xss_clean');

			if($this->form_validation->run()) {
				$user_data = array(
					"customer_company" => $this->input->post('customer_company'),
					"customer_phone" => $this->input->post('customer_phone'),
					"customer_address" => $this->input->post('customer_address'),
					"customer_city" => $this->input->post('customer_city'),
					"customer_state" => $this->input->post('customer_state'),
					"customer_zipcode" => $this->input->post('customer_zipcode'),
					"customer_status" => '1'
				);

				$this->Customer_Model->update_customer($customer_id, $user_data);
			}

			$data['customer'] = $this->Customer_Model->get_cutomer_detail_by_id($customer_id);
			$this->load->view('customers/edit_customer_view', $data);
		}
	}

	public function create() {
		$store_name = $this->Option_Model->get_option_by_value('store_name')->option_value;

		$this->form_validation->set_rules('customer_fname', ' first name', 'required|xss_clean|ucwords');
		$this->form_validation->set_rules('customer_lname', ' last name', 'required|xss_clean|ucwords');
		$this->form_validation->set_rules('customer_email', ' email', 'required|xss_clean|valid_email|is_unique[cs_customers.customer_email]');
		$this->form_validation->set_rules('customer_company', ' company', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', ' phone', 'required|xss_clean');
		$this->form_validation->set_rules('customer_address', ' address', 'required|xss_clean');
		$this->form_validation->set_rules('customer_city', ' city', 'required|xss_clean');
		$this->form_validation->set_rules('customer_province', ' province', 'required|xss_clean');
		$this->form_validation->set_rules('customer_zipcode', ' zipcode', 'required|xss_clean');

		if($this->form_validation->run()) {
			$user_data = array(
				"customer_fname" => $this->input->post('customer_fname'),
				"customer_lname" => $this->input->post('customer_lname'),
				"customer_email" => $this->input->post('customer_email'),
				"customer_company" => $this->input->post('customer_company'),
				"customer_phone" => $this->input->post('customer_phone'),
				"customer_address" => $this->input->post('customer_address'),
				"customer_city" => $this->input->post('customer_city'),
				"customer_state" => $this->input->post('customer_province'),
				"customer_zipcode" => $this->input->post('customer_zipcode')
			);

			$this->Customer_Model->insert_customer($user_data);
			redirect(base_url() . 'ez/customers/');
		}

		$data['title'] = "EZ Dashboard - " . $store_name;
		$this->load->view('customers/add_customer_view', $data);
	}

}
