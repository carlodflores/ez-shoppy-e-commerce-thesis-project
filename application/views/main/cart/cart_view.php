<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content">
        <?php if (isset($_REQUEST['cart_success_edit'])): ?>
            <div class="row">
                <div class="success alert" style="padding: 10px;"><center>Cart Updated.</center></div>
            </div>
        <?php endif; ?>

        <?php if (isset($_REQUEST['cart_failed_delete'])): ?>
            <div class="row">
                <div class="danger alert" style="padding: 10px;"><center>Item Deletion Failed.</center></div>
            </div>
        <?php endif; ?>

        <?php if (isset($_REQUEST['cart_success_delete'])): ?>
            <div class="row">
                <div class="success alert" style="padding: 10px;"><center>Item Deletion Success.</center></div>
            </div>
        <?php endif; ?>
    <form class="" action="<?=base_url()?>cart/update/" method="post">
		<div class="row">
			<div class="sixteen columns">
				<div class="filter-holder">
					<div class="row">
						<ul class="items-control">
							<li>ITEMS ON CART</li>
							<li>
                                <?php if ($this->session->userdata('group') !== null): ?>
                                    <a href="<?=base_url()?>cart/checkout/<?=$this->session->userdata('group')?>"><button type="button" ><i class="fa fa-sign-out"> </i> Checkout</button></a>
                                <?php endif; ?>
                            </li>
						</ul>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="sixteen columns cart-holder">
                <Br/>
                <?php if ($this->session->userdata('group') !== null && count($this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $this->session->userdata('group'))) >= 1): ?>
                    <?php
                        $total = 0;

                        $group = $this->session->userdata('group');
                        $content = $this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $group);
                     ?>
                    <table>
                        <?php foreach ($content as $key => $row): ?>
                            <?php
                                $value = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                                $product_data = $this->Product_Model->get_product_by_id($value->product_id);

                                $total += intval($row->cart_quantity)*intval($value->product_variant_price);

                                $meta = $this->Product_Model->get_meta_by_id($value->product_id);
                                $pvname = (strpos($value->product_variant_name, ' '))? explode(' ', $value->product_variant_name) : $value->product_variant_name;
                                $desc = "";

                                foreach ($meta as $metaKey => $metaRow) {
                                    if(is_array($pvname)) {
                                        $desc .= $metaRow->product_meta_keyword . ': ' . $pvname[$metaKey] . ' ';
                                    } else {
                                        $desc =  $metaRow->product_meta_keyword . ': ' . $pvname . ' ';
                                    }
                                }

                                $img = $value->product_variant_img;
                                if(strpos($img, ',') >= 1) {
                                    $img_ex = explode(',', $img);
                                    $img = $img_ex[0];
                                }
                             ?>
                            <tr class="field">
                                <td><img src="<?=base_url()?>img/products/<?=$img?>" alt="" width="80"></td>
                                <td><?=$product_data->product_title?><br/>(<?=$desc?>)</td>
                                <td>P <?=number_format($value->product_variant_price, 2)?></td>
                                <td>
                                    <input type="hidden" name="cart_id[]" value="<?=$row->cart_id?>">
                                    <input type="number" class="input xxwide" data-id="<?=$row->cart_id?>" name="cart_qty" min="1" max="10" value="<?=$row->cart_quantity?>">
                                </td>

                                <td>
                                    P <?=number_format($value->product_variant_price*$row->cart_quantity, 2)?>
                                </td>

                                <td><button type="button" class="cart_remove" data-id="<?=$row->cart_id?>"><i class="fa fa-close"> </i></button></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                <?php else: ?>
                    <center>
                        <i class="error-icon fa fa-shopping-bag"></i>
                        <p>
                            Your Shopping Bag is Empty.
                        </p>
                        <a href="<?=base_url()?>"><button type="button">Back to Shop</button></a>
                        <br/><br/>
                    </center>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
			<div class="sixteen columns">
				<div class="filter-holder">
					<div class="row">
						<ul class="items-control">
							<li>TOTAL : P <?=(isset($total))? number_format($total, 2) : "0.00"?></li>
							<li>
                                <?php if ($this->session->userdata('group') !== null): ?>
                                    <a href="<?=base_url()?>cart/checkout/<?=$this->session->userdata('group')?>"><button type="button" ><i class="fa fa-sign-out"> </i> Checkout</button></a>
                                <?php endif; ?>
                            </li>
						</ul>
					</div>
				</div>
			</div>
		</div>

    </form>
	</div>

    <script type="text/javascript">
        $(function() {

            $("input[name='cart_qty']").on('change', function() {
                var cart_id = $(this).attr('data-id');
                $.post('<?=base_url()?>cart/update/'  + cart_id + '/', {cart_id:cart_id, cart_qty: $(this).val()}, function(data) {
                    if(data == "OK!") {
                        window.location = "<?=base_url()?>cart/?cart_success_edit";
                    } else {
                        window.location = "<?=base_url()?>cart/?cart_failed_edit";
                    }
                });
            });

            $('.cart_remove').click(function() {
                var cart_id = $(this).attr('data-id');
                $.post('<?=base_url()?>cart/delete/'  + cart_id + '/', {cart_id:cart_id}, function(data) {
                    if(data == "OK!") {
                        window.location = "<?=base_url()?>cart/?cart_success_delete";
                    } else {
                        window.location = "<?=base_url()?>cart/?cart_failed_delete";
                    }
                });
            });
        });
    </script>
<?php
    $this->load->view('main/footer_view');
?>
