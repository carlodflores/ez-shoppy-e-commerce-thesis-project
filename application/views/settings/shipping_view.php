<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Shipping Option</h3>
                    <a href="<?=base_url(). 'ez/settings/shipping/add/'?>"><span class="btn-more" style="color:#fff;">Add Shipping Option</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">

                </div>
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <center><h2>Shipping Option</h2>
                        <p>List of all available shipping</p></center>
                    </div>

                    <div class="centered push_one twelve columns">
                        <Br/>

                        <table class="paginate" max="10">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th><center><span class="product-title">Shipping Alias</span></center></th>
                                    <th><center><span class="product-title">Shipping Fee</span></center></th>
                                    <th><center><span class="product-title">Min Size</span></center></th>
                                    <th><center><span class="product-title">Max Size</span></center></th>
                                    <th><center><span class="product-title">Location</span></center></th>
                                    <th><center><span class="product-title">Date Created</span></center></th>
									<th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach ($this->Settings_Model->get_all_shipping() as $key => $value): ?>
                                <tr class="stocks-row">
                                    <td>
                                        <span class="product-title"><?=$value->shipping_name?></span><Br/>
                                        <span class="txt-label" style="color: #000;"></span>
                                    </td>

                                    <td>
                                        <span class="product-title">P <?=number_format($value->shipping_amount, 2)?></span><Br/>
                                        <span class="txt-label">Amount</span>
                                    </td>

                                    <td>
                                        <span class="product-title"><?=$value->shipping_min?></span><Br/>
                                        <span class="txt-label">Min</span>
                                    </td>

                                    <td>
                                        <span class="product-title"><?=$value->shipping_max?></span><Br/>
                                        <span class="txt-label" >Max</span>
                                    </td>

                                    <td>
                                        <span class="product-title">
                                            <?php if ($value->shipping_type == 1): ?>
                                                Metro Manila
                                            <?php else: ?>
                                                Province
                                            <?php endif; ?>
                                        </span><Br/>
                                        <span class="txt-label" >Location</span>
                                    </td>

                                    <td class="field">
                                        <span class="product-title"><?=$value->date_created?></span><Br/>
                                        <span class="txt-label" >date created</span>
                                    </td>

									<td class="field">
                                        <a href="<?=base_url(). 'ez/settings/shipping/edit/' . $value->shipping_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Edit</span></a>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>

        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>
$(function() {



    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
