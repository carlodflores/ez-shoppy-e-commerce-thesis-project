<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content field">
        <div class="row">
            <div class="seven centered columns text-center field">
                <h3><i class="fa fa-send"> </i> Send an Inquiry</h3>
                <?php if (validation_errors() != ""): ?>
                    <div class="row">
                        <div class="danger alert" style="padding: 10px;"><center><?=validation_errors('<span style="display: block;">','</span>')?></center></div>
                    </div>
                <?php endif; ?>
                <form action="<?=base_url()?>shop/contact/" method="post">
                    <span class="text-label input-label">Email</span> <span class="text-indicator">*</span>
                    <input type="text" class="input" name="txt_email" placeholder="E-mail"value="<?=set_value('txt_email')?>"><br/>

                    <span class="text-label input-label">Full Name</span> <span class="text-indicator">*</span>
                    <input type="text" class="input" name="txt_fullname" placeholder="Full Name"value="<?=set_value('txt_fullname')?>"><br/>

                    <span class="text-label input-label">Category</span> <span class="text-indicator">*</span>
                    <select name="txt_category" id="" class="input xxwide">
                        <option value="Product"<?php echo set_select('txt_category', 'Product', TRUE); ?> >Product</option>
                        <option value="Sale"<?php echo set_select('txt_category', 'Sale', TRUE); ?> >Sale</option>
                        <option value="Policy"<?php echo set_select('txt_category', 'Policy', TRUE); ?> >Policy</option>
                        <option value="Product"<?php echo set_select('txt_category', 'Product', TRUE); ?> >Feedback</option>
                    </select><Br/>

                    <span class="text-label input-label">Message</span> <span class="text-indicator">*</span>
                    <textarea name="txt_message" class="input textarea" rows="4"><?=set_value('txt_message')?></textarea>
                    <Br/><br/>
                    <input type="submit"  class="medium primary btn" name="send" value="Send" />
                </form>
            </div>
        </div>
	</div>
<?php
    $this->load->view('main/footer_view');
?>
