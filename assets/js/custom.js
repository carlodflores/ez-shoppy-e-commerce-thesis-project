

$(document).ready(function() {

	$('.loading').fadeOut();

	// Rotator

	$('ul.content-rotate').hover(function() {
		$(this).animate({'marginTop': '-30px'}, "fast");
	}, function() {
		$(this).animate({'marginTop': '0px'}, "fast");
	});

	// Add Ons
	var fbg = $('.feedback-holder').attr('data-image');
	$('.feedback-holder').css('background-image', 'url('+ fbg +')');


	// Mobile

	$('#mm-trig').click(function() {
		$('.menu-toggle').slideToggle('fast');
	});

	  // Home Slider
	  var rotate = $('.slide-wrapper').attr('data-rotate') == null ? true : false;
	  var totalSlides = $('.slide-wrapper li').length;
	  var ctr = 1;

	  // setting the images
	  $(".slide-wrapper li").each(function() {
			var bg = $(this).attr('data-image');
			$(this).css('background-image', 'url('+ bg +')');
	  });


	  $('.control .right').click(function() {
			if(ctr != totalSlides) {
				  $('.slide-wrapper li:nth-child('+ ctr +')').fadeOut("slow");
				  ctr++;
				  $('.slide-wrapper li:nth-child('+ ctr +')').fadeIn("slow");
			} else {
				  $('.slide-wrapper li:nth-child(1)').fadeIn("slow");
				  $('.slide-wrapper li:nth-child('+ totalSlides +')').fadeOut("slow");
				  ctr = 1;
			}
	  });

	  $('.control .left').click(function() {
			if(ctr > 1 && ctr != 1) {
				  $('.slide-wrapper li:nth-child('+ ctr +')').fadeOut("slow");
				  ctr--;
				  $('.slide-wrapper li:nth-child('+ ctr +')').fadeIn("slow");
				   console.log(ctr);
			} else {
				  ctr = totalSlides;
				  $('.slide-wrapper li:nth-child(1)').fadeOut("slow");
				  $('.slide-wrapper li:nth-child('+ totalSlides +')').fadeIn("slow");
				  console.log(ctr);
			}

	  });

	  if(rotate) {
			setInterval(function() {
				if(ctr != totalSlides) {
					  $('.slide-wrapper li:nth-child('+ ctr +')').fadeOut("slow");
					  ctr++;
					  $('.slide-wrapper li:nth-child('+ ctr +')').fadeIn("slow");
				} else {
					  $('.slide-wrapper li:nth-child(1)').fadeIn("slow");
					  $('.slide-wrapper li:nth-child('+ totalSlides +')').fadeOut("slow");
					  ctr = 1;
				}
			}, 5000);
	  }
});
