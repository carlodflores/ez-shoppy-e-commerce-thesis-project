<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');

    $tm = isset($this->Reports_Model->sales_per_month(date('m', time()))->sales_per_week) ? $this->Reports_Model->sales_per_month(date('m', time()))->sales_per_week : 0;
    $lm = isset($this->Reports_Model->sales_per_month(date('m', strtotime("-30 days", time())))->sales_per_week) ? $this->Reports_Model->sales_per_month(date('m', strtotime("-30 days", time())))->sales_per_week : 0;

    $tm = $tm !== null ? $tm : 0;
    $lm = $lm !== null ? $lm : 0;

    $total = $tm + $lm;

    if($total != 0) {
        $tp = ($tm/$total)*100;
        $lp = ($lm/$total)*100;
    } else {
        $tp = 0;
        $lp = 0;
    }

    $weekly_sales = isset($this->Reports_Model->sales_per_week(date('W', time()))->sales_per_week) ? shorten($this->Reports_Model->sales_per_week(date('W', time()))->sales_per_week) : 0;
    $monthly_sales = isset($this->Reports_Model->sales_per_month(date('m', time()))->sales_per_week) ? shorten($this->Reports_Model->sales_per_month(date('m', time()))->sales_per_week) : 0;
    $yearly_sales = isset($this->Reports_Model->sales_per_year(date('Y', time()))->sales_per_week) ? shorten($this->Reports_Model->sales_per_year(date('Y', time()))->sales_per_week) : 0;
?>
<style>
.bar-holder div div.on-hand { background: #fc4545; width: <?=$tp?>% !important; height: 50px; left: 0; position: absolute; z-index: 1; }
.bar-holder div div.sold { background: #fff; width: <?=$lp?>% !important; height: 50px; left: 0; position: absolute; margin-top: 40px; }
</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columns">
                    <h3 class="banner-title">Sales Overview</h3>
                    <ul class="three_up tiles">
                        <li>
                            <center>
                                <span class="sales-value">P <?=$weekly_sales?></span>
                                <span class="txt-label">This Week</span>
                            </center>
                        </li>
                        <li>
                            <center>
                                <span class="sales-value">P <?=$monthly_sales?></span>
                                <span class="txt-label">This Month</span>
                            </center>
                        </li>
                        <li>
                            <center>
                                <span class="sales-value">P <?=$yearly_sales?></span>
                                <span class="txt-label">This Year</span>
                            </center>
                        </li>
                    </ul>
                </div>

                <div class="nine columns">
                    <h3 class="banner-title">Recent Orders</h3>
                    <table class="field">
                    <?php foreach ($this->Reports_Model->order_pending() as $key => $value): ?>
                        <?php
                        $ci = "";

                          if($value->customer_id != "-1") {
                             $ci = $this->Customer_Model->get_cutomer_detail_by_id($value->customer_id);
                          }
                         ?>
                        <tr class="stocks-row">
                            <td><a href="<?=base_url()?>ez/order/edit/<?=$value->order_id?>/"># <?=$value->order_id?></a></td>
                            <td>
                                <?php if ($ci == ""): ?>
                                    POS SALE
                                <?php else: ?>
                                    <?=$ci->customer_fname . ' ' . $ci->customer_lname?>
                                <?php endif; ?>
                            </td>
                            <td>P <?=number_format($value->order_total, 2)?></td>
                            <td>
                                <?php if($value->order_status == '-1'): ?>
                                    <span class="danger label" style="text-transform: uppercase;">Cancelled</span>
                                <?php elseif($value->order_payment_status == 1): ?>
                                    <span class="dark label" style="text-transform: uppercase;">Proof Sent</span>
                                <?php elseif($value->order_payment_status <= 0): ?>
                                    <span class="warning label" style="text-transform: uppercase;">Pending</span>
                                <?php else: ?>
                                    <span class="success label" style="text-transform: uppercase;">Done!</span>
                                <?php endif; ?>
                            </td>
                            <td><?=isset($this->Order_Model->get_all_meta_value($value->order_id, 'payment_method')->value)? $this->Order_Model->get_all_meta_value($value->order_id, 'payment_method')->value : "Do not Process"?></td>
                            <td><?=$value->date_created?></td>
                        </tr>
                      <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="sixteen colgrid">
            <canvas id="chart" style="max-width: 100%; margin-top:-50px;"></canvas>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">
                    <div class="push_one eight columns">
                        <div class="row">
                        <span><h3 class="content-title">Items on Sale</h3><a href="<?=base_url()?>ez/product/sale/"><span class="btn-more">More</span></a></span>
                        <hr>
                            <table>
                                <?php foreach ($this->Reports_Model->get_all_product_on_sale() as $key => $value): ?>
                                    <?php
                                        $vi = $this->Product_Model->get_variant_by_id($value->product_variant_id);
                                        $pi = $this->Product_Model->get_product_by_id($vi->product_id);

                                        $img = $vi->product_variant_img;

                                        if(strpos($img, ',')) {
                                            $imgs = explode(',', $img);
                                            $img = $imgs[0];
                                        }

                                        $datetime1 = new DateTime(date("d-m-Y", strtotime($value->product_sale_expires)));
                                        $datetime2 = new DateTime(date("d-m-Y", time()));
                                        $interval = $datetime1->diff($datetime2);
                                        $date_reamains = $interval->format('%R%a');

                                     ?>

                                    <tr class="sale-row">
                                        <td><img src="<?=base_url()?>img/products/<?=$img?>" width="90" alt="" /></td>
                                        <td>
                                            <span class="product-title"><?=$pi->product_title?></span><Br/>
                                            <span class="txt-label"><?=$vi->product_variant_name?></span>
                                        </td>
                                        <td>
                                            <span class="product-title"><?=$this->Reports_Model->get_sold_count($value->product_sale_id)->sold_count?>/<?=$this->Inventory_Model->get_inventory_details_sku($vi->product_variant_sku)->inventory_stocks?></span><Br/>
                                            <span class="txt-label" style="font-size: 12px" >Sold/Stock</span>
                                        </td>
                                        <td>
                                            <span class="product-title"><?=$date_reamains?></span><Br/>
                                            <span class="txt-label" style="font-size: 12px" >Days Remaining</span>
                                        </td>
                                        <td>
                                            <a href="<?=base_url()?>ez/product/sale/end/<?=$value->product_sale_id?>/">
                                                <button type="button" name="button">End</button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>

                        </div>

                        <div class="row">
                            <span><h3 class="content-title">Top 5 Searches</h3><span class="btn-more">More</span></span>
                            <hr>
                                <canvas id="chart2" style="max-height: 150px; max-width: 300px; float: left;"></canvas>

                                <?php foreach ($this->Reports_Model->get_search_stat() as $key => $value): ?>
                                    <?php
                                        $color = "";
                                        $key += 1;
                                        switch ($key) {
                                            case '1': $color = "#F7464A"; break;
                                            case '2': $color = "#46BFBD"; break;
                                            case '3': $color = "#3ab879"; break;
                                            case '4': $color = "#f09216"; break;
                                            case '5': $color = "#1d91cb"; break;
                                        }
                                     ?>

                                     <span><i class="icon-record" style="color: <?=$color?>;"></i> <span> <?=$value->search_statistic_keyword?></span></span><br/>
                                <?php endforeach; ?>

                            <hr>
                            <BR/>
                        </div>

                        <div class="row">
                            <span><h3 class="content-title">On Going Promos</h3><a href="<?=base_url()?>ez/discount/"><span class="btn-more">More</span></a></span>
                            <hr>
                            <table>
                                <?php foreach ($this->Discount_Model->get_all_discounts() as $key => $row): ?>
                                    <?php
                                        if($row->discount_status == '0' || count($this->Discount_Model->get_discount_by_code($row->discount_code)) <= 0) {
                                            continue;
                                        }
                                     ?>

                                     <tr class="stocks-row">
                                        <td>
                                            <span class="product-title"><?=$row->discount_code?></span><Br/>
                                            <span class="txt-label">Code</span>
                                        </td>
                                        <td>
                                            <span class="product-title"><?=($row->discount_type == 1 ? number_format($row->discount_value, 2) : number_format($row->discount_value, 0)) . ' ' . ($row->discount_type == 1 ? 'Off' : '%')?></span><Br/>
                                            <span class="txt-label">Value</span>
                                        </td>
                                        <td>
                                            <span class="product-title"><?=date('m/d/Y', strtotime($row->date_created))?></span><Br/>
                                            <span class="txt-label">Start</span>
                                        </td>
                                        <td>
                                            <span class="product-title"><?=date('m/d/Y', strtotime($row->date_expires))?></span><Br/>
                                            <span class="txt-label">Ends</span>
                                        </td>
                                     </tr>
                                <?php endforeach; ?>
                            </table>
                            <hr>
                        </div>
                    </div>

                    <div class="seven columns">
                        <div class="row">
                            <span><h3 class="content-title">Inquiries</h3><span class="btn-more">More</span></span>
                            <hr>
                            <table class="inquiries-holder">
                                <?php foreach ($this->Customer_Model->get_all_inquiries() as $key => $value): ?>
                                    <?php
                                        $fullname = explode(' ', $value->inquiry_fullname);
                                        $initial = "";
                                        if (count($fullname) >= 2) {
                                            $initial = $fullname[0][0] . '' . $fullname[1][0];
                                        } else {
                                            $initial = $fullname[0][0];
                                        }
                                     ?>
                                    <tr onclick="javascript:window.location='<?=base_url()?>ez/inquiry/info/<?=$value->inquiry_id?>'">
                                        <td><div class="initial-holder"><?=$initial?></div></td>
                                        <td><?=substr($value->inquiry_message, 0, 45) . '' . ((strlen($value->inquiry_message) > 45 ) ? '...' : '')?></td>
                                        <td><?=date('m/d/Y', strtotime($value->date_created))?></td>
                                        <td><?=$value->inquiry_category?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>

                        <div class="row">
                            <span><h3 class="content-title">Sales Summary</h3></span>
                            <hr>
                            <div class="bar-holder">
                            	<div>
                                    <Br/>
                                    <span><i class="icon-record" style="color: #fe3b3f;"></i><span class="txt-label"> This Month</span></span>
                                    <span><i class="icon-record" style="color: #fff;"></i> <span class="txt-label"> Last Month</span></span>
                            		<div class="on-hand"></div>
                            		<div class="sold"></div>
                            	</div>
                            </div>

                            <ul class="two_up tiles">
                                <li style="border-right: 1px solid #000;">
                                    <center>
                                        <span class="sales-value">P <?=number_format($tm, 2)?></span>
                                        <span class="txt-label">This Month ( <?=date('M', time())?> )</span>
                                    </center>
                                </li>
                                <li>
                                    <center>
                                        <span class="sales-value">P <?=number_format($lm, 2)?></span>
                                        <span class="txt-label">Last Month ( <?=date('M', strtotime("-30 days", time()))?> )</span>
                                    </center>
                                </li>
                            </ul>
                            <hr>
                        </div>

                        <div class="row">
                            <span><h3 class="content-title">Stocks Monitoring</h3></span>
                            <hr>
                                <table>
                                    <?php foreach ($this->Reports_Model->get_product_w_inventory(5) as $key => $value): ?>
                                        <?php
                                            $vi = $this->Product_Model->get_variant_by_id($value->product_variant_id);
                                            $pi = $this->Product_Model->get_product_by_id($vi->product_id);

                                            $img = $vi->product_variant_img;

                                            if(strpos($img, ',')) {
                                                $imgs = explode(',', $img);
                                                $img = $imgs[0];
                                            }
                                         ?>
                                        <tr class="stocks-row">
                                            <td><img src="<?=base_url()?>img/products/<?=$img?>" width="50" alt="" /></td>
                                            <td style="width: 30% !important;">
                                                <span class="product-title"><?=$pi->product_title?></span><br/>
                                                <span class="txt-label"><?=$vi->product_variant_name?></span>
                                            </td>
                                            <td>
                                                <span class="product-title"><?=$this->Inventory_Model->get_inventory_details_sku($vi->product_variant_sku)->inventory_stocks?></span><Br/>
                                                <span class="txt-label">Stock</span>
                                            </td>
                                            <td>
                                                <span class="product-title">P <?=shorten($vi->product_variant_price)?></span><Br/>
                                                <span class="txt-label">Price</span>
                                            </td>

                                            <td>
                                                <span class="product-title">P <?=shorten($vi->product_variant_price*$this->Inventory_Model->get_inventory_details_sku($vi->product_variant_sku)->inventory_stocks)?></span><Br/>
                                                <span class="txt-label">Total Value</span>
                                            </td>

                                            <td>
                                                <span class="product-title">
                                                    <?php if ($this->Inventory_Model->get_inventory_details_sku($vi->product_variant_sku)->inventory_stocks <= $vi->product_variant_critical_value): ?>
                                                        Re-Stock
                                                    <?php else: ?>
                                                        None
                                                    <?php endif; ?>
                                                </span><Br/>
                                                <span class="txt-label">Action Needed</span>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                </table>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script>
Chart.defaults.global.animationEasing        = 'easeInOutQuad',
Chart.defaults.global.responsive             = true;
Chart.defaults.global.scaleOverride          = true;
Chart.defaults.global.scaleShowLabels        = false;
Chart.defaults.global.scaleSteps             = 1000;
Chart.defaults.global.scaleStepWidth         = 10;
Chart.defaults.global.scaleStartValue        = 0;
Chart.defaults.global.tooltipFontFamily      = 'Open Sans';
Chart.defaults.global.tooltipFillColor       = 'rgba(0, 0, 0, 0.5)';
Chart.defaults.global.tooltipFontColor       = '#fff';
Chart.defaults.global.tooltipCaretSize       = 0;
Chart.defaults.global.maintainAspectRatio    = true;
Chart.defaults.global.showTooltips 	= false;

Chart.defaults.Line.scaleShowHorizontalLines = false;
Chart.defaults.Line.scaleShowHorizontalLines = false;
Chart.defaults.Line.scaleGridLineColor       = 'transparent';
Chart.defaults.Line.scaleLineColor           = 'transparent';

var chart = document.getElementById('chart').getContext('2d');
var chart2 = document.getElementById('chart2').getContext('2d');

var data  = {
labels: ['','','','','','',''],

datasets: [
    {
      label: 'Profit',
      fillColor: '#fff',
      strokeColor: '#fff',
      pointColor: 'transparent',
      pointStrokeColor: 'transparent',
      pointHighlightStroke: 'transparent',
      data: [0,3320,3012,2550,5220,6033,2660]
  }
]
};

var data2 = [
    <?php
        foreach ($this->Reports_Model->get_search_stat() as $key => $value) {
            $color = "";
            $key += 1;
            switch ($key) {
                case '1': $color = "#F7464A"; break;
                case '2': $color = "#46BFBD"; break;
                case '3': $color = "#3ab879"; break;
                case '4': $color = "#f09216"; break;
                case '5': $color = "#1d91cb"; break;
            }
    ?>
        {
            value: parseInt("<?=$value->search_statistic_count?>"),
            color:"<?=$color?>",
            highlight: "<?=$color?>",
            label: "<?=$value->search_statistic_keyword?>"
        },
    <?php
        }
     ?>
]

var chart = new Chart(chart).Line(data);
var myDoughnutChart = new Chart(chart2).Doughnut(data2);
</script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
