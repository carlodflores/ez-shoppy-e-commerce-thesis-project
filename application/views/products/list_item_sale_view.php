<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    .upload-holder {
        width: 100%;
        background #000;
        text-align: center;
        position: relative;
    }

    .upload-holder input {
        opacity: 0;
        width: 100%;
        padding: 20px;
        position: absolute;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-tag"> </i>Put Item on Sale</h3><a href="<?=base_url(). 'ez/product/sale/'?>"><span class="btn-more" style="color:#fff;">Cancel</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">

                </div>
                <div class="row field">
                    <div class="centered push_one eight columns">
                        <center><h2>Put an Item on Sale</h2>
                        <p>Add / Set Stocks of your Product Variants</p></center>
                        <Br/>
                        <form method="post" class="append">
                            <span class="txt-label">Product Name</span><br/>
                            <input type="text" class="input" name="search_temp" placehoder="Search Product" list="search-datalist">
                            <datalist id="search-datalist">
                                <?php foreach($products as $row): ?>
                                    <option data-value="<?=$row->product_id?>" value="<?=ucwords(strtolower($row->product_title))?>" />
                                <?php endforeach; ?>
                            </datalist>
                        </form>
                    </div>

                    <div class="centered push_one twelve columns">
                        <Br/>

                        <table class="paginate" max="10">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th></th>
                                    <th><center><span class="product-title">Product Variant</span></center></th>
                                    <th><center><span class="product-title">SKU</span></center></th>
                                    <th><center><span class="product-title">Quantity</span></center></th>
                                    <th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach($variants as $row): ?>
                                <?php
                                    if(count($this->Reports_Model->check_product_on_sale($row->product_variant_id)) >= 1) {
                                        continue;
                                    }
                                 ?>
                            <tr class="stocks-row <?=$row->product_variant_id?>_holder"  data-tag="<?=$this->Product_Model->get_product_by_id($row->product_id)->product_title?>">
                                <td>
                                    <div class="upload-holder">
                                        <input type='file' data-value="<?=$row->product_variant_id?>" name="file_prod_images" id="inputFile" multiple/>
                                        <?php if($row->product_variant_img != ""): ?>
                                            <?php if(strpos($row->product_variant_img, ',')): ?>
                                                <?php $img = explode(',',$row->product_variant_img); ?>
                                                <img src="<?=base_url() . 'img/products/' . $img[0]?>" alt="" width="50"/>
                                            <?php else: ?>
                                                <img src="<?=base_url() . 'img/products/' .$row->product_variant_img?>" alt="" width="50"/>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                    </div>
                                </td>

                                <td>
                                    <span class="product-title"><?=$this->Product_Model->get_product_by_id($row->product_id)->product_title?></span><Br/>
                                    <span class="txt-label" style="color: #000;"><?=str_replace(' ', ' / ', $row->product_variant_name)?></span>
                                </td>

                                <td>
                                    <span class="product-title"><?=$row->product_variant_sku?></span><Br/>
                                    <span class="txt-label">Sku</span>
                                </td>

                                <td>
                                    <span class="product-title <?=$row->product_variant_id?>_content" data-value="<?=$this->Inventory_Model->get_inventory_details_sku($row->product_variant_sku)->inventory_stocks?>"><?=$this->Inventory_Model->get_inventory_details_sku($row->product_variant_sku)->inventory_stocks?></span><Br/>
                                    <span class="txt-label">Quantity</span>
                                </td>

                                <td class="field">
                                    <a href="<?=base_url(). 'ez/product/sale/create/' . $row->product_variant_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Put On Sale</span></a>
                                </td>

                            </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>

        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>
$(function() {



    $('input[name="search_temp"]').keyup('change', function() {
        var data = $('#search-datalist'),
            sel = $(this).val();

        $( ".stocks-row[data-tag]" ).each(function( index ) {
            if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                $(this).hide("fast");
            } else {
                $(this).show("fast");
            }
        });

    });

});
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
