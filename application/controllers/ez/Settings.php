<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$config['base_url'] = $this->Option_Model->get_option_by_value('base_url')->option_value;
		$this->load->model("Settings_Model");
	}

	public function shipping($action="", $id="") {
		$data['title'] = "EZ Dashboard";
		switch($action) {
			case 'add':

				$this->form_validation->set_rules('shipping_name', '', 'required');
				$this->form_validation->set_rules('shipping_type', '', 'required');
				$this->form_validation->set_rules('shipping_price', '', 'required');
				$this->form_validation->set_rules('shipping_min_size', '', 'required');
				$this->form_validation->set_rules('shipping_max_size', '', 'required');

				if($this->form_validation->run()) {

					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$shipping_data = array(
						"shipping_name" => $this->input->post('shipping_name'),
						"shipping_amount" => $this->input->post('shipping_price'),
						"shipping_max" => $this->input->post('shipping_max_size'),
						"shipping_min" => $this->input->post('shipping_min_size'),
						"shipping_type" => $this->input->post('shipping_type'),
						"shipping_courier" => "Xend/2Go",
						"date_created" => $time
					);

					$this->Settings_Model->insert_shipping($shipping_data);
					redirect(base_url() . 'ez/settings/shipping/');
				}

				$this->load->view('settings/add_shipping_view', $data);
			break;

			case 'edit':

				$this->form_validation->set_rules('shipping_name', '', 'required');
				$this->form_validation->set_rules('shipping_type', '', 'required');
				$this->form_validation->set_rules('shipping_price', '', 'required');
				$this->form_validation->set_rules('shipping_min_size', '', 'required');
				$this->form_validation->set_rules('shipping_max_size', '', 'required');

				if($this->form_validation->run()) {

					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$shipping_data = array(
						"shipping_name" => $this->input->post('shipping_name'),
						"shipping_amount" => $this->input->post('shipping_price'),
						"shipping_max" => $this->input->post('shipping_max_size'),
						"shipping_min" => $this->input->post('shipping_min_size'),
						"shipping_type" => $this->input->post('shipping_type')
					);

					$this->Settings_Model->update_shipping($shipping_data, $id);
					redirect(base_url() . 'ez/settings/shipping/');
				}

				$data['is'] = $this->Settings_Model->get_shipping_details($id);
				$this->load->view('settings/edit_shipping_view', $data);
			break;

			default:
				$this->load->view('settings/shipping_view', $data);
			break;
		}
    }

	public function courier($action="", $id="") {
		$data['title'] = "EZ Dashboard";
		switch($action) {
			case 'add':
				$this->form_validation->set_rules('courier_name', '', 'required');
				if ($this->form_validation->run()) {

					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$courier_data = array(
						"shipping_courier_name" => $this->input->post('courier_name'),
						"date_created" => $time
					);

					$this->Settings_Model->insert_courier($courier_data);
					redirect(base_url() . 'ez/settings/courier/');
				}
				$this->load->view('settings/add_courier_view', $data);
			break;

			case 'edit':
			$this->form_validation->set_rules('courier_name', '', 'required');
			if ($this->form_validation->run()) {

				$courier_data = array(
					"shipping_courier_name" => $this->input->post('courier_name')
				);

				$this->Settings_Model->update_courier($courier_data, $id);
				redirect(base_url() . 'ez/settings/courier/');
			}
			$data['ic'] = $this->Settings_Model->get_courier_details($id);
			$this->load->view('settings/edit_courier_view', $data);
		break;
			break;

			default:
				$this->load->view('settings/courier_view', $data);
			break;
		}
	}

	public function payment($action="", $id="") {
		$data['title'] = "EZ Dashboard";
		switch($action) {
			case 'add':
				$this->form_validation->set_rules('courier_name', '', 'required');
				if ($this->form_validation->run()) {

					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$courier_data = array(
						"shipping_courier_name" => $this->input->post('courier_name'),
						"date_created" => $time
					);

					$this->Settings_Model->insert_courier($courier_data);
					redirect(base_url() . 'ez/settings/courier/');
				}
				$this->load->view('settings/add_courier_view', $data);
			break;

			case 'edit':
			$this->form_validation->set_rules('courier_name', '', 'required');
			if ($this->form_validation->run()) {

				$courier_data = array(
					"shipping_courier_name" => $this->input->post('courier_name')
				);

				$this->Settings_Model->update_courier($courier_data, $id);
				redirect(base_url() . 'ez/settings/courier/');
			}
			$data['ic'] = $this->Settings_Model->get_courier_details($id);
			$this->load->view('settings/edit_courier_view', $data);
		break;
			break;

			default:
				$this->load->view('settings/courier_view', $data);
			break;
		}
	}

	public function general() {
		$data['title'] = "EZ Dashboard";
		$this->load->view('settings/general_view', $data);
	}

}

?>
