<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content field">
        <center>
            <h1>Checkout</h1>
            <p>
                Returning Customer? <a href="#" class="switch" gumby-trigger="#login_modal">Click to Login</a>.
            </p>
            <hr style="height: 1px; border: none; background: #000; width: 100px;">
        </center>
        <?php if (validation_errors() != ""): ?>
            <div class="row">
                <div class="danger alert" style="padding: 10px;"><center><?=validation_errors('<span style="display: block;">','</span>')?></center></div>
            </div>
        <?php endif; ?>
        <form class="" action="<?=base_url()?>cart/checkout/<?=$group?>/" method="post">
        <div class="row">
            <div class="ten columns">
                <h3>Shipping Address</h3>
                <div class="sixteen colgrid">
                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">First Name </span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_fname" placeholder="First Name" value="<?=(set_value('txt_fname') != "") ? set_value('txt_fname') : ((isset($c->customer_fname))? $c->customer_fname : '')?>"><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">Last Name</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_lname" placeholder="Last Name" value="<?=(set_value('txt_lname') != "") ? set_value('txt_lname') : ((isset($c->customer_lname))? $c->customer_lname : '')?>"><br/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">E-mail</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_email" placeholder="E-mail" value="<?=(set_value('txt_email') != "") ? set_value('txt_email') : ((isset($c->customer_email))? $c->customer_email : '')?>"><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">Phone #</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_phone" placeholder="Phone #" value="<?=(set_value('txt_phone') != "") ? set_value('txt_phone') : ((isset($c->customer_phone))? $c->customer_phone : '')?>"><br/>
                        </div>
                    </div>
                </div>
                <?php if ($this->session->userdata('customer') == null): ?>
                    <Br/>

                    <input name="account_check" id="check1" type="checkbox" /> Create an Account?
                    <div class="sixteen colgrid password-field" <?=set_value('txt_passwod') != ""? '' : 'style="display: none;"'?>>
                        <div class="row">
                            <div class="eight columns">
                                <span class="text-label input-label">Password </span>
                                <input type="password" class="input" name="txt_password" placeholder="Password" value="<?=set_value('txt_password')?>"><br/>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <Br/>
                <h4>Address</h4>

                <div class="sixteen colgrid">
                    <span class="text-label input-label">Company</span>
                    <input type="text" class="input" name="txt_company" placeholder="Company (Optional)" value="<?=(set_value('txt_company') != "") ? set_value('txt_company') : ((isset($c->customer_company))? $c->customer_company : '')?>"><br/>

                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">Street Address</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_address" placeholder="Street Address" value="<?=(set_value('txt_address') != "") ? set_value('txt_address') : ((isset($c->customer_address))? $c->customer_address : '')?>"?><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">City</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_city" placeholder="City" value="<?=(set_value('txt_city') != "") ? set_value('txt_city') : ((isset($c->customer_city))? $c->customer_city : '')?>"><br/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="eight columns">
                            <span class="text-label input-label">Province / State (eg. Manila, Davao)<span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_state" placeholder="Province / State" value="<?=(set_value('txt_state') != "") ? set_value('txt_state') : ((isset($c->customer_state))? $c->customer_state : '')?>"><br/>
                        </div>
                        <div class="eight columns">
                            <span class="text-label input-label">Postal / Zip Code</span> <span class="text-indicator">*</span>
                            <input type="text" class="input" name="txt_zipcode" placeholder="Postal / Zip Code" value="<?=(set_value('txt_zipcode') != "") ? set_value('txt_zipcode') : ((isset($c->customer_zipcode))? $c->customer_zipcode : '')?>"><br/>
                        </div>
                    </div>
                </div>

                <span class="text-label input-label">Country</span> <span class="text-indicator">*</span>
                <input type="text" class="input" name="txt_country" placeholder="Country" value="Philippines" disabled="disable"><br/>
                <Br/>
                <h4>Payment Method</h4>
                <span class="text-label input-label">Select Payment Method <span class="text-indicator">*</span></span>
                <select class="input xxwide" name="txt_payment_method">
                    <option value="BDO" <?=set_select('txt_payment_method', 'BDO', TRUE); ?>>BDO</option>
                    <option value="GCash" <?=set_select('txt_payment_method', 'GCash'); ?>>GCash</option>
                    <option value="LBC" <?=set_select('txt_payment_method', 'LBC'); ?>>LBC</option>
                    <option value="BPI" <?=set_select('txt_payment_method', 'BPI'); ?>>BPI</option>
                    <option value="PayPal" <?=set_select('txt_payment_method', 'PayPal'); ?>>PayPal (Safe & Secured Payment)</option>
                </select>
                <br/>
            </div>

            <div class="six columns  cart-holder">
                <h3>Discount</h3>
                    <span class="text-label input-label">Discount Code</span><Br/>
                    <input type="text" class="input normal" name="txt_discount" placeholder="Discount Code">
                    <button type="button" name="discount_trg" style="padding: 8px; font-size: 14px;">Apply Discount</button>
                    <br/>
                    <span class="text-label discount-error" style="color: red;"></span>
                <br/>
                <h3>Your Order</h3>
                <?php
                    $total=$size= 0;

                    $group = $this->session->userdata('group');
                    $content = $this->Cart_Model->get_cart_content($this->ezclient->get_ip(), $group);
                 ?>
                <table>
                    <tr class="field">
                        <td>Item x Quantity</td>
                        <td>Total</td>
                    </tr>
                    <?php foreach ($content as $key => $row): ?>
                        <?php
                            $value = $this->Product_Model->get_variant_by_id($row->product_variant_id);
                            $product_data = $this->Product_Model->get_product_by_id($value->product_id);
                            $size += $value->product_variant_weight * $row->cart_quantity;
                            $total += intval($row->cart_quantity)*intval($value->product_variant_price);

                            $meta = $this->Product_Model->get_meta_by_id($value->product_id);
                            $pvname = (strpos($value->product_variant_name, ' '))? explode(' ', $value->product_variant_name) : $value->product_variant_name;
                            $desc = "";

                            foreach ($meta as $metaKey => $metaRow) {
                                if(is_array($pvname)) {
                                    $desc .= $metaRow->product_meta_keyword . ': ' . $pvname[$metaKey] . ' ';
                                } else {
                                    $desc =  $metaRow->product_meta_keyword . ': ' . $pvname . ' ';
                                }
                            }
                         ?>
                        <tr class="field">
                            <td><?=$product_data->product_title?><br/>(<?=$desc?>)  x <?=$row->cart_quantity?></td>
                            <td>P <?=number_format(intval($value->product_variant_price)*$row->cart_quantity, 2)?></td>
                        </tr>
                    <?php endforeach; ?>

                    <tbody class="load-discount">

                    </tbody>

                    <tr class="field">
                        <td>Subtotal</td>
                        <td id="subtotal-value">P <?=number_format($total, 2)?></td>
                    </tr>

                    <tbody class="load-shipping">

                    </tbody>

                    <tbody class="load-payment">

                    </tbody>

                    <tfoot>
                        <tr class="field total-holder">
                            <td>Total</td>
                            <td id="total-value">P <?=number_format($total, 2)?></td>
                        </tr>
                    </tfoot>
                </table>
                <h3>Order Note</h3>
                    <span class="text-label input-label">Order Note</span><Br/>
                    <textarea class="input textarea" name="txt_note" placeholder="Place message to Hairgeek Here."></textarea>
                <center>
                    <div class="">
                        <input name="tac" id="check1" type="checkbox"> I’ve read and accept the <a href="#" style="font-weight: 700;">Terms & Conditions</a>
                    </div><Br/>
                    <input type="hidden" name="txt_total" value="<?=$total?>" />
                    <input type="hidden" name="txt_shipping" />
                    <input type="hidden" name="txt_additional_payment" value="0" />
                    <button type="submit" style="padding: 10px; font-size: 20px;"> <i class="fa fa-check-circle"> </i> Place Order</button>
                </center>
            </div>
        </form>
        </div>
	</div>

    <script type="text/javascript">
        $(function() {


            var local = false;
            var subtotal = "<?=$total?>";
            var nTotal = subtotal;

            var shipping = 0;

            if($('input[name="txt_state"]').val().toLowerCase().match(/manila/g)) local = true;
            else local = false;

            generate_shipping();


			$('input').on('change', function() {
				generate_shipping();
			}).trigger('change');

            $('input[name="txt_state"]').on('change', function() {
                if($(this).val().toLowerCase().match(/manila/g))
                    local = true;
                else
                    local = false;
            });

            $('input[name="txt_address"], input[name="txt_city"], input[name="txt_state"], input[name="txt_zipcode"]').on('change', function() {
                generate_shipping();
            });

            $('select[name="txt_payment_method"]').on('change', function(){
                generate_shipping();
            }).trigger('change');

            $("input[name='account_check']").on('change', function() {
                $('.password-field').slideToggle('fast');
            });

            $("button[name='discount_trg']").click(function() {
                if($('input[name="txt_discount"]').val()  != "") {
                    $.post('<?=base_url()?>cart/getDiscount/' + $('input[name="txt_discount"]').val()  + '/', function(data) {
                        var info = $.parseJSON(data);
                        if(data != "null") {
                            var des = info.discount_type == 1 ? 'P' : '%';
                            var discount = info.discount_value;
                            var discount_type = info.discount_type;
                            if(discount != 0
                            && discount_type != 0) {
                                if(discount_type == 1) {
                                    subtotal = parseInt(subtotal) - parseInt(discount);
                                } else {
                                    subtotal = parseInt(subtotal) - (parseInt(subtotal)*(parseInt(discount)/100));
                                }
                                $("#subtotal-value").html("P " + subtotal.toFixed(2));
                            }

                            $("button[name='discount_trg']").text("Discount Applied").attr({'name': 'discount_trigger', 'disabled': 'disable'});
                            $('input[name="txt_discount"]').attr({'readonly': 'readonly'})
                            $('.discount-error').html("");
                        } else {
                            subtotal = "<?=$total?>";
                            $('.load-discount').html('');
                            $("#subtotal-value").html("P " + parseInt(subtotal).toFixed(2));
                            generate_shipping();
                            $('.discount-error').html("Failed to apply discount. Is it type correctly?");
                        }
                        $('.load-discount').html('<tr class="field"><td>Discount -</td><td>' + (des == 'P'? 'P' : '')  +' '+ info.discount_value +''+ (des == 'P'? '' : '%')   +'</td></tr>');
                        generate_shipping();
                    });
                } else {
                    subtotal = "<?=$total?>";
                    $('.load-discount').html('');
                    $("#subtotal-value").html("P " + parseInt(subtotal).toFixed(2));
                    generate_shipping();
                }
            });

            function generate_shipping() {

                var paymethod = $('select[name="txt_payment_method"]').val();
                var payment = 0;
                var discount = 0,
                    discount_type = 0;

                switch (paymethod) {
                    case 'BDO':
                    case 'BPI': payment = 0; break;

                    case 'GCash':
                    case 'LBC': payment = (subtotal < 1000)? 50 : 100; break;
                }

                $('input[name="txt_additional_payment"]').val(payment);
                $(".load-payment").html('<tr class="field"><td>Payment Fee +</td><td>P '+ payment.toFixed(2) +' ('+ paymethod +')</td></tr>');

                if($('input[name="txt_address"]').val() != ""
                && $('input[name="txt_city"]').val() != ""
                && $('input[name="txt_state"]').val() != ""
                && $('input[name="txt_zipcode"]').val() != "") {
                    var isLocal = local? '1' : '2';
                    $.post('<?=base_url()?>cart/getSize/' + isLocal + '/<?=$size?>/', function(data) {
                        var info = $.parseJSON(data);
                        var des = info.shipping_name;
                        console.log(isLocal);
                        $('.load-shipping').html('<tr class="field"><td>Shipping +</td><td>P '+ info.shipping_amount +'<br/>'+ des +'</td></tr>');
                        shipping = parseInt(info.shipping_amount);
                        $("input[name='txt_shipping']").val(info.shipping_amount);
						nTotal = (parseInt(subtotal)+parseInt(payment)+parseInt(shipping));
						$("#total-value").html("P " + nTotal.toFixed(2));
                    });
                } else {
                    $('.load-shipping').html('');
                }

                nTotal = (parseInt(subtotal)+parseInt(payment)+parseInt(shipping));
                $("#total-value").html("P " + nTotal.toFixed(2));

                $("input[name='txt_total']").val(subtotal);
            }

			generate_shipping();
        });
    </script>
<?php
    $this->load->view('main/footer_view');
?>
