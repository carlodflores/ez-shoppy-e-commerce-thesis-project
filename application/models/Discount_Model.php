<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_Model extends CI_Model {

    function get_all_discounts() {
        return $this->db->get('cs_discounts')->result();
    }

    function get_discount_by_code($code) {
        $time = time();
        $this->db->where('discount_code', $code);
        $this->db->where('discount_limit !=', 'discount_used');
        $this->db->where("UNIX_TIMESTAMP(date_created) <=", $time. "");
        $this->db->where("UNIX_TIMESTAMP(date_expires) >=", $time. "");
        return $this->db->get('cs_discounts')->row();
    }

    function update_discount_by_code($code, $data) {
        $this->db->where('discount_code', $code);
        $this->db->update('cs_discounts', $data);
        return $this->db->affected_rows();
    }

    function insert_discount($data) {
        $this->db->insert('cs_discounts', $data);
        return $this->db->insert_id ();
    }

}
