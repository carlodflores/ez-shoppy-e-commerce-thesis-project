<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class='icon-tag'> </i>All Product</h3><a href="<?=base_url(). 'ez/product/add/'?>"><span class="btn-more" style="color:#fff;">Add Product</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid">
                <div class="row">
                    <div class="centered push_one twelve columns">
                        <table class="paginate">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th></th>
                                    <th><center><span class="product-title">Name</span></center></th>
                                    <th><center><span class="product-title">Inventory</span></center></th>
                                    <th><center><span class="product-title">Category</span></center></th>
                                    <th><center><span class="product-title">Brand</span></center></th>
                                    <th><center><span class="product-title">Action</span></center></th>
                                </tr>
                            </thead>

                            <?php foreach($products as $row): ?>

                                    <tr class="stocks-row">
                                        <?php
                                            $img = "";
                                            if (strpos($row->product_img, ',')) {
                                                $img = explode(',', $row->product_img);
                                            } else {
                                                $img = $row->product_img;
                                            }
                                        ?>
                                        <td><span class="product-title"><img src="<?php echo base_url() . 'img/products/' . ((is_array($img))? $img[0] : $img); ?>" alt="" width="50"/></span></td>
                                        <td>
                                            <span class="product-title"><?=$row->product_title?></span><Br/>
                                            <span class="txt-label"></span>
                                        </td>
                                        <td>
                                            <span class="product-title"><?=$this->Inventory_Model->get_sum_stocks($row->product_id)?>/<?=count($this->Product_Model->get_product_variant($row->product_id))?></span><Br/>
                                            <span class="txt-label">Stocks/Variants</span>
                                        </td>
                                        <td>
                                            <span class="product-title"><?=$row->product_type_name?></span><Br/>
                                            <span class="txt-label">Category</span>
                                        </td>

                                        <td>
                                            <span class="product-title"><?=$row->product_brand_name?></span><Br/>
                                            <span class="txt-label">Brand</span>
                                        </td>

                                        <td>
                                            <a href="<?=base_url(). 'ez/product/edit/' . $row->product_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Edit</span></a>
                                            <a href="<?=base_url(). 'ez/product/manage/variant/' . $row->product_id?>/"><span class="btn-more" style="margin-right: 5px !important; float: none;">Manage Variant</span></a>
                                        </td>
                                    </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
