<?php
    $this->load->view('header_view');
    $this->load->view('left_nav_view');
?>
<style media="screen">
    .pager {
        width: 100%;
        text-align: center;
        margin-bottom: 20px;
    }

    .pager .page-number {
        margin-left: 10px;
    }

    input[type='checkbox'] {
        width: 16px;
        height: 16px;
        display: block !important;
        -webkit-appearance: checkbox;
    }

    a {
        color: #7997c1;
    }

    a:hover {
        color: #7999e2;
    }
</style>
<div id="site-wrapper">
    <br/>
    <div id="site-canvas">
        <div class="sixteen colgrid">
            <div class="row">
                <div class="push_one six columsn"><h3 class="content-title" style="color:#fff;"><i class="icon-download"> </i>Orders</h3><a href="<?=base_url(). 'ez/order/create/'?>"><span class="btn-more" style="color:#fff;">Create Order</span></a><Br/><Br/></div>
            </div>
        </div>

        <section class="page-content">
            <div class="sixteen colgrid field">
                <div class="row">
                    <div class="centered push_one eight columns">
                        <center><h2>Manage Orders</h2>
                        <p>The list of orders has been made.</p></center>
                        <Br/>
                        <form method="post" class="append">
                            <span class="txt-label">Customer Name</span><br/>
                            <input type="text" class="input" name="search_temp" placehoder="Search Product" list="search-datalist">
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="centered push_one twelve columns">
                        <table class="paginate" max="50">
                            <thead style="background: transparent;">
                                <tr class="stocks-row" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);">
                                    <th><center><span class="product-title">Order</span></center></th>
                                    <th><center><span class="product-title">Date</span></center></th>
                                    <th><center><span class="product-title">Customer</span></center></th>
                                    <th><center><span class="product-title">Payment Status</span></center></th>
                                    <th><center><span class="product-title">Fulfillment Status</span></center></th>
                                    <th><center><span class="product-title">Total</span></center></th>
                                </tr>
                            </thead>
                            <?php foreach ($orders as $key => $row): ?>
                                <tr class="stocks-row" data-tag="<?=($row->customer_id != '-1') ? $this->Customer_Model->get_cutomer_detail_by_id($row->customer_id)->customer_fname . ' '. $this->Customer_Model->get_cutomer_detail_by_id($row->customer_id)->customer_lname : "POS Sale"?>">
                                    <td><a href="<?=base_url()?>ez/order/edit/<?=$row->order_id?>/">#<?=$row->order_id?></a></td>
                                    <td><?=ago(human_to_unix($row->date_created))?></td>
                                    <td>
                                        <?php if ($row->customer_id != '-1'): ?>
                                            <a href="<?=base_url()?>ez/customer/profile/<?=$this->Customer_Model->get_cutomer_detail_by_id($row->customer_id)->customer_id?>/"><?=$this->Customer_Model->get_cutomer_detail_by_id($row->customer_id)->customer_fname . ' '. $this->Customer_Model->get_cutomer_detail_by_id($row->customer_id)->customer_lname?></a>
                                        <?php else: ?>
                                            POS Sale
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($row->order_status == '-1'): ?>
                                            <span class="danger label" style="text-transform: uppercase;">Cancelled</span>
                                        <?php elseif($row->order_payment_status == 1): ?>
                                            <span class="dark label" style="text-transform: uppercase;">Proof Sent</span>
                                        <?php elseif($row->order_payment_status == 3): ?>
                                            <span class="danger label" style="text-transform: uppercase;">Proof Req</span>
                                        <?php elseif($row->order_payment_status <= 0): ?>
                                            <span class="warning label" style="text-transform: uppercase;">Pending</span>
                                        <?php else: ?>
                                            <span class="success label" style="text-transform: uppercase;">Done!</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($row->order_status == '0'): ?>
                                            <span class="warning label" style="text-transform: uppercase;">Unfulfilled</span>
                                        <?php elseif($row->order_status == '-1'): ?>
                                            <span class="danger label" style="text-transform: uppercase;">Cancelled</span>
                                        <?php else: ?>
                                            <span class="success label" style="text-transform: uppercase;">Fulfilled</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>P <?=number_format($row->order_total, 2)?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('footer_view'); ?>
    </div>
</div>
<script>

    $(function() {



        $('input[name="search_temp"]').keyup('change', function() {
            var data = $('#search-datalist'),
                sel = $(this).val();

            $( ".stocks-row[data-tag]" ).each(function( index ) {
                if($(this).attr('data-tag').toLowerCase().indexOf(sel.toLowerCase())) {
                    $(this).hide("fast");
                } else {
                    $(this).show("fast");
                }
            });

        });

    });
</script>
<script src="<?=base_url()?>js/custom.js"></script>
<script gumby-touch="js/libs" src="<?=base_url()?>js/libs/gumby.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.retina.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.fixed.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.skiplink.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.toggleswitch.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.checkbox.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.radiobtn.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.tabs.js"></script>
<script src="<?=base_url()?>js/libs/ui/gumby.navbar.js"></script>
<script src="<?=base_url()?>js/libs/ui/jquery.validation.js"></script>
<script src="<?=base_url()?>js/libs/gumby.init.js"></script>
<script src="<?=base_url()?>js/plugins.js"></script>
<script src="<?=base_url()?>js/main.js"></script>
</body>
</html>
