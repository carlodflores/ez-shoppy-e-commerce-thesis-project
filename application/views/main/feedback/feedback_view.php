<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content field">
        <center>
            <h1>Feedback</h1>
            <p>
                Customer feedback on transaction or product.
            </p>
            <hr style="height: 1px; border: none; background: #000; width: 100px;">

        </center>
        <div class="row">
            <div class="sixteen columns">
                <ul class="feedback-page-holder">
                    <?php foreach($this->Customer_Model->get_all_feedback() as $key => $row): ?>

                    <li>
                        <center>
                            <img src="<?=base_url()?>img/profile.jpg" width="150" alt="" />
                            <h4><?=$row->feedback_fullname?></h4>	
                            <p>
                               <?=$row->feedback_description?>
                            </p>

                            <hr style="height: 1px; border: none; background: #000; width: 100px;">
                        </center>
                    </li>

                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
	</div>
<?php
    $this->load->view('main/footer_view');
?>
