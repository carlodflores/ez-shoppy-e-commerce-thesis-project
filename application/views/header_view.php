<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=$title?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="humans.txt">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?=base_url()?>img/favicon.png" type="image/x-icon" />

    <!-- Facebook Metadata /-->
    <meta property="fb:page_id" content="" />
    <meta property="og:image" content="" />
    <meta property="og:description" content=""/>
    <meta property="og:title" content=""/>

    <!-- Google+ Metadata /-->
    <meta itemprop="name" content="">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="">
    <link rel="stylesheet" href="<?=base_url()?>css/gumby.css" charset="utf-8">
    <link rel="stylesheet" href="<?=base_url()?>css/custom.min.css" charset="utf-8">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.css">
    <script src="<?=base_url()?>js/libs/modernizr-2.6.2.min.js"></script>
    <script src="<?=base_url()?>js/jquery.min.js"></script>
    <script src="<?=base_url()?>js/jquery-ui-1.9.2.min.js"></script>

    <style>
        .txt-label {
            color: rgba(0, 0, 0, 0.8) !important;
        }
    </style>
</head>
<body>
