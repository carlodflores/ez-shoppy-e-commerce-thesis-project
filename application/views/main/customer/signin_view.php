<?php
    $this->load->view('main/header_view');
    $this->load->view('main/top_navigation_view');
    $this->load->view('main/login_modal_view');
?>

	<div class="sixteen colgrid main-content field">
        <div class="row">
            <div class="seven centered columns text-center field">
                <h3><i class="fa fa-user"> </i> Login</h3>
                <?php if (validation_errors() != ""): ?>
                    <div class="row">
                        <div class="danger alert" style="padding: 10px;"><center><?=validation_errors('<span style="display: block;">','</span>')?></center></div>
                    </div>
                <?php endif; ?>
                <form action="<?=base_url()?>login/" method="post">
                    <span class="text-label input-label">Email</span>
                    <input type="text" class="input" name="txt_email" placeholder="E-mail"><br/>
                    <span class="text-label input-label">Password</span>
                    <input type="password" class="input" name="txt_password" placeholder="Password"><br/><br/>
                    <input type="hidden" name="txt_url" value="<?="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" />
                    <input type="submit"  class="medium primary btn" name="login" value="Login" />
                </form>
            </div>
        </div>
	</div>
<?php
    $this->load->view('main/footer_view');
?>
